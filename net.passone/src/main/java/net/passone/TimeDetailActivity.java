package net.passone;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.TextView;

import net.passone.adapter.Adapter;
import net.passone.adapter.AdapterItemManager;
import net.passone.adapter.Api;
import net.passone.adapter.CouponAdapter;
import net.passone.adapter.OnResponseListener;
import net.passone.common.CUser;
import net.passone.common.IntentModelActivity;
import net.passone.common.StaticVars;
import net.passone.common.Util;
import net.passone.container.CouponInfo;
import net.passone.container.CouponItem;
import net.passone.container.CouponUsedInfo;

import java.util.ArrayList;
import java.util.List;

public class TimeDetailActivity extends IntentModelActivity implements OnClickListener,OnResponseListener {
	ImageButton btn_login;
	Context context;
	OnResponseListener callback;
	Handler handler;
	ExpandableListView list_time;
	ArrayList<CouponItem> couponList;
	int cindex=0;
	CouponAdapter cAdapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.fragment_myclass);
		((TextView)findViewById(R.id.tv_apptitle)).setText("시간제쿠폰");
		list_time=(ExpandableListView)findViewById(R.id.list_package);
		couponList=new ArrayList<CouponItem>();
		context=this;
		callback=this;
		_api.MyCoupon(CUser.userid, context, callback);

		list_time.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                MyLectureItem item=StaticVars.myLectureItems.get(position);
//                getActivity().startActivity(new Intent(getActivity(),LectureActivity.class).putExtra("lec_seq",item.getLectureSeq()).putExtra("lec_basket_seq",item.getLecBasketSeq()));
			}
		});
		((ImageButton)findViewById(R.id.btn_back)).setOnClickListener(this);

		super.onCreate(savedInstanceState);
	}
	@Override
	public void onResponseReceived(int api, Object result) {
		Util.debug("Login result    :   " + result);
		switch(api) {

			case Api.COUPON:

				if (result != null) {
					AdapterItemManager.AddCouponList((List<CouponInfo>) result);
					if(StaticVars.couponItems.size()>0)
					{
//                      new PackageAsync(context).execute();
						cindex=0;
						CouponItem item=StaticVars.couponItems.get(cindex);
						_api.MyCouponUsed(CUser.userid, item.getUid(), context, callback);
					}
					Util.debug("coupon list:"+StaticVars.couponItems.size());

				}
				break;
			case Api.COUPONUSED:

				if (result != null) {
					if(cindex<StaticVars.couponItems.size()) {

						AdapterItemManager.AddCouponUsedList((List<CouponUsedInfo>) result, StaticVars.couponItems.get(cindex).getUid());

						if (StaticVars.couponUsedItems.size() > 0) {
							CouponItem citem = StaticVars.couponItems.get(cindex);
							citem.setCouponUsedList(StaticVars.couponUsedItems);
							couponList.add(citem);

						}

						cindex++;
						if (StaticVars.couponItems.size() > cindex) {
							CouponItem item = StaticVars.couponItems.get(cindex);
							_api.MyCouponUsed(CUser.userid, item.getUid(), context, callback);
						} else {
							cAdapter = new CouponAdapter(context, list_time, couponList);
							list_time.setAdapter(cAdapter);
							cAdapter.notifyDataSetChanged();

						}
					}
				}

				break;

		}
	}

	@Override
	protected void onResume() {
		cindex=0;

		if(CUser.userid.length()==0)
			loadUserInfo();
		if(_api==null)
			_api=new Adapter();
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		finish();
		super.onClick(v);
	}
}