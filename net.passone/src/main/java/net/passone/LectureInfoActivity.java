package net.passone;

import android.app.Fragment;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import net.passone.adapter.Adapter;
import net.passone.adapter.DBmanager;
import net.passone.adapter.LecDataAdapter;
import net.passone.adapter.OnResponseListener;
import net.passone.common.IntentModelActivity;
import net.passone.container.LecDataItem;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LectureFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LectureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LectureInfoActivity extends IntentModelActivity implements OnResponseListener, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Adapter _api;
    ArrayList<LecDataItem> dataList;
    Context context;
    OnResponseListener callback;
    LecDataAdapter dAdapter;
    ListView listView;
    DBmanager db_manager;
    SQLiteDatabase db;
    boolean mode_search=false;
    LinearLayout layout_search, layout_timetab,layout_timeinfo;
    EditText edit_search;
    String course_title="",prof="",class_period="",class_cnt="",class_state="",class_kind="",class_type="",class_total="",class_subject="";
    TextView tv_hourtime, tv_usetime;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lecture_info);
        prof = getIntent().getExtras().getString("prof");
        course_title = getIntent().getExtras().getString("course_title");
        class_period = getIntent().getExtras().getString("class_period");
        class_cnt = getIntent().getExtras().getString("class_cnt");
        class_state = getIntent().getExtras().getString("class_state");
        class_kind = getIntent().getExtras().getString("class_kind");
        class_type = getIntent().getExtras().getString("class_type");
        class_total = getIntent().getExtras().getString("class_total");
        class_subject = getIntent().getExtras().getString("class_subject");
        if(class_type.equals("normal"))
            class_type="일반";
        else if(class_type.equals("time1"))
            class_type="시간제쿠폰 1형";
        else if(class_type.equals("time2"))
            class_type="시간제쿠폰 2형";
        else if(class_type.equals("time3"))
            class_type="시간제쿠폰 3형";

        self=this;
        _api=new Adapter();
        context=this;
        callback=this;
        loadUserInfo();
        ((TextView)findViewById(R.id.tv_apptitle)).setText("강의 정보");
        ((TextView)findViewById(R.id.tv_info_course)).setText(course_title);
        ((TextView)findViewById(R.id.tv_info_prof)).setText(prof);
        ((TextView)findViewById(R.id.tv_info_studytime)).setText(class_period);
        ((TextView)findViewById(R.id.tv_info_classcnt)).setText(class_cnt+"강");
        ((TextView)findViewById(R.id.tv_info_classstate)).setText(class_state);
        ((TextView)findViewById(R.id.tv_info_classtype)).setText(class_type);
        ((TextView)findViewById(R.id.tv_info_classkind)).setText(class_kind);
        ((TextView)findViewById(R.id.tv_info_classtotal)).setText(class_total+"시간");
        ((TextView)findViewById(R.id.tv_info_subject)).setText(class_subject);
        ((ImageView)findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_goback);
        ((ImageView)findViewById(R.id.btn_back)).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        finish();
        super.onClick(v);
    }
}
