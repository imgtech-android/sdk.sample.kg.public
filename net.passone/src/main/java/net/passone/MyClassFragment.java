package net.passone;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Browser;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import net.passone.adapter.Adapter;
import net.passone.adapter.AdapterItemManager;
import net.passone.adapter.Api;
import net.passone.adapter.CourseAdapter;
import net.passone.adapter.DBmanager;
import net.passone.adapter.OnResponseListener;
import net.passone.adapter.PackageAdapter;
import net.passone.adapter.WaitDialog;
import net.passone.common.CUser;
import net.passone.common.FirstPageFragmentListener;
import net.passone.common.IntentModelFragment;
import net.passone.common.StaticVars;
import net.passone.common.Util;
import net.passone.container.ContinueInfo;
import net.passone.container.ContinueLeclInfo;
import net.passone.container.CourseInfo;
import net.passone.container.CourseItem;
import net.passone.container.PackageInfo;
import net.passone.container.PackageItem;
import net.passone.container.PlayResultInfo;
import net.passone.container.Version;

import org.andlib.helpers.Logger;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;


public class MyClassFragment extends IntentModelFragment implements IntentDataDefine,OnResponseListener,FirstPageFragmentListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    static FirstPageFragmentListener firstPageListener;
    //2018.05.24추가.int의 Default값 0.
    private int mOpenCounter;

    Adapter _api;
    ArrayList<PackageItem> pkgList;
    Context context;
    OnResponseListener callback;
    PackageAdapter pAdapter;
    CourseAdapter cAdapter;

    ListView listView;
    DBmanager db_manager;
    SQLiteDatabase db;
    ExpandableListView list_package;
    int pIndex=0;
    ContinueLeclInfo selectItem;
    boolean isContinueDown=false;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @SuppressLint("StaticFieldLeak")
    private static MyClassFragment instance;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LectureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyClassFragment newInstance(String param1, String param2) {
        MyClassFragment fragment = new MyClassFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    /**
     * 생성자
     */
    public MyClassFragment() {
        super();
    }

    /**
     * MainFragment 반환
     * @return  MainFragment
     */
    public static MyClassFragment getInstance() {

        if (instance == null) {
            instance = new MyClassFragment();
        }

        return instance;
    }


    //2018.05.24추가

    public synchronized SQLiteDatabase openDatabase() {
        mOpenCounter++;
        if(mOpenCounter == 1) {
            // Opening new database
            db = db_manager.getWritableDatabase();
        }
        return db;
    }
    //2018.05.24추가
    public synchronized void closeDatabase() {
        mOpenCounter--;
        if(mOpenCounter == 0) {
            // Closing database
            db.close();

        }
    }

//화면 잠금, 홀드 버튼으로 onDestry되고 화면 잠금 해제 할 때, onCreateView 발생.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_myclass, container, false);

        _api=new Adapter();
        context=getActivity();
        callback=this;
        //2018.05.24추가
        mOpenCounter++;
        openDatabase();
        db_manager = new DBmanager(getActivity(), "UserInfo.db");
        db = db_manager.getReadableDatabase();
        loadUserInfo();
        ((TextView)rootView.findViewById(R.id.tv_apptitle)).setText("내강의실");

        ((ImageView)rootView.findViewById(R.id.btn_back)).setVisibility(View.VISIBLE);
        ((ImageView)rootView.findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_t_downmanage);
        ((ImageView)rootView.findViewById(R.id.btn_back)).setOnClickListener(this);
        if(CUser.userid.equals("mobileguest"))
            ((ImageView)rootView.findViewById(R.id.btn_rightmenu)).setVisibility(View.GONE);
        else
        {
            ((ImageView)rootView.findViewById(R.id.btn_rightmenu)).setVisibility(View.VISIBLE);
            ((ImageView)rootView.findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_keepwatch);
            ((ImageView)rootView.findViewById(R.id.btn_rightmenu)).setOnClickListener(this);
        }


//        lectureAdapter=new MyLectureAdapter(context, StaticVars.myLectureItems);
        list_package=(ExpandableListView)rootView.findViewById(R.id.list_package);
        pkgList=new ArrayList<PackageItem>();
        list_package.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                Util.debug("test group click");
                if(pkgList.get(groupPosition).getCourseList().size()==0)
                {
                    PackageItem courseItem=pkgList.get(groupPosition);

                    startActivity(new Intent(getActivity(), LectureDetailActivity.class).putExtra("course", courseItem.getUid()).putExtra("package", "<SINGLE>"));

                }
                return false;
            }
        });
        list_package.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                CourseItem courseItem=pkgList.get(groupPosition).getCourseList().get(childPosition);
                startActivity(new Intent(getActivity(),LectureDetailActivity.class).putExtra("course", courseItem.getUid()).putExtra("package", courseItem.getPackage_id()));
                return false;
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        pIndex=0;
        Util.debug("onResume");
        if(CUser.userid.length()==0)
            loadUserInfo();
        if(_api==null)
            _api=new Adapter();
        if(pkgList.size()==0)
            _api.MyPackage(CUser.userid, context, callback);
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_back:
                startActivity(new Intent(getActivity(),DownLectureActivity.class).putExtra("mode","myclass"));
                break;
            case R.id.btn_rightmenu:
                _api.playContinue(CUser.userid,context,callback);
                break;
        }
        super.onClick(v);
    }

    @Override
    public void onResponseReceived(int api, Object result) {
        Util.debug("Login result    :   " + result);
//        try{
        switch(api) {
            case Api.PACKAGE:
                if (result != null) {
                    AdapterItemManager.AddPackageList((List<PackageInfo>) result);
                    pkgList.clear();

                    if (StaticVars.packageItems.size() > 0) {
//                      new PackageAsync(context).execute();
                        pIndex = 0;
                        PackageItem item = StaticVars.packageItems.get(pIndex);
                        _api.MyPackageCourse(CUser.userid, item.getUid(), item.getOrderid(), context, callback);
                    } else {
                        _api.MyCourse(CUser.userid, context, callback);
                    }

                }
                break;
            case Api.PACKAGECOURSE:

                if (result != null) {
                    Util.debug("pIndex:" + pIndex + ",size:" + StaticVars.packageItems.size());

                    if (pIndex < StaticVars.packageItems.size()) {
                        AdapterItemManager.AddPkgCourseList((List<CourseInfo>) result, StaticVars.packageItems.get(pIndex).getUid());
                        if (StaticVars.pkgcourseItems.size() > 0) {
                            PackageItem pitem = StaticVars.packageItems.get(pIndex);

                            pitem.setCourseList(StaticVars.pkgcourseItems);
                            pkgList.add(pitem);

                        }
                        Util.debug("package list size:" + pkgList.size());
                        pIndex++;

                        if (StaticVars.packageItems.size() > pIndex) {

                            PackageItem item = StaticVars.packageItems.get(pIndex);

                            _api.MyPackageCourse(CUser.userid, item.getUid(), item.getOrderid(), context, callback);
                        } else {
                            _api.MyCourse(CUser.userid, context, callback);

                        }
                    }

                }

                break;
            case Api.COURSE:
                if (result != null) {
                    AdapterItemManager.AddCourseList((List<CourseInfo>) result);
                    if (StaticVars.courseItems.size() > 0) {
                        for (int i = 0; i < StaticVars.courseItems.size(); i++) {

                            PackageItem citem = StaticVars.courseItems.get(i);
                            pkgList.add(citem);
                        }
                        Util.debug("list size:" + pkgList.size());

                    }
                    pAdapter = new PackageAdapter(context, list_package, pkgList);
                    list_package.setAdapter(pAdapter);
                    pAdapter.notifyDataSetChanged();
                    _api.Version(context, callback);


                }
                break;
            case Api.PLAYCONTINUE:
                if (result != null) {
                    ContinueInfo continueInfo = (ContinueInfo) result;
                    if (continueInfo.courseid != null && continueInfo.lectureid != null) {
                        _api.ContinueLecture(continueInfo.courseid, continueInfo.lectureid, context, callback);
                    }
                    testPlayer();
                }
                break;
            case Api.CONTINUELEC:
                if (result != null) {
                    List<ContinueLeclInfo> clist = (List<ContinueLeclInfo>) result;
                    if (clist.size() > 0) {
                        selectItem = clist.get(0);
                        _api.playStream(selectItem.uid, selectItem.orderid, CUser.userid, selectItem.courseid, selectItem.etc, context, callback);

                    }
                }
                break;
            case Api.PLAYSTREAM:
                Log.d("qwer123456","에이피아이값:" + api);
                PlayResultInfo playResult = (PlayResultInfo) result;
                if (playResult.result.equals("OK")) {
                    /*final Intent intentSubActivity =
                            new Intent(context, YoondiskPlayerActivity.class);*/
                    Cursor cursor = db.query("download", null, "lecture=? and courseid=? and leccode=?", new String[]{selectItem.uid, selectItem.courseid, String.valueOf(selectItem.leccode)}, null, null, null);
                    File continueFile = new File(Util.getFilePath(getActivity().getApplicationContext(), "") + selectItem.courseid.replace(":","_") + "_" + selectItem.leccode + "_" + selectItem.uid + ".mp4");
                    if (cursor.getCount() > 0 && continueFile.exists()) {
                       // intentSubActivity.putExtra("vurl", "file://" + Util.getFilePath(getActivity().getApplicationContext(), "") + selectItem.courseid.replace(":","_") + "_" + selectItem.leccode + "_" + selectItem.uid + ".mp4");
                        //intentSubActivity.putExtra("playtype", "download");
                        isContinueDown = true;
                    } else {
                        isContinueDown = false;

                       // intentSubActivity.putExtra("vurl", playResult.url.replace("\\/", "/"));
//						intentSubActivity.putExtra("vurl","http://heesomobile.wjpass.hd.stream.scs.skcdn.co.kr/ssamplussp/15/03/11ks-ir/11ks-ir-1504001.mp4?SM4558_38087044");
                        //intentSubActivity.putExtra("playtype", "stream");
                    }
                    cursor.close();


                    //intentSubActivity.putExtra("title", selectItem.title);
                   // intentSubActivity.putExtra("uid", selectItem.uid);
                   // intentSubActivity.putExtra("price", true);
                   // intentSubActivity.putExtra("position", selectItem.currentTime);
                   // intentSubActivity.putExtra("etc", playResult.etc);


                    if (!Util.isWifiConnected(context) && !isContinueDown) {
                        Util.alert(context, getString(R.string.app_name), getString(R.string.video_3g_alert), "확인", "취소", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                                //startActivity(intentSubActivity);

                            }
                        }, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                    } else{

                    }
                        //startActivity(intentSubActivity);
                } else {
                    Util.alert(context, "재생 안내", playResult.message + " 강의목록에서 직접 선택 후 재생해주세요", "확인", null, null, null);

                }
                break;
            case Api.VERSION:
                if (result != null) {
                    final Version ver = (Version)result;

                        if (ver.version.trim().length() > 0) {

                             if (mOpenCounter == 0 ) {
                                 openDatabase();
                             }

                            Cursor c = db.query("appinfo", new String[]{"version", "notice", "isfirst"}, null, null, null, null, null);
                            String str_current = "";
                            int notice = 0, isfirst = 1;
                            final ContentValues cv = new ContentValues();

                            if (ver.servercheck == 0) {

                                try {
                                    if (c.getCount() != 0) {
                                        c.moveToFirst();
                                        for (int i = 0; i < c.getCount(); i++) {
                                            str_current = c.getString(0);
                                            notice = c.getInt(1);
                                            isfirst = c.getInt(2);
                                            Util.debug("isfirst" + isfirst);
                                            c.moveToNext();
                                        }

                                    } else {
                                        str_current = Util.getVersion(getActivity());
                                    }
                                } catch (Exception e) {
                                }
                                c.close();
                                Util.debug(str_current);

                                if (notice < ver.regdate) {

                                    if (ver.contents.length() > 0) {
                                        Util.alert(getActivity(), "공지사항", ver.contents, "확인", null, null, null);
                                        cv.put("notice", ver.regdate);
                                        int update = db.update("appinfo", cv, null, null);

                                        if (update == 0) {
                                            db.insert("appinfo", "", cv);
                                        }
                                    }
                                }


                            }


                        } else {
                            Util.ToastMessage(getActivity(), "버전 정보 로딩 실패");

                        }
                } else {
                    // 수신, 파싱 오류
                    Util.PopupMessage(getActivity(), getResources().getString(R.string.api_http_alert));
                }
                break;



        }
//        } catch(Exception e)
//        {
//            Util.ToastMessage(getActivity(),"다시 시도해주세요.");
//        }
    }

    public static android.support.v4.app.Fragment newInstance(FirstPageFragmentListener firstPageFragmentListener) {
        MyClassFragment fragment = new MyClassFragment();
        return fragment;
    }

    @Override
    public void onSwitchToNextFragment() {
        firstPageListener.onSwitchToNextFragment();

    }


    public class PackageAsync extends AsyncTask<String, String, String> {


        private Context mContext;
        String filePath="";
        WaitDialog dialog;

        public PackageAsync(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            dialog= WaitDialog.show(mContext, "", "");


            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {




            // 수행이 끝나고 리턴하는 값은 다음에 수행될 onProgressUpdate 의 파라미터가 된다
            return null;
        }

        @Override
        protected void onProgressUpdate(String... progress) {
//			 if (progress[0].equals("progress")) {
//				 	dialog.setProgress(Integer.parseInt(progress[1]));
//				 	dialog.setMessage(progress[2]);
//			    } else if (progress[0].equals("max")) {
//			    	dialog.setMax(Integer.parseInt(progress[1]));
//			    }
        }

        @Override
        protected void onPostExecute(String unused) {
            dialog.dismiss();


        }
    }

    /**
     * 테스트 앱 - 스트리밍 실행
     */
    private void testPlayer() {
        Log.d("qwer123456","겟패키지네임값:" + getActivity());
        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(getString(R.string.scheme_kg))
                .authority(getString(R.string.host_player))
                .appendQueryParameter(SITE_ID, MainActivity.KG_ID)

                // info-url 및 data 는 아래 샘플 참조해서 고객사에서 설정
                .appendQueryParameter(INFO_URL, "http://m.imgtech.co.kr/mobile/kg/test/info_url.php")
                .appendQueryParameter(DATA, "play;guest;이명학.Prestart");

        Logger.d(uriBuilder.toString());

        // 플레이어 실행
        Intent intent;
        try {
            // Intent Scheme 실행
            intent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);

            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getActivity().getPackageName());

            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        //2018.05.24추가
        //자동 잠금, 홀드 버튼으로 잠금시 onDestroy 발생.
        closeDatabase();
        super.onDestroy();
    }
}
