package net.passone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.passone.common.Util;
import net.passone.container.FreeDetailItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

public class FreeLectureAdapter extends BaseAdapter {
	Context context;
	ArrayList<FreeDetailItem> leclist;
	long firstdate=0;
	boolean mode_down=false;
	public Hashtable<Integer, View> hashConvertView = new Hashtable<Integer, View>();
	private String courseCode="",leccode="";


	public FreeLectureAdapter(Context context, ArrayList<FreeDetailItem> list) {
		this.context=context;
		this.leclist=list;
	}
	public void setList(ArrayList<FreeDetailItem> list)
	{
		this.leclist=list;
	}

	public int getCount() {
		// TODO Auto-generated method stub

		return leclist.size();
	}


	public FreeDetailItem getItem(int position) {
		// TODO Auto-generated method stub
		return leclist.get(position);
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}
	public void setMode(boolean mode)
	{
		mode_down=mode;
	}
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final FreeDetailItem item;
			item=leclist.get(position);
		ViewHolder holder=new ViewHolder();

		if (hashConvertView.containsKey(position) == false) {
			convertView = (LinearLayout) LayoutInflater.from(context).inflate(
					R.layout.free_lecture_cell, parent, false);
			//			convertView = mInflater.inflate(R.layout.wronglist_cell, null);

			holder = new ViewHolder();
			holder.chk_lec = (CheckBox) convertView.findViewById(R.id.chk_lec);
			holder.tv_lectitle=(TextView)convertView.findViewById(R.id.tv_lectitle);
			holder.iv_downstate=(ImageView)convertView.findViewById(R.id.iv_downstate);
			convertView.setTag(holder);
			hashConvertView.put(position, convertView);
			convertView.setTag(R.id.chk_lec, holder.chk_lec);

		} else {
			convertView = (View) hashConvertView.get(position);

			holder = (ViewHolder) convertView.getTag();
		}
		if(mode_down)
		{
			holder.chk_lec.setVisibility(View.VISIBLE);

		}
		else
			holder.chk_lec.setVisibility(View.GONE);

		String filepath=courseCode+"_"+leccode+"_"+item.getLecDtlCode()+".mp4";
		//String filePath= Util.getFilePath(context, filepath);
		String filePath= Util.getFilePath(context.getApplicationContext(), filepath);
		Util.debug("filepath:"+filePath);

		File dfile=new File(filePath);

		if(dfile.isFile() && dfile.exists())
		{
			Util.debug("filepath:"+dfile.exists());
			holder.iv_downstate.setVisibility(View.VISIBLE);

			if(item.getIsDown()>0)
				holder.iv_downstate.setImageResource(R.drawable.ico_downok);
			else
				holder.iv_downstate.setImageResource(R.drawable.ico_downstop);

		}
		else {
			holder.iv_downstate.setVisibility(View.INVISIBLE);

		}
		holder.tv_lectitle.setText(item.getLecInfo());
		holder.chk_lec.setId(position);
		holder.chk_lec.setChecked(item.isSelected());
		holder.chk_lec.setFocusable(false);
		holder.chk_lec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				item.setSelected(isChecked);
				((FreeLectureDetailActivity)context).countChk(isChecked);

			}
		});
		return convertView;
	}
	
public void goDetail(int position)
{
    final FreeDetailItem item;
    item=leclist.get(position);

}

	public void setCourseId(String courseId, String leccode) {
		this.courseCode = courseId;
		this.leccode=leccode;

	}

	static class ViewHolder {
		CheckBox chk_lec;
		TextView tv_lectitle;
		ImageView iv_downstate;

	}
}
