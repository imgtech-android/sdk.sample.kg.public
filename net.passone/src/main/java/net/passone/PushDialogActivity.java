package net.passone;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;

import net.passone.common.CUser;
import net.passone.common.Util;


public class PushDialogActivity extends Activity implements OnClickListener {

	private String msg="",type="",imgurl="",msg_img="";
	PushDialogActivity self;
    boolean isApprun=false;
	@Override
	public void onCreate(Bundle savedInstanceState) {


		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Bundle bun = getIntent().getExtras();
        if(bun!=null) {
            if (bun.getString("msg_type") != null) {
                type = bun.getString("msg_type");

            }
            if (bun.getString("msg_imgrul") != null) {
                imgurl = bun.getString("msg_imgrul");
            }
            if (bun.getString("msg") != null) {
                msg = bun.getString("msg");
            }
            if (bun.getString("msg_img") != null) {
                msg_img = bun.getString("msg_img");
            }
            isApprun = bun.getBoolean("isApprun");

        }
		self=this;
        if(type.equals("T"))
        {
            Util.analyticsSend(self, "푸시 알림", "텍스트 푸시", "ID: " + CUser.userid);

//            setContentView(R.layout.popdefaultdialog);
//            ((ImageButton)findViewById(R.id.btn_push_close)).setOnClickListener(self);
//            ImageButton btn_submit = (ImageButton)findViewById(R.id.btn_push_view);
//            btn_submit.setOnClickListener(self);
//            ((TextView)findViewById(R.id.tv_push_msg)).setText(msg);

        }
        else
        {
            Util.analyticsSend(self, "푸시 알림", "이미지 푸시", "ID: " + CUser.userid);

//            setContentView(R.layout.popimagedialog);
//            ((ImageButton)findViewById(R.id.btn_closex)).setOnClickListener(self);
//            ((SmartImageView)findViewById(R.id.iv_popup)).setOnClickListener(self);
//            if(msg_img!=null && msg_img.length()>0)
//            {
//                ((SmartImageView)findViewById(R.id.iv_popup)).setImageUrl(msg_img);
//            }


        }




	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
//		case R.id.btn_closex:
//			finish();
//			break;
//		case R.id.btn_push_view:
//            if(!isApprun)
//			    startActivity(new Intent(self, IntroActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//			finish();
//			break;
//        case R.id.iv_popup:
//            Util.debug("pop imgurl:" + imgurl);
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(imgurl));
//            startActivity(intent);
//            finish();
//            break;
//            case R.id.btn_push_close:
//                finish();
//                break;
		default:
			break;
		}
	}

}
