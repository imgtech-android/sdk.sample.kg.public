package net.passone;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import net.passone.adapter.DBmanager;
import net.passone.common.IntentModelActivity;

public class PassVocaDayResultActivity extends IntentModelActivity implements OnClickListener {
	private static String[] filters = {"", "isfinish = 1", "isfinish = 0" };
	DBmanager db_manager;
	SQLiteDatabase db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.passvoca_day_complete);
		final int day = getIntent().getExtras().getInt("day");
		((TextView)findViewById(R.id.tv_apptitle)).setText("Day " + day + " 완료");
		((TextView)findViewById(R.id.tv_comp_day)).setText(day + "");
		((ImageView)findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_goback);
		((ImageView)findViewById(R.id.btn_back)).setOnClickListener(this);
		db_manager = new DBmanager(this,"UserInfo.db");
		db = db_manager.getWritableDatabase();
		Cursor yesCursor=db.query("voca",null,"day=? and isfinish=1",new String[]{String.valueOf(day)},null,null,null);
		Cursor noCursor=db.query("voca",null,"day=? and isfinish=0",new String[]{String.valueOf(day)},null,null,null);
		int yesCount=yesCursor.getCount();
		int noCount=noCursor.getCount();
		((TextView)findViewById(R.id.tv_memorycnt)).setText(yesCount+"");
		((TextView)findViewById(R.id.tv_nomemory_cnt)).setText(noCount+"");
		yesCursor.close();
		noCursor.close();
		final int filterType = getIntent().getExtras().getInt("filterType");
		String filter = filters[filterType];
		Cursor daycursor = db.query("voca", new String[] {"max(day)"}, filter, null, null, null, null);
		daycursor.moveToFirst();
		int lastDay = daycursor.getInt(0);
		daycursor.close();
		Cursor finishcursor = db.query("voca", new String[] {"sum(isfinish)"}, filter, null, null, null, null);
		finishcursor.moveToFirst();
		int finishedCount = finishcursor.getInt(0);
		daycursor.close();
		finishcursor.close();

		int messageId = 0;

		findViewById(R.id.btn_rememory).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(PassVocaDayResultActivity.this, PassVocaStudyActivity.class);
				intent.putExtra("day", day);
				intent.putExtra("filterType", filterType);
				startActivity(intent);
				finish();
			}
		});

		if (day < lastDay) {
			findViewById(R.id.btn_nextmemory).setVisibility(View.VISIBLE);
			findViewById(R.id.btn_nextmemory).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(PassVocaDayResultActivity.this, PassVocaStudyActivity.class);
					intent.putExtra("day", day + 1);
					intent.putExtra("filterType", filterType);
					startActivity(intent);
					finish();
				}
			});
		} else {
			findViewById(R.id.btn_nextmemory).setVisibility(View.GONE);
		}
	}

	@Override
	protected void onDestroy() {
		if(db!=null)
			db.close();
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
			case R.id.btn_back:
				finish();
				break;
		}
		super.onClick(v);
	}
}
