package net.passone;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import net.passone.adapter.DBmanager;
import net.passone.adapter.OnResponseListener;
import net.passone.adapter.VocaWeekAdapter;
import net.passone.common.IntentModelActivity;
import net.passone.container.VocaItem;

import java.util.ArrayList;

public class PassVocaWeekActivity extends IntentModelActivity implements OnClickListener,OnResponseListener {
	Context context;
	DBmanager db_manager;
	SQLiteDatabase db;
	ImageButton btn_tab_all,btn_tab_eng,btn_tab_kor;
	private static String[] filters = { "", "and isfinish = 1", "and isfinish = 0" };
	private String filter,dayfilter;
	private int view_flag = 0; // 0:초기화면;영어만, 1: 모두보기, 2: 한글만
	private boolean sound_flag=true;
	private MediaPlayer mMediaPlayer;
	VocaWeekAdapter vadapter;
	ListView list_week;
	private ArrayList<VocaItem> vocaList = new ArrayList<VocaItem>();
	private int filterType = 0,week=0;
	Typeface typeface;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.voca_weekstudy);
		context=this;
		week = getIntent().getExtras().getInt("week");
		filter = filters[getIntent().getExtras().getInt("filterType")];

		btn_tab_all=(ImageButton)findViewById(R.id.btn_voca_viewall);
		btn_tab_eng=(ImageButton)findViewById(R.id.btn_voca_vieweng);
		btn_tab_kor=(ImageButton)findViewById(R.id.btn_voca_viewkor);
		btn_tab_all.setSelected(true);
		list_week=(ListView)findViewById(R.id.list_week);
		btn_tab_all.setOnClickListener(this);
		btn_tab_eng.setOnClickListener(this);
		btn_tab_kor.setOnClickListener(this);
		db_manager = new DBmanager(this, "UserInfo.db");
		db = db_manager.getWritableDatabase();
		setDaybyWeek(week);
		((TextView) findViewById(R.id.tv_apptitle)).setText("Week " + week
				+ " (" + ((week - 1) * 300 + 1) + "~" + vocaList.size() * week + ")");
		view_flag=1;
		list_week.setItemsCanFocus(true);
		list_week.setFocusable(false);
		list_week.setFocusableInTouchMode(false);
		list_week.setClickable(false);
		((ImageButton)findViewById(R.id.btn_back)).setOnClickListener(this);
		typeface = Typeface.createFromAsset(getAssets(), "playregular.ttf");
		((TextView)findViewById(R.id.tv_apptitle)).setTypeface(typeface);
		vadapter=new VocaWeekAdapter(context,vocaList);
		list_week.setAdapter(vadapter);
		loadDatabase();
		vadapter.notifyDataSetChanged();

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
			case R.id.btn_voca_viewall:
				view_flag=1;
				btn_tab_all.setSelected(true);
				btn_tab_eng.setSelected(false);
				btn_tab_kor.setSelected(false);

				loadDatabase();
				break;
			case R.id.btn_voca_vieweng:
				view_flag=0;
				btn_tab_all.setSelected(false);
				btn_tab_eng.setSelected(true);
				btn_tab_kor.setSelected(false);
				loadDatabase();

				break;
			case R.id.btn_voca_viewkor:
				view_flag=2;
				loadDatabase();
				btn_tab_all.setSelected(false);
				btn_tab_eng.setSelected(false);
				btn_tab_kor.setSelected(true);
				break;
			case R.id.btn_back:
				finish();
				break;
		}
		super.onClick(v);
	}

	@Override
	protected void onDestroy() {
		if(db!=null)
			db.close();
		super.onDestroy();
	}

	public void loadDatabase()
	{
//		cursor = db.query("ZVOCABULARY", new String[] {"ZDAY", "sum(ZFINISH) finished", "sum(ZSEEN)"}, "ZUSER_ID = ?", new String[] { User.currentUser(context).getUserID() }, "ZDAY", getFilter(), "ZDAY");
		Cursor cursor;
		cursor = db.query("voca", new String[] {"day", "uid","vindex","word","desc","meaning","seen","isfinish"}, dayfilter+filter, null,null,null, null);

		try {
			vocaList.clear();
			Log.d("passone", "db count=" + cursor.getCount());
			if (cursor.getCount()> 0) {
				cursor.moveToFirst();
				for(int i=0; i<cursor.getCount();i++)
				{
					VocaItem vitem=new VocaItem(cursor.getString(1),cursor.getInt(0),cursor.getInt(2),cursor.getString(3),cursor.getString(5),cursor.getString(4),cursor.getInt(6),cursor.getInt(7));
					switch(view_flag)
					{
						case 0:
								vitem.setWordVisible(1);
								vitem.setMeanVisible(0);

							break;
						case 1:
							vitem.setWordVisible(1);
							vitem.setMeanVisible(1);
							break;
						case 2:
							vitem.setWordVisible(0);
							vitem.setMeanVisible(1);
							break;

						default:
							break;
					}
					vocaList.add(vitem);
					cursor.moveToNext();

				}
			}
			((TextView) findViewById(R.id.tv_apptitle)).setText("Week " + week
					+ " (" + ((week - 1) * 300 + 1) + "~" + vocaList.size() * week + ")");
			vadapter.notifyDataSetChanged();
	}

	catch(Exception e) {
		}
		vadapter.notifyDataSetChanged();
		cursor.close();
	}
	protected void setFilterIndex(int i) {
		this.filterType = i;
	}

	protected int getFilterIndex() {
		return filterType;
	}

	protected String getFilter() {
		return filters[filterType];
	}
	public void setDaybyWeek(int week)
	{
		dayfilter=" day in ("+(week*5-4)+", "+(week*5-3)+", "+(week*5-2)+", "+(week*5-1)+", "+(week*5)+")";

	}
}