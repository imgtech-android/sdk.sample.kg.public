package net.passone;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gcm.GCMRegistrar;

import net.passone.adapter.Api;
import net.passone.adapter.DBmanager;
import net.passone.adapter.HttpHelper;
import net.passone.adapter.OnResponseListener;
import net.passone.common.AES256Cipher;
import net.passone.common.CUser;
import net.passone.common.Constants;
import net.passone.common.Environments;
import net.passone.common.IntentModelActivity;
import net.passone.common.Util;
import net.passone.common.WebViewActivity;
import net.passone.container.ApiResult;
import net.passone.container.Version;

import org.apache.http.cookie.Cookie;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class IntroActivity extends IntentModelActivity implements OnResponseListener {
    IntroActivity self;
    DBmanager db_manager;
    SQLiteDatabase db;
    String uid="",pwd="",device_token="",aesKey="",sendId="";
    int isfirst=1, pushchk=0;
    Context context;
    OnResponseListener callback;
    HttpHelper hh=new HttpHelper();
    String movieurl="",movietitle="",imgurl="",str_current="";
    boolean goMyClass=false;
    Dialog dialog;
    ImageButton btn_pchk;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro);
        self=this;
        CookieSyncManager.createInstance(this);
        context = this;
        callback=this;
        initialize();
        String regId = GCMRegistrar.getRegistrationId(this);

        Log.d("qwer1235","씨유저 푸시체크:" + CUser.pushchk + "씨유저 디바이스 토큰랭스:" + CUser.device_token.length());
        if(CUser.pushchk>0 && CUser.device_token.length()==0)
            GCMRegistration_id();

//        if(Intent.ACTION_VIEW.equals(getIntent().getAction())){
//            Uri uri = getIntent().getData();
//            int kind = Integer.valueOf(uri.getQueryParameter("moviekind"));
//
//            Log.d("passone","kind:"+kind);
//            Log.d("passone","getQuery:"+uri.getQuery());
//            Log.d("passone","getSchemeSpecificPart:"+	uri.getSchemeSpecificPart());
//            Log.d("passone","getScheme:"+	uri.getScheme());
//            Log.d("passone","getHost:"+	uri.getHost());
//            Log.d("passone","getQueryParameter:"+	uri.getQueryParameter("kind"));
//            switch (kind) {
//                case 2:
//                    String url=uri.getQueryParameter("url");
//                        startActivity(new Intent(this,YoondiskPlayerActivity.class).putExtra("vurl",url).putExtra("price", false));
//                    break;
//
//                default:
//                    break;
//            }
//        }
        Bundle bun = getIntent().getExtras();
        if(bun!=null) {
            if (bun.getString("imgurl") != null) {
                imgurl = bun.getString("imgurl");
                Util.debug("intent imgurl:"+imgurl);

            }

        }
        if(new File(Util.getDownloadPath("voca/")).exists())
        {
            Util.debug(Util.getDownloadPath("voca/"));
        }
        if(isfirst==0) {
            if(Double.valueOf(str_current)<4.51)
            {
                showDialog();

            }
            else {
                if(CUser.pushchk==2) //오류로 값이 반영되지 않음.
                {
                    Util.alert(self, "알림", getString(R.string.event_push_agree), "동의", "동의안함", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            pushchk = 1;
                            _api.RegisterApi("Y", "Y", CUser.device_token, CUser.device_token, self, callback);
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            pushchk = 0;
                            _api.RegisterApi("Y", "N", CUser.device_token, CUser.device_token, self, callback);
                        }
                    });
                }
                else {
                    start();
                }
            }
        }
        else
        {

            Util.alert(this, "환영합니다!", "스마트패스원은 네트워크 환경에서만 이용하실 수 있습니다."
                    + "\n\n * 다운로드된 강의를 재생할 때도 사용자 인증, 진도율 등의 확인을 위한 데이터 통신 필요", "시작하기", null, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    showDialog();


                }

            }, null);

        }


    }
    public void setPushchk(int pushchk)
    {
        if(pushchk==2)
            Util.ToastMessage(this,"서버와의 통신에 일시적으로 문제가 발생되었습니다. 차후 다시 시도해주세요.");

        DBmanager db_manager= new DBmanager(this,Constants.DATABASE_NAME);
        SQLiteDatabase db=db_manager.getReadableDatabase();
        ContentValues cv= new ContentValues();

        cv.put("pushchk", pushchk);
        int pushcnt=db.update("device",cv,null,null);
        if(pushcnt==0) {
            String sql = "insert into device (device_token,pushchk) values('',"+pushchk+")";
            db.execSQL(sql);
        }
        db.close();
        CUser.pushchk=pushchk;
        start();
    }
    private void showDialog() {
        // TODO Auto-generated method stub
        dialog = new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.permission_dialog);
        dialog.setCancelable(false);
        //there are a lot of settings, for dialog, check them all out!

        btn_pchk = (ImageButton) dialog.findViewById(R.id.btn_pchk);
        btn_pchk.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                btn_pchk.setColorFilter(Color.DKGRAY);
                dialog.dismiss();
                if(isfirst>0)
                {

                    Util.alert(self, "알림", getString(R.string.event_push_agree), "동의", "동의안함", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            pushchk = 1;
                            _api.RegisterApi("Y", "Y", CUser.device_token, CUser.device_token, self, callback);
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            pushchk = 0;
                            _api.RegisterApi("Y", "N", CUser.device_token, CUser.device_token, self, callback);
                        }
                    });
                }
                else
                {
                    start();

                }
            }

        });

        dialog.show();
    }
    public void start() {
        //		startPushService();

        (new Handler()).postDelayed(new Runnable() {
            public void run() {
                if (Intent.ACTION_VIEW.equals(getIntent().getAction())) {

                    getIntentInfo(getIntent());


                } else {
                    Log.d("passone", uid);
                    _api.Version(context, callback);
                }


            }
        }, 2000);	/* 2 seconds */

    }

    public void goMain() {
        cookieManager = CookieManager.getInstance();
        List<Cookie> cookies=hh.getCookies();
        Cookie sessionInfo = null;
        Util.debug("uid=" + CUser.mno);

        for (Cookie cookie : cookies ) {
            sessionInfo = cookie;
            String cookieString = sessionInfo.getName() + "="
                    + sessionInfo.getValue() + "; path="
                    + sessionInfo.getPath()+"; domain="+sessionInfo.getDomain();
            Util.debug("s"+cookieString);
            cookieManager.setCookie(sessionInfo.getDomain(),cookieString);

            CookieSyncManager.getInstance().sync();
        }

        CookieSyncManager.getInstance().startSync();
        Bundle bun = getIntent().getExtras();

        Intent i = new Intent(this,MainActivity.class);
        if(imgurl.length()>0)
            i.putExtra("url",imgurl);
        Util.debug("imgurl:"+imgurl);
        startActivity(i);
        finish();
    }
    public void goLogin() {
        Intent i = new Intent(this,LoginActivity.class);

        startActivity(i);
        finish();
    }
    /**
     * �����?������ ����<br>
     * Manifaest�� debuggable ���¸� ���?br>
     */
    public void initializeDebuggable() {
        Constants.debuggable =
                (0 != (getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE));
    }

    /**
     * �ɼ� ���� ������ �ҷ��´�. �������� �ʴ� ���?�ʱⰪ�� ����<br>
     */
    public void loadEnvironments() {
        Environments.load(this);
        if (!Environments.PREFERENCE_EXIST) {
            Environments.save(this);
        }
    }
    public void initialize() {

        initializeDebuggable();
        context=this;
        callback=this;

        loadEnvironments();

        DBmanager db_manager= new DBmanager(this,Constants.DATABASE_NAME);
        SQLiteDatabase db=db_manager.getReadableDatabase();
        Cursor tw_cursor = db.query("userinfo", new String[] {"mno","userid","autochk","nick","pushchk","userpw"}, "autochk=?", new String[] {"1"}, null, null, null);
        //	values.put("uid", StaticVars.uid);

        try {
            Log.d("passone","db count="+tw_cursor.getCount());
            if (tw_cursor.getCount()!= 0) {
                tw_cursor.moveToFirst();
                for(int i=0; i<tw_cursor.getCount();i++)
                {
                    CUser.mno=tw_cursor.getString(0);
                    CUser.userid=tw_cursor.getString(1);
                    CUser.autochk=tw_cursor.getInt(2);
                    CUser.nicname=tw_cursor.getString(3);
                    pwd=tw_cursor.getString(5);
                    tw_cursor.moveToNext();
                }



            }
        } catch(Exception e) {
        }

        tw_cursor.close();
        Cursor device_cursor = db.query("device", new String[] {"pushchk","device_token"}, null, null, null, null, null);
        //	values.put("uid", StaticVars.uid);

        try {
            Log.d("passone","db count="+device_cursor.getCount());
            if (device_cursor.getCount()!= 0) {
                device_cursor.moveToFirst();
                for(int i=0; i<device_cursor.getCount();i++)
                {
                    CUser.device_token=device_cursor.getString(1);
                    CUser.pushchk=device_cursor.getInt(0);
                    Util.debug("jumprate:"+CUser.pushchk);
                    device_cursor.moveToNext();
                }



            }
        } catch(Exception e) {
        }

        device_cursor.close();
        Cursor appinfo_cursor = db.query("appinfo", new String[] {"jumprate","useSD", "isfirst","version"}, null, null, null, null, null);
        //	values.put("uid", StaticVars.uid);

        try {
            Log.d("passone","db count="+appinfo_cursor.getCount());
            if (appinfo_cursor.getCount()!= 0) {
                appinfo_cursor.moveToFirst();
                for(int i=0; i<appinfo_cursor.getCount();i++)
                {
                    CUser.jumprate=appinfo_cursor.getInt(0);
                    isfirst=appinfo_cursor.getInt(2);
                    if(appinfo_cursor.getInt(1)==0)
                        CUser.isExternal=false;
                    else
                        CUser.isExternal=true;
                    str_current=appinfo_cursor.getString(3);

                    appinfo_cursor.moveToNext();
                }



            }
        } catch(Exception e) {
        }

        appinfo_cursor.close();

        if(db!=null)
            db.close();

    }
    public void GCMRegistration_id()
    {

//        GCMRegistrar.checkDevice(this);
//        GCMRegistrar.checkManifest(this);
        final String regId = GCMRegistrar.getRegistrationId(this);
        Log.i("passone", "intro registration id =====&nbsp; " + regId);

        if (regId.equals("") || regId ==null) {
            Log.i("passone", "registration id null");
            GCMRegistrar.register(this, Constants.senderID);
        }
        else {
            _api.DeviceInfo(CUser.device_token, CUser.device_token, context, callback);
            Util.debug("registration_id length="+regId.length());
            CUser.device_token=regId;
            System.out.println("registration_id complete!!");
            db_manager = new DBmanager(context,"UserInfo.db");
            db = db_manager.getWritableDatabase();
            String sql = "delete from device";
            db.execSQL(sql);
            sql = "insert into device (device_token) values('"+regId+"')";
            db.execSQL(sql);
            db.close();
            Log.v("passone", "Already registered");
        }

    }

    public void startPushService()
    {
        //C2DM ���ID �߱�
        Log.d("passone", "start push");

        Intent registrationIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
        registrationIntent.putExtra("app", PendingIntent.getBroadcast(this, 0, new Intent(), 0)); // ���ø����̼�ID
        registrationIntent.putExtra("sender", "365258868387"); //������ID
        startService(registrationIntent); //���� ����(���ID�߱޹ޱ�)
        // ������ ������ "app"�� "sender"�� �����?�����Ͻô°� �ƴ϶� ���ۿ��� �ʿ��� ��������Դϴ�?
        //	getUserInfo(self);


    }
    @Override
    public void onResponseReceived(int api, Object result) {
        // TODO Auto-generated method stub
        Util.debug("Login result    :   " + result);
        switch(api) {
            case Api.REGISTERAPI:
                if(result!=null)
                {
                    ApiResult regapi = (ApiResult) result;
                    Calendar calendar = Calendar.getInstance();
                    Date date = calendar.getTime();
                    String nowtime = new SimpleDateFormat("yyyy년 MM월 dd일").format(date);
                    String process="";
                    if(pushchk==0)
                        process="알림 수신거부 처리 완료";
                    else if(pushchk==1)
                        process="알림 수신동의 처리 완료";
                    if(regapi.result.trim().equals("OK"))
                    {
                        Util.alert(this, "광고성 푸시 알림 수신 동의 안내", "1. 전송자: KG에듀원 \n\n"
                                + "2. 처리 일자: "+nowtime+"\n\n"
                                + "3. 처리 내용: 해당 기기 "+process+"\n\n"
                                +"설정 메뉴에서 알림 설정 변경 가능합니다.", "확인", null, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                setPushchk(pushchk);
                            }

                        }, null);
                    }
                    else
                    {
                        setPushchk(2);

                    }

                }
                else {
                    setPushchk(2);

                }
                break;
            case Api.SIGN:

                if (result != null) {

                    ApiResult login = (ApiResult) result;

                    if (login.result.trim().equals("OK")) {
                        cookieManager = CookieManager.getInstance();
                        List<Cookie> cookies = hh.getCookies();
                        Cookie sessionInfo = null;
                        Util.debug("mno=" + CUser.mno);


                        for (Cookie cookie : cookies) {
                            sessionInfo = cookie;
                            String cookieString = sessionInfo.getName() + "="
                                    + sessionInfo.getValue() + "; path="
                                    + sessionInfo.getPath() + "; domain=" + sessionInfo.getDomain();
                            Util.debug(cookieString);
                            if(sessionInfo.getName().equals("PASSONE_IDX"))
                            {
                                CUser.mno= URLDecoder.decode(sessionInfo.getValue());
                            }
                            if(sessionInfo.getName().equals("PASSONE_UID"))
                            {
                                CUser.userid=URLDecoder.decode(sessionInfo.getValue());
                            }

                            cookieManager.setCookie(sessionInfo.getDomain(), cookieString);
                            CookieSyncManager.getInstance().sync();
                        }
                        if (goMyClass)
                        {
                            Intent intent;
                            if(CUser.userid.equals(sendId)) {
                                intent =
                                        new Intent(context, MainActivity.class);

                            }
                            else
                            {
                                intent =
                                        new Intent(context, WebViewActivity.class);
                                Util.ToastMessage(context,"로그인한 아이디와 일치하지 않습니다.");
                                CUser.mno="";
                                CUser.userid="";
                                CUser.autochk=0;
                                CUser.nicname="";
                                CUser.pushchk=0;
                                logout();
                            }

                            startActivity(intent);
                            finish();
                            return;
                        }
                        CookieSyncManager.getInstance().startSync();
                        goMain();

                    } else {
                        Util.ToastMessage(self, login.message);

                        goLogin();
                    }

                } else {
                    Util.PopupMessage(self, getResources().getString(R.string.api_http_alert));
                    goMain();

                }


                break;
            case Api.VERSION:
                if (result != null) {
                    final Version ver = (Version)result;
                    if (ver.version.trim().length()>0) {
                        db_manager = new DBmanager(self, Constants.DATABASE_NAME);
                        db = db_manager.getWritableDatabase();
                        Cursor c = db.query("appinfo", new String[]{"version","notice"}, null, null, null, null, null);
                        str_current=Util.getVersion(self);

                        int notice=0;
                        ContentValues cv= new ContentValues();

                        if(ver.servercheck==0)
                        {
                            try {
                                if (c.getCount()!= 0) {
                                    c.moveToFirst();
                                    for(int i=0; i<c.getCount();i++)
                                    {
                                        notice=c.getInt(1);
                                        c.moveToNext();
                                    }

                                }

                            } catch(Exception e) {
                            }
                            c.close();

                            if(!str_current.equals(ver.version.trim())&& Double.valueOf(str_current)<Double.valueOf(ver.version.trim()))
                            {
                                String alert_str="현재버전: "+str_current+"\n최신버전: "+ver.version.trim();

                                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                builder.setMessage(alert_str);
                                builder.setTitle("New Version");
                                if(ver.update==0)
                                {
                                    builder.setNegativeButton("취소", new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int which) {
                                            if (!CUser.userid.equals("") && CUser.autochk == 1) {

                                                _api.Login(CUser.userid, pwd, CUser.device_token, context, callback);
                                                //					goLogin();


                                            } else {
                                                goLogin();
                                            }
                                        }
                                    });
                                }
                                builder.setPositiveButton("업데이트하기", new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        if (!ver.link.equals("")) {

                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ver.link.replace("\\/", "/"))));
                                        }
                                    }
                                });
                                AlertDialog dialog = builder.create();
                                if(ver.update>0)
                                    dialog.setCancelable(false);

                                dialog.show();

                            }
                            else {
                                if (!CUser.userid.equals("") && CUser.autochk == 1) {

                                    _api.Login(CUser.userid, pwd, CUser.device_token, context, callback);
                                    //					goLogin();


                                } else {
                                    goLogin();
                                }
                            }
                            cv.put("version", str_current);
                            cv.put("isfirst",0);

                            int update=db.update("appinfo", cv, null, null);
                            if(update==0)
                            {
                                db.insert("appinfo","",cv);
                            }
                            int verup=db.update("appinfo", cv, null, null);
                            if(verup==0)
                            {
                                db.insert("appinfo","",cv);
                            }
                            db.close();

                        }
                        else
                        {
                            String serverstr=ver.serverstr;
                            String serverStr="";
                            if(serverstr.length()>0)
                            {
                                serverStr=serverstr;
                            }
                            else
                                serverStr="서버 점검중입니다.";
                            Util.alert(self, "New Version", serverStr, "확인", null, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                        }


                    } else {
                        Util.ToastMessage(this, "버전 정보 로딩 실패");

                    }
                } else {
                    // 수신, 파싱 오류
                    Util.PopupMessage(this, getResources().getString(R.string.api_http_alert));
                }
                break;
        }
    }
    public void getIntentInfo(Intent intent)
    {
        if(Intent.ACTION_VIEW.equals(getIntent().getAction())){
            Uri uri = getIntent().getData();

            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
            final String dateString=format.format(new Date().getTime());


            String enc= AES256Cipher.encrypt("http://s01.wj.scs.skcdn.co.kr/4485/passone2011/9/i/1409/11881/11881_01017.mp4", Constants.aesKey);
            Util.debug("enc:"+enc+",key:"+Constants.aesKey);

            Util.debug("dec:"+AES256Cipher.decrypt(enc, Constants.aesKey));

            String moviekind="";
            String movieurl="";
            int kind=0;
            if(uri.getQueryParameter("kind")!=null)
                kind = Integer.valueOf(uri.getQueryParameter("kind"));
            if(uri.getQueryParameter("moviekind")!=null)
                moviekind=	uri.getQueryParameter("moviekind");
            if(uri.getQueryParameter("movieurl")!=null)
            {
                movieurl=	uri.getQueryParameter("movieurl");
                Util.debug("intent url:"+movieurl);
                try {
                    Util.debug("intent encode url:"+ URLEncoder.encode(movieurl, "UTF-8"));
                    Util.debug("intent decode url:"+ URLDecoder.decode(URLEncoder.encode(movieurl, "UTF-8"), "UTF-8"));

                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                movieurl=AES256Cipher.decrypt(movieurl, Constants.aesKey);

            }

            Log.d("passone","kind:"+kind);
            Log.d("passone","getQuery:"+uri.getQuery());
            Log.d("passone", "getSchemeSpecificPart:" + uri.getSchemeSpecificPart());
            Log.d("passone","getScheme:"+	uri.getScheme());
            Log.d("passone", "getHost:" + uri.getHost());
            Log.d("passone", "getQueryParameter:" + uri.getQueryParameter("kind"));
            switch (kind) {
                case 2:

                    String url=uri.getQueryParameter("url");
                    //startActivity(new Intent(this, YoondiskPlayerActivity.class).putExtra("vurl", url).putExtra("price", false).putExtra("title",uri.getQueryParameter("movietitle")));
                    finish();

                    break;

                default:

                    break;
            }
            if(moviekind!=null && moviekind.length()>0)
            {
                if(moviekind.equals("direct") && movieurl.length()>0)
                {
                    if (movieurl != null && movieurl.length() > 0) {

                        if (movieurl.contains(".wmv")) {

                            Util.ToastMessage(context, getString(R.string.wmv_alert));
                        }
                        else
                        {
                            //startActivity(new Intent(this, YoondiskPlayerActivity.class).putExtra("vurl", movieurl).putExtra("price", false).putExtra("title", uri.getQueryParameter("movietitle")));
                            finish();

                        }
                        return;


                    }

                }
                else
                {
                    returnBack();
                }
            }else {

                start();

            }
        }
    }
    public void returnBack()
    {
        Util.alert(self, "스마트 패스원", "해당 컨텐츠를 불러오지 못했습니다. 다시 시도해주시고, 동일 현상이 반복된다면 문의하기 게시판을 통해 문의 부탁드립니다.", "확인", null, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }, null);
    }
    @Override
    protected void onNewIntent(Intent intent) {
        // TODO Auto-generated method stub
        Util.debug("pause NewIntent");
        getIntentInfo(intent);
        super.onNewIntent(intent);
    }

}