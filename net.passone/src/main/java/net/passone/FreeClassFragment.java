package net.passone;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import net.passone.adapter.Adapter;
import net.passone.adapter.AdapterItemManager;
import net.passone.adapter.Api;
import net.passone.adapter.DBmanager;
import net.passone.adapter.FreeListAdapter;
import net.passone.adapter.OnResponseListener;
import net.passone.common.FirstPageFragmentListener;
import net.passone.common.IntentModelFragment;
import net.passone.common.StaticVars;
import net.passone.common.Util;
import net.passone.container.CourseDataInfo;
import net.passone.container.FreeInfo;
import net.passone.container.FreeItem;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LectureFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LectureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FreeClassFragment extends IntentModelFragment implements OnResponseListener,FirstPageFragmentListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    static FirstPageFragmentListener firstPageListener;

    Adapter _api;
    ArrayList<FreeItem> free_list;
    Context context;
    OnResponseListener callback;
    FreeListAdapter pAdapter;

    ListView listView;
    DBmanager db_manager;
    SQLiteDatabase db;
    ExpandableListView list_package;
    int pIndex=0;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LectureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FreeClassFragment newInstance(String param1, String param2) {
        FreeClassFragment fragment = new FreeClassFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    public FreeClassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_myclass, container, false);

        _api=new Adapter();
        context=getActivity();
        callback=this;
        loadUserInfo();
        ((TextView)rootView.findViewById(R.id.tv_apptitle)).setText("무료특강");

        ((ImageView)rootView.findViewById(R.id.btn_back)).setVisibility(View.VISIBLE);
        ((ImageView)rootView.findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_t_downmanage);
        ((ImageView)rootView.findViewById(R.id.btn_back)).setOnClickListener(this);

//        lectureAdapter=new MyLectureAdapter(context, StaticVars.myLectureItems);
        list_package=(ExpandableListView)rootView.findViewById(R.id.list_package);
        free_list=new ArrayList<FreeItem>();


        list_package.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                CourseDataInfo courseItem=free_list.get(groupPosition).getCourseData().get(childPosition);
                startActivity(new Intent(context,FreeLectureDetailActivity.class).putExtra("leccode", String.valueOf(courseItem.lecCode)).putExtra("courseCode", free_list.get(groupPosition).getCourseCode()));
                return false;
            }
        });
        _api.freeList(context, callback);

        return rootView;
    }

    @Override
    public void onResume() {

        if(_api==null)
            _api=new Adapter();


        super.onResume();
    }



    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_back:
                startActivity(new Intent(getActivity(),DownLectureActivity.class).putExtra("mode","freeclass"));
                break;
        }
        super.onClick(v);
    }

    @Override
    public void onResponseReceived(int api, Object result) {
        Util.debug("Login result    :   " + result);
        switch(api) {
            case Api.FREELIST :

                if (result != null) {

                    if (((List<FreeInfo>)result).size()>0) {
                        AdapterItemManager.AddLecture((List<FreeInfo>)result);
                        free_list=(ArrayList<FreeItem>)StaticVars.freeItems.clone();

                        pAdapter = new FreeListAdapter(getActivity(), list_package,free_list);
                        list_package.setAdapter(pAdapter);
                    }


                } else {
                }
                break;
        }
    }

    public static android.support.v4.app.Fragment newInstance(FirstPageFragmentListener firstPageFragmentListener) {
        FreeClassFragment fragment = new FreeClassFragment();
        return fragment;
    }

    @Override
    public void onSwitchToNextFragment() {
        firstPageListener.onSwitchToNextFragment();

    }

}
