package net.passone;


import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Browser;
import android.text.LoginFilter;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.passone.adapter.Api;
import net.passone.adapter.DBmanager;
import net.passone.adapter.DownLectureAdapter;
import net.passone.adapter.OnResponseListener;
import net.passone.common.CUser;
import net.passone.common.DownLoad;
import net.passone.common.IntentModelActivity;
import net.passone.common.Util;
import net.passone.container.DownloadItem;
import net.passone.container.PlayResultInfo;

import org.andlib.helpers.Logger;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;

import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;


public class DownLectureActivity extends IntentModelActivity implements IntentDataDefine,OnClickListener {
	ImageButton btn_login;
	Context context;
	OnResponseListener callback;
	Handler handler;
	ListView list_lecture;
	ArrayList<DownloadItem> lectureList=new ArrayList<DownloadItem>();
	int cindex=0,progress,totaltime,days;
	Dialog dialog;
	boolean mode_complete=false;

	DownLectureAdapter lAdapter;
	String course,beginDate,lastLectureId,title,orderid;
	LinearLayout layout_downtools;
	boolean isDown=false,mode_all=false;
	int chk_cnt=0,down_cnt=0,chk_total=0;
	long totalsize=0;
	Button btn_all, btn_down, btn_del;
	ArrayList<DownloadItem> down_list=new ArrayList<DownloadItem>();
	int recent_position=0,leccode=0;
	String filename="",tmp_file="",filepath="",uid="",etc="",packageid="",coupontype="",category="",status=""
			,enddate="",teacher="",lecturecnt="",subject="",price="",book="",table="",where="";
	private int down_no = 0;
	public String err_msg = "";
	public String down_msg = "",mode="";
	private int down_load_end=0;
	public String down_arg = "";
	DBmanager db_manager;
	SQLiteDatabase db;
	DownLoad drmdownload;
	ImageButton btn_dcancel;
	TextView tv_total;
	TextView tv_subject;
	TextView tv_progress,tv_result;
	ProgressBar down_progress,total_progress;
	DownloadItem selectItem;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.downlec_detail);
		mode = getIntent().getExtras().getString("mode");
        Log.d("qwer123456","넌뭐니?" + mode);
		if(mode.equals("myclass"))
		{
			table="download";
			where="";
		}
		else if(mode.equals("freeclass"))
		{
			table="free_download";

		}
		else
		{
			table="time_download";

		}
		((TextView)findViewById(R.id.tv_apptitle)).setText("다운로드관리");
		((ImageView)findViewById(R.id.btn_rightmenu)).setVisibility(View.VISIBLE);
		((ImageView)findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_edit);
		((ImageView)findViewById(R.id.btn_rightmenu)).setOnClickListener(this);
		((ImageView)findViewById(R.id.btn_back)).setVisibility(View.VISIBLE);
		if(mode.equals("myclass"))
			((ImageView) findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_gomyroom);
		else if(mode.equals("freeclass"))
		{
			((ImageView) findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_goback);

		}
		else
			((ImageView) findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_gotime);

		((ImageView)findViewById(R.id.btn_back)).setOnClickListener(this);


		context=this;
		callback = this;
		db_manager = new DBmanager(this,"UserInfo.db");
		db=db_manager.getReadableDatabase();

		btn_all=(Button)findViewById(R.id.btn_allselect);
		btn_all.setOnClickListener(this);
		btn_del=(Button)findViewById(R.id.btn_delete);

		btn_del.setOnClickListener(this);
		layout_downtools=(LinearLayout)findViewById(R.id.layout_downtools);

		list_lecture=(ListView)findViewById(R.id.list_lecture);
		list_lecture.setItemsCanFocus(true);
		list_lecture.setFocusable(false);
		list_lecture.setFocusableInTouchMode(false);
		list_lecture.setClickable(false);
		list_lecture.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				DownloadItem item = lectureList.get(position);
				CheckBox checkbox = (CheckBox) view.getTag(R.id.chk_lec);
				Util.debug("click:" + position);

				if (layout_downtools.getVisibility() == View.VISIBLE) {
					checkbox.setChecked(!checkbox.isChecked());
					item.setSelected(checkbox.isChecked());
					countChk(checkbox.isChecked());
					//					 dadapter.chkItem(position);
				} else {
					selectItem=item;
					if(mode.equals("freeclass"))
					{
						/*final Intent intentSubActivity =
								new Intent(context, YoondiskPlayerActivity.class);

							intentSubActivity.putExtra("vurl",  "file://"+selectItem.getFilepath());
							intentSubActivity.putExtra("playtype", "download");

						intentSubActivity.putExtra("title", selectItem.getTitle());
						intentSubActivity.putExtra("uid", selectItem.getUid());
						intentSubActivity.putExtra("price", false);
						intentSubActivity.putExtra("position", 0);
						intentSubActivity.putExtra("etc", "");

							startActivity(intentSubActivity);*/
						testPlayer();
					}
					else
						_api.playStream(item.getUid(), item.getOrderId(), CUser.userid, item.getCourseId(), item.getEtc(), context, callback);

				}

			}
		});
		super.onCreate(savedInstanceState);
	}
	public void loadLecture()
	{

		lectureList.clear();
		Cursor cursor;
		Util.debug("mno:"+ CUser.userid);
		if(mode.equals("freeclass"))
		{

			cursor = db.query(table,new String[]{"leccode","lecdtlcode","courseid","filename","course_title","lec_title"},"mno=?",new String[]{CUser.mno},null,null,null);
			try {

				if(cursor.getCount()>0)
				{
					cursor.moveToFirst();
					btn_del.setEnabled(true);
					for(int i=0; i<cursor.getCount();i++)
					{
						DownloadItem lecItem=new DownloadItem(cursor.getString(1), cursor.getString(5),"", cursor.getString(2), cursor.getInt(0), "",cursor.getString(3), cursor.getString(4));
						lectureList.add(lecItem);
						cursor.moveToNext();
					}
				}
				else
					btn_del.setEnabled(false);
			} catch(Exception e) {
			}
		}
		else
		{
			cursor = db.query(table,new String[]{"etc","leccode","lecture","courseid","filename","course_title","lec_title","orderid"},"mno=?",new String[]{CUser.mno},null,null,null);
			try {

				if(cursor.getCount()>0)
				{
					cursor.moveToFirst();
					btn_del.setEnabled(true);
					for(int i=0; i<cursor.getCount();i++)
					{
						DownloadItem lecItem=new DownloadItem(cursor.getString(2), cursor.getString(6),cursor.getString(7), cursor.getString(3), cursor.getInt(1), cursor.getString(0), cursor.getString(4), cursor.getString(5));
						if(lecItem.getFilepath()!=null)
							lectureList.add(lecItem);
						Util.debug("leccode:"+cursor.getInt(1)+", lecture:"+cursor.getString(2)+", courseid:"+cursor.getString(3));
						Util.debug("filename:"+cursor.getString(3));

						cursor.moveToNext();
					}
				}
				else
					btn_del.setEnabled(false);
			} catch(Exception e) {
			}
		}

		lAdapter=new DownLectureAdapter(context,lectureList);
		lAdapter.setMode(isDown);
		list_lecture.setAdapter(lAdapter);
		lAdapter.notifyDataSetChanged();
		cursor.close();

	}
	@Override
	public void onResponseReceived(int api, Object result) {
		Util.debug("Login result    :   " + result);
		switch(api) {
			case Api.PLAYSTREAM:
				PlayResultInfo playResult = (PlayResultInfo)result;
				if(playResult.result.equals("OK")) {
                    testPlayer();
					/*final Intent intentSubActivity =
							new Intent(context, YoondiskPlayerActivity.class);

					intentSubActivity.putExtra("vurl", "file://" +  selectItem.getFilepath());
					intentSubActivity.putExtra("playtype", "download");

					intentSubActivity.putExtra("title", selectItem.getTitle());
					intentSubActivity.putExtra("uid", selectItem.getUid());
					intentSubActivity.putExtra("price", true);
					intentSubActivity.putExtra("position", playResult.currentTime);
					intentSubActivity.putExtra("etc", playResult.etc);*/


					if (!Util.isWifiConnected(context) && selectItem.getIsDown() == 0) {
						Util.alert(context, getString(R.string.app_name), getString(R.string.video_3g_alert), "확인", "취소", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int which) {
								//startActivity(intentSubActivity);

							}
						}, new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int which) {
							}
						});
					} else{

                    }
						//startActivity(intentSubActivity);
				}
				else
				{
					Util.alert(context, "재생 안내", playResult.message, "확인", null, null, null);

				}
				break;


			case Api.LECDOWN:
				PlayResultInfo resultInfo=(PlayResultInfo)result;
				if(resultInfo.result.equals("OK"))
				{
					String vodUrl=resultInfo.url.replace("\\/","/");
					etc=resultInfo.etc;
					down_arg=vodUrl;

					if(dialog==null){}
					if(down_progress!=null)
					{
						total_progress.setProgress(down_cnt);
						tv_total.setText("전체 ( "+down_cnt+"/"+down_list.size()+" )");
						tv_subject.setText(filename);
					}
//					//다운로드 기능 선언부.
//					drmdownload=  new DownLoad();
//					drmdownload.setCommunicator(this);
//	                 /*파일 다운로드 시작 url , 로컬저장경로 . */
//					String filePath=Util.getFilePath(context, filepath);
//					Util.debug("down filepath:"+filePath);
//
//					drmdownload.drm_download_start(down_arg, filePath);
				}
				else
				{
					Util.ToastMessage(this,resultInfo.message);
					if(dialog!=null)
					{
						dialog.dismiss();

					}
				}
				break;

		}
	}

	@Override
	protected void onResume() {

		loadLecture();
		super.onResume();
	}




	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
			case R.id.btn_rightmenu:
				isDown=!isDown;

				if(isDown)
				{
					findViewById(R.id.layout_downtools).setVisibility(View.VISIBLE);
					((ImageButton)findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_ok);
					lAdapter.setMode(true);

				}
				else {
					findViewById(R.id.layout_downtools).setVisibility(View.GONE);
					((ImageButton)findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_edit);

					lAdapter.setMode(false);

				}
				break;
			case R.id.btn_allselect:
				mode_all=!mode_all;
				chk_cnt=0;
				Util.debug("all click" + mode_all);
				if(mode_all)
				{
					for(DownloadItem item:lectureList)
					{
						item.setSelected(true);
						countChk(true);
					}
					btn_all.setText("선택해제");
				}
				else
				{
					for(DownloadItem item:lectureList)
					{
						item.setSelected(false);

						countChk(false);
					}
					down_list.clear();
					down_cnt=0;
					chk_cnt=0;
					btn_all.setText("전체선택");
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						lAdapter.setList(lectureList);
						lAdapter.notifyDataSetChanged();

					}
				});

				break;
			case R.id.btn_delete:
				Util.alert(this, "다운로드 삭제", "삭제하시겠습니까?", "확인", "취소", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						makeChkList(false);

					}
				}, null);
				break;


			case R.id.btn_back:
				finish();
				break;
		}
		super.onClick(v);
	}
	public void countChk(boolean ischk)
	{


		if(ischk)
		{
			chk_cnt++;
		}
		else
		{
			if(chk_cnt>0)
				chk_cnt--;
		}
		if(chk_cnt==0)
		{


		}
		else
		{


			if(chk_total==chk_cnt)
			{
				btn_all.setText("선택해제");
			}
			else
			{
				btn_all.setText("전체선택");
			}

		}

	}
	public void makeChkList(boolean isdown) {
		down_list.clear();

		int size=lectureList.size();
		Util.debug("isdown:" + size);

		for(DownloadItem item:lectureList)
		{
			Util.debug("item:" + item.isSelected());

			if(item.isSelected())
			{
				down_list.add(item);
				Util.debug("item:" + item.getUid());
			}


		}

		if(isdown) //다운로드
		{

			if(down_list.size()>0)
			{
				down_cnt=0;
			}
		}
		else //삭제
		{

			for(DownloadItem lec : down_list)
			{
				delDownload(lec.getFilepath(),lec.getUid(),lec.getCourseId(),lec.getLeccode());
			}
			loadLecture();

			clearDown();
			Util.ToastMessage(context, "삭제되었습니다.");
//			isDown=!isDown;
//			findViewById(R.id.layout_downtools).setVisibility(View.GONE);
//			((ImageButton)findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_edit);
//
//			lAdapter.setMode(false);

		}

	}
	public void clearDown()
	{
		for (DownloadItem item : lectureList)
		{
			item.setSelected(false);
			countChk(false);

		}
		Util.debug("dd");
		lAdapter.notifyDataSetChanged();
		down_list.clear();
		down_cnt=0;
		chk_cnt=0;
		if(dialog!=null && dialog.isShowing())
		{
			dialog.dismiss();
			dialog=null;

		}
	}

	public void delDownload(String filename, String cseq, String courseid, int leccode)
	{

		String ext = Environment.getExternalStorageState();
		String Save_folder =filename;

		Util.debug("file exist:" +  filename);

		File file = new File(filename);
		if(file.isFile() && file.exists())
		{
			if(file.delete())
			{
			}
		}
		int delete=0;
		if(mode.equals("myclass"))
			delete=db.delete(table, "lecture=? and courseid=? and leccode=? ", new String[]{String.valueOf(cseq), String.valueOf(courseid), String.valueOf(leccode)});
		else if(mode.equals("freeclass"))
			delete=db.delete("free_download","lecdtlcode=? and leccode=? and courseid=?",new String[]{String.valueOf(cseq), String.valueOf(leccode),courseid});
		else
			delete=db.delete(table, "lecture=? and courseid=?", new String[]{String.valueOf(cseq), String.valueOf(courseid)});

		Util.debug("update:" + leccode+","+cseq+","+courseid+table);
	}




	@Override
	protected void onDestroy() {
		if(db!=null)
			db.close();
		super.onDestroy();
	}

	/*
     테스트 앱 - 스트리밍 실행
     */

    private void testPlayer() {

        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(getString(R.string.scheme_kg))
                .authority(getString(R.string.host_player))
                .appendQueryParameter(SITE_ID, MainActivity.KG_ID)

                // info-url 및 data 는 아래 샘플 참조해서 고객사에서 설정
                .appendQueryParameter(INFO_URL, "http://m.imgtech.co.kr/mobile/kg/test/info_url.php")
                .appendQueryParameter(DATA, "play;guest;이명학.Prestart");

        Logger.d(uriBuilder.toString());

        // 플레이어 실행
        Intent intent;
        try {
            // Intent Scheme 실행
            intent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);

            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getApplication().getPackageName());
            intent.setPackage(getApplication().getPackageName());

            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}