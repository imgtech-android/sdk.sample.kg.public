package net.passone;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import net.passone.adapter.Adapter;
import net.passone.adapter.AdapterItemManager;
import net.passone.adapter.Api;
import net.passone.adapter.DBmanager;
import net.passone.adapter.OnResponseListener;
import net.passone.adapter.TimeAdapter;
import net.passone.common.CUser;
import net.passone.common.IntentModelFragment;
import net.passone.common.StaticVars;
import net.passone.common.Util;
import net.passone.container.CouponInfo;
import net.passone.container.TimeCourseInfo;
import net.passone.container.TimeCourseItem;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LectureFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LectureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TimeFragment extends IntentModelFragment implements OnResponseListener, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Adapter _api;
    ArrayList<TimeCourseItem> timeList;
    ArrayList<TimeCourseItem> search_timeList;
    Context context;
    OnResponseListener callback;
    TimeAdapter tAdapter;
    ListView listView;
    DBmanager db_manager;
    SQLiteDatabase db;
    boolean mode_search=false;
    LinearLayout layout_search, layout_timetab,layout_timeinfo;
    ImageView iv_timenext;
    EditText edit_search;
    String remainedText="",boughtText="";
    int remained=0,bought=0;
    TextView tv_hourtime, tv_usetime;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private InputMethodManager imm;
    ImageButton btn_rightmenu;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LectureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TimeFragment newInstance(String param1, String param2) {
        TimeFragment fragment = new TimeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public TimeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onResume() {
        if(CUser.userid.length()==0)
            loadUserInfo();
        if(_api==null)
            _api=new Adapter();
        if(edit_search.getText().toString().trim().length()==0)
        _api.MyTimeCourse(getActivity(),this);

        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_timeclass, container, false);

        _api=new Adapter();
        context=getActivity();
        callback=this;
        loadUserInfo();
        ((TextView)rootView.findViewById(R.id.tv_apptitle)).setText("시간제강의");
        imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//        lectureAdapter=new MyLectureAdapter(context, StaticVars.myLectureItems);
        timeList=new ArrayList<TimeCourseItem>();
        search_timeList=new ArrayList<TimeCourseItem>();
//        _api.MyPackage(CUser.userid, context, callback);
        listView=(ListView)rootView.findViewById(R.id.list_time);
        ((ImageButton)rootView.findViewById(R.id.btn_timetab_position)).setOnClickListener(this);
        ((ImageButton)rootView.findViewById(R.id.btn_timetab_all)).setOnClickListener(this);
        ((ImageButton)rootView.findViewById(R.id.btn_timetab_lecture)).setOnClickListener(this);
        ((ImageButton)rootView.findViewById(R.id.btn_timetab_professor)).setOnClickListener(this);
        btn_rightmenu=((ImageButton)rootView.findViewById(R.id.btn_rightmenu));
        btn_rightmenu.setVisibility(View.VISIBLE);
        btn_rightmenu.setImageResource(R.drawable.btn_t_lecsearch);
        btn_rightmenu.setOnClickListener(this);
        ((ImageView)rootView.findViewById(R.id.btn_back)).setVisibility(View.VISIBLE);
        ((ImageView)rootView.findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_t_downmanage);
        ((ImageView)rootView.findViewById(R.id.btn_back)).setOnClickListener(this);
        iv_timenext=(ImageView)rootView.findViewById(R.id.iv_timenext);

        tv_hourtime=(TextView)rootView.findViewById(R.id.tv_hourtime);
        tv_usetime=(TextView)rootView.findViewById(R.id.tv_usetime);
        layout_timeinfo=(LinearLayout)rootView.findViewById(R.id.layout_timeinfo);
        layout_timeinfo.setOnClickListener(this);
        ((ImageButton)rootView.findViewById(R.id.btn_t_search)).setOnClickListener(this);

        layout_search=(LinearLayout)rootView.findViewById(R.id.layout_search);
        layout_timetab=(LinearLayout)rootView.findViewById(R.id.layout_timetab);
        edit_search=(EditText)rootView.findViewById(R.id.edit_search);
        edit_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        searchLecture(edit_search.getText().toString());
                        imm.hideSoftInputFromWindow(edit_search.getWindowToken(), 0);


                        return true;

                    default:
                        return false;
                }
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TimeCourseItem item = timeList.get(position);
                startActivity(new Intent(getActivity(), TimeLectureDetailActivity.class).putExtra("teacher", item.getTeacher()).putExtra("title", item.getTitle()).putExtra("beginDate", item.getBeginDate())
                        .putExtra("lectureCount", String.valueOf(item.getLectureCount())).putExtra("status", item.getStatus())
                        .putExtra("category", item.getCategory()).putExtra("couponType", item.getCouponType()).putExtra("totalTime", item.getTotalTime())
                        .putExtra("subject", item.getSubject()).putExtra("book", item.getBook()).putExtra("price", item.getPrice())
                        .putExtra("etc", item.getEtc()).putExtra("days", item.getDays()).putExtra("course", item.getUid()));

            }
        });
        return rootView;
    }


    @Override
    public void onResponseReceived(int api, Object result) {
        Util.debug("Login result    :   " + result);
        switch(api) {

            case Api.TIMECOURSE:

                if (result != null) {
                    AdapterItemManager.AddTimeCourseList((List<TimeCourseInfo>) result);
                    if(StaticVars.timecourseItems.size()>0)
                    {
                        timeList=(ArrayList<TimeCourseItem>) StaticVars.timecourseItems.clone();
                        tAdapter=new TimeAdapter(context,timeList);
                        listView.setAdapter(tAdapter);
                    }

                }
                _api.MyCoupon(CUser.userid,context,callback);
                break;
            case Api.COUPON:

                if (result != null) {
                    List<CouponInfo> couponList=((List<CouponInfo>) result);
                    remained=0; bought=0;
                    if(couponList.size()>0)
                    {
                        for(CouponInfo cinfo:couponList)
                        {
                            remained+=cinfo.remainedTime;
                            bought+=cinfo.boughtTime;
                        }
                        layout_timeinfo.setVisibility(View.VISIBLE);
                        layout_timeinfo.setEnabled(true);

                        tv_usetime.setText(Util.secondToTime(remained) + "/" + Util.secondToTime(bought));
                        tv_hourtime.setText(secondTohour(bought)+"시간");
                        iv_timenext.setVisibility(View.VISIBLE);

                    }
                    else
                    {
//                        layout_timeinfo.setVisibility(View.GONE);
                        tv_hourtime.setText("이 없습니다.");
                        layout_timeinfo.setEnabled(false);
                        iv_timenext.setVisibility(View.GONE);
                    }


                }
                break;

        }
    }
    private final static Comparator<TimeCourseItem> categoryComparator= new Comparator<TimeCourseItem>() {

        private final Collator collator = Collator.getInstance();
        @Override

        public int compare(TimeCourseItem object1, TimeCourseItem object2) {
            return collator.compare(object1.getCategory(), object2.getCategory());

        }
    };
    private final static Comparator<TimeCourseItem> profComparator= new Comparator<TimeCourseItem>() {

        private final Collator collator = Collator.getInstance();
        @Override

        public int compare(TimeCourseItem object1, TimeCourseItem object2) {
            return collator.compare(object1.getTeacher(), object2.getTeacher());

        }
    };
    private final static Comparator<TimeCourseItem> lecComparator= new Comparator<TimeCourseItem>() {

        private final Collator collator = Collator.getInstance();
        @Override

        public int compare(TimeCourseItem object1, TimeCourseItem object2) {
            return collator.compare(object1.getSubject(), object2.getSubject());

        }
    };
    public void searchLecture(String search)
    {
        timeList.clear();
        for(TimeCourseItem titem: StaticVars.timecourseItems)
        {
            if(titem.getTitle().contains(search) || titem.getTeacher().contains(search))
            {
                timeList.add(titem);
            }
        }
        tAdapter.setList(timeList);
        tAdapter.notifyDataSetChanged();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_timetab_all:
                timeList.clear();
                timeList.addAll((ArrayList<TimeCourseItem>) StaticVars.timecourseItems);
                tAdapter.setList(timeList);
                tAdapter.notifyDataSetChanged();

                break;
            case R.id.btn_timetab_position:
                Collections.sort(timeList, categoryComparator);
                tAdapter.setList(timeList);
                tAdapter.notifyDataSetChanged();
                break;
            case R.id.btn_timetab_lecture:
                tAdapter.setList(timeList);
                Collections.sort(timeList, lecComparator);
                tAdapter.notifyDataSetChanged();

                break;
            case R.id.btn_timetab_professor:
                Collections.sort(timeList, profComparator);
                tAdapter.setList(timeList);
                tAdapter.notifyDataSetChanged();

                break;
            case R.id.btn_rightmenu:
                if(mode_search)
                {
                    layout_timetab.setVisibility(View.VISIBLE);
                    layout_search.setVisibility(View.GONE);
                    btn_rightmenu.setImageResource(R.drawable.btn_t_lecsearch);
                    edit_search.setText("");
                    imm.hideSoftInputFromWindow(edit_search.getWindowToken(), 0);
                    timeList.clear();
                    timeList.addAll((ArrayList<TimeCourseItem>) StaticVars.timecourseItems);
                    tAdapter.setList(timeList);
                    tAdapter.notifyDataSetChanged();


                }
                else
                {
                    layout_timetab.setVisibility(View.GONE);
                    layout_search.setVisibility(View.VISIBLE);
                    btn_rightmenu.setImageResource(R.drawable.btn_t_cancle);

                }
                mode_search=!mode_search;

                break;
            case R.id.layout_timeinfo:

                    startActivity(new Intent(getActivity(),TimeDetailActivity.class));
                break;
            case R.id.btn_back:
                startActivity(new Intent(getActivity(),DownLectureActivity.class).putExtra("mode","timeclass"));
                break;
            case R.id.btn_t_search:
                searchLecture(edit_search.getText().toString());
                imm.hideSoftInputFromWindow(edit_search.getWindowToken(), 0);
                break;
        }
        super.onClick(v);
    }

    public String secondTohour(int seconds)
    {
        int hr = seconds/3600;

        String hrStr = String.valueOf(hr);

        return hrStr;
    }
}
