package net.passone;


import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

import net.passone.adapter.Api;
import net.passone.adapter.DBmanager;
import net.passone.adapter.HttpHelper;
import net.passone.adapter.OnResponseListener;
import net.passone.common.CUser;
import net.passone.common.Constants;
import net.passone.common.DownLoad;
import net.passone.common.IntentModelFragment;
import net.passone.common.Util;
import net.passone.common.WebViewActivity;
import net.passone.container.ApiResult;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class SettingFragment extends IntentModelFragment implements OnClickListener,DownLoad.Communicator, OnResponseListener {
	ImageButton btn_login, btn_dcancel;
	Context context;
	CheckBox chk_push,chk_sd;
	LinearLayout layout_gochange, layout_manage, layout_change;
	String simplepwd = "", sdCardDir = "";
	DBmanager db_manager;
	SQLiteDatabase db;
	ProgressBar progress_internal,progress_sd;
	TextView tv_usein,tv_usesd;
	int jumprate[] = {5000, 10000, 15000, 30000, 60000};
	Dialog dialog;
	final String jumpNames[] = {"5초", "10초", "15초", "30초", "60초"};
	int choicePos = 0,pushchk=0;
	Typeface typeface;
	ProgressBar down_progress;
	DownLoad drmdownload;
	int mainTime=1;
	long bytetotal=0;
	TimerTask task;
	Handler handler;
	Timer mTimer;
	String testurl="http://mobilehd.wjpass.hd.stream.scs.skcdn.co.kr/test/test.mp4";
	String filePath="";
	Button btn_sddelete;
	OnResponseListener callback;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView=inflater.inflate(R.layout.setting, container, false);
		((TextView)rootView.findViewById(R.id.tv_userid)).setText(CUser.userid);
		context=getActivity();
		callback=this;
		db_manager = new DBmanager(context,"UserInfo.db");
		db = db_manager.getWritableDatabase();
		handler = new Handler();
		chk_push=((CheckBox) rootView.findViewById(R.id.chk_push));

		chk_sd=((CheckBox) rootView.findViewById(R.id.chk_sd));

		progress_internal=((ProgressBar)rootView.findViewById(R.id.progress_internal));
		tv_usein=((TextView)rootView.findViewById(R.id.tv_usein));
		progress_sd=((ProgressBar)rootView.findViewById(R.id.progress_sd));
		tv_usesd=((TextView)rootView.findViewById(R.id.tv_usesd));
		btn_sddelete=((Button)rootView.findViewById(R.id.btn_sddelete));
		((TextView)rootView.findViewById(R.id.tv_apptitle)).setText("설정");
		((ImageView)rootView.findViewById(R.id.btn_back)).setVisibility(View.GONE);

		((Button)rootView.findViewById(R.id.btn_indelete)).setOnClickListener(this);
		((Button)rootView.findViewById(R.id.btn_sddelete)).setOnClickListener(this);

		((Button)rootView.findViewById(R.id.btn_logout)).setOnClickListener(this);
		((Button)rootView.findViewById(R.id.btn_speedtest)).setOnClickListener(this);
		((LinearLayout)rootView.findViewById(R.id.layout_faq)).setOnClickListener(this);
		((LinearLayout)rootView.findViewById(R.id.layout_qna)).setOnClickListener(this);
		((LinearLayout)rootView.findViewById(R.id.layout_notice)).setOnClickListener(this);

		((TextView)rootView.findViewById(R.id.tv_version)).setText("Version " + Util.getVersion(getActivity()));
		init();
		return rootView;
	}
	public void init()
	{
		loadUserInfo();
		filePath= Util.getFilePath(context, "test.mp4");

		if(Build.VERSION.SDK_INT< Build.VERSION_CODES.KITKAT)
		{
			if(Util.getMicroSDCardDirectory()==null)
				sdCardDir=null;
			else
				sdCardDir= Util.getMicroSDCardDirectory()+"/"+getString(R.string.api_prefix)+"/data";
		}
		else {
			if(getActivity().getExternalFilesDirs(null).length<=1)
				sdCardDir=null;

			else
			{
				if(getActivity().getExternalFilesDirs(null).length>1)
				{

					if(getActivity().getExternalFilesDirs(null)[1]!=null)
					{
						sdCardDir=getActivity().getExternalFilesDirs(null)[1].toString();
					}
					else {
						sdCardDir=null;
					}
				}
				else
					sdCardDir=null;
			}


		}
		if(CUser.pushchk==1)
			chk_push.setChecked(true);
		else
			chk_push.setChecked(false);
		chk_push.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!chk_push.isChecked()) {
					pushchk = 0;
					_api.RegisterApi("N","N", CUser.device_token, CUser.device_token,context,callback);

				} else {

					pushchk = 1;
					_api.RegisterApi("N", "Y", CUser.device_token, CUser.device_token, context, callback);



				}
			}
		});
		if(sdCardDir!=null && !getActivity().getExternalFilesDir(null).toString().contains(sdCardDir))
			chk_sd.setChecked(CUser.isExternal);
		else {
			chk_sd.setChecked(false);

		}
		chk_sd.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (sdCardDir == null || getActivity().getExternalFilesDir(null).toString().contains(sdCardDir)) {
					Toast.makeText(getActivity(), "SD카드가 장착되어있지 않습니다.", Toast.LENGTH_LONG).show();
					buttonView.setChecked(false);
					return;
				}
				else
				{
					ContentValues cv= new ContentValues();
					CUser.isExternal = isChecked;
					if(CUser.isExternal)
						cv.put("useSD",1);
					else
						cv.put("useSD",0);
					cv.put("version", Util.getVersion(context));

					int update=db.update("appinfo",cv,null,null);
					Util.debug("update sd :" + update);
					if(update==0)
						db.insert("appinfo","",cv);
					/**
					 * GCM에 디바이스토큰 삭제
					 */
					if (GCMRegistrar.isRegistered(context)) {
						GCMRegistrar.unregister(context);

					}
				}

			}
		});
		progress_internal.setMax((int) (Util.getExteranlMemoryTotalSize() / Util.MegaBytes));
		progress_internal.setProgress((int) (Util.getExteranlMemoryUseSize() / Util.MegaBytes));
		String ava_size=new DecimalFormat("###.##").format((double) Util.getExteranlMemoryAvailableSize()/(double) Util.GigaBytes);
		String save_Path = context.getExternalFilesDir(null).toString();
		String use_size = new DecimalFormat("###.##").format(folderMemoryCheck(save_Path) / (double) Util.GigaBytes);

		tv_usein.setText(ava_size + "GB 사용가능 (" + use_size + "GB 사용 중)");

		if(sdCardDir!=null)
		{

			progress_sd.setMax((int) (Util.getSDCardMemoryTotalSize(context) / Util.MegaBytes));
			progress_sd.setProgress((int) (Util.getSDCardMemoryUseSize(context) / Util.MegaBytes));
			String sd_ava_size=new DecimalFormat("###.##").format((double) Util.getSDCardMemoryAvailableSize(context) / (double) Util.GigaBytes);
			use_size = new DecimalFormat("###.##").format(folderMemoryCheck(sdCardDir) / (double) Util.GigaBytes);

			tv_usesd.setText(sd_ava_size + "GB 사용가능 ("+use_size+"GB 사용 중)"	);
			if((double) Util.getSDCardMemoryAvailableSize(context)<0)
			{
				tv_usesd.setText("SD카드가 없습니다.");
				btn_sddelete.setVisibility(View.GONE);

			}

		}
		else {
			progress_sd.setMax(100);
			progress_sd.setProgress(0);
			tv_usesd.setText("SD카드가 없습니다.");
			btn_sddelete.setVisibility(View.GONE);

		}
	}
	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.btn_indelete:
				Util.alert(getActivity(), getString(R.string.app_name), "동영상 파일을 전부 삭제하시겠습니까?", "확인", "취소", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						String save_Path = context.getExternalFilesDir(null).toString();
						File dir = new File(save_Path);

						String[] children = dir.list();
						if (children != null) {
							for (int i = 0; i < children.length; i++) {
								String filename = children[i];
								Util.debug("save_Path:"+save_Path + filename);

								File f = new File(save_Path + "/"+ filename);

								if (f.exists()) {
									f.delete();
								}

							}//for
							File vocadir=new File(Util.getDownloadPath("voca/"));
							String[] voca_children = vocadir.list();
							if (voca_children != null) {
								for (int i = 0; i < voca_children.length; i++) {
									String filename = voca_children[i];

									new File(vocadir, voca_children[i]).delete();


								}//for
							}
							progress_internal.setProgress((int) (Util.getExteranlMemoryUseSize() / Util.MegaBytes));
							String ava_size = new DecimalFormat("###.##").format((double) Util.getExteranlMemoryAvailableSize() / (double) Util.GigaBytes);
							String use_size = new DecimalFormat("###.##").format(folderMemoryCheck(save_Path) / (double) Util.GigaBytes);

							tv_usein.setText(ava_size + "GB 사용가능 ("+use_size+"GB 사용 중)");
							int delete = 0;
							db.delete("download", "filename like ? ", new String[]{"%" + save_Path + "%"});
							db.delete("free_download", "filename like ? ", new String[]{"%" + save_Path + "%"});
							db.delete("time_download", "filename like ? ", new String[]{"%" + save_Path + "%"});


						}//if
					}
				}, new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
					}
				});


				break;
			case R.id.btn_sddelete:
				if(sdCardDir!=null)
				{
					Util.alert(getActivity(), getString(R.string.app_name), "동영상 파일을 전부 삭제하시겠습니까?", "확인", "취소", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							String save_Path = sdCardDir;
							File dir = new File(save_Path);

							String[] children = dir.list();
							if (children != null) {
								for (int i = 0; i < children.length; i++) {
									String filename = children[i];
									File f = new File(save_Path + "/"+filename);
									Util.debug("delete sd file:"+save_Path +"/"+ filename);
									if (f.exists()) {
										f.delete();
									}

								}//for

								progress_sd.setMax((int) (Util.getSDCardMemoryTotalSize(context) / Util.MegaBytes));
								progress_sd.setProgress((int) (Util.getSDCardMemoryUseSize(context) / Util.MegaBytes));
								String sd_ava_size=new DecimalFormat("###.##").format((double) Util.getSDCardMemoryAvailableSize(context) / (double) Util.GigaBytes);
								String use_size = new DecimalFormat("###.##").format(folderMemoryCheck(sdCardDir) / (double) Util.GigaBytes);

								tv_usesd.setText(sd_ava_size + "GB 사용가능 (" + use_size + "GB 사용 중)");
								int delete = 0;
								db.delete("download", "filename like ? ", new String[]{"%" + sdCardDir + "%"});
								db.delete("free_download", "filename like ? ", new String[]{"%"+sdCardDir+"%"});
								db.delete("time_download", "filename like ? ", new String[]{"%"+sdCardDir+"%"});



							}//if
						}
					}, new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {

						}
					});
				}
				else{
					Util.ToastMessage(getActivity(),"SD카드가 없습니다.");
				}

				break;
			case R.id.btn_logout:
				DBmanager db_manager= new DBmanager(context,Constants.DATABASE_NAME);
				SQLiteDatabase db=db_manager.getReadableDatabase();
				HttpHelper hh= new HttpHelper();
				hh.clearCookie();
				CookieManager cookieManager = CookieManager.getInstance();
				cookieManager.removeAllCookie();
				CUser.mno="";
				CUser.userid="";
				CUser.username="";
				String sql = "delete from userinfo";
				db.execSQL(sql);
				if(db!=null) db.close();
				startActivity(new Intent(getActivity(),LoginActivity.class));
				getActivity().finish();
				break;
			case R.id.btn_speedtest:
				testDownload();
				break;
			case R.id.layout_faq:
				startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", Constants.faqUrl).putExtra("title", "FAQ"));

				break;
			case R.id.layout_qna:
				startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url",Constants.qnaUrl).putExtra("title", "문의하기"));

				break;
			case R.id.layout_notice:
				startActivity(new Intent(getActivity(), WebViewActivity.class).putExtra("url", String.format(Constants.noticeUrl, getString(R.string.api_prefix), "V12")+"smart.html").putExtra("title", "공지사항"));

				break;
		}

		super.onClick(v);
	}

	public void GCMRegistration_id()
	{

		GCMRegistrar.checkDevice(getActivity());
		GCMRegistrar.checkManifest(getActivity());
		final String regId = GCMRegistrar.getRegistrationId(getActivity());
		Log.i("passone", "registration id =====&nbsp; " + regId);

		if (regId.equals("") || regId ==null) {
			Log.i("passone", "registration id null");
			GCMRegistrar.register(getActivity(), Constants.senderID);
		}
		else {
			Util.debug("registration_id length=" + regId.length());
			CUser.device_token=regId;
			System.out.println("registration_id complete!!");

			String sql = "delete from device";
			db.execSQL(sql);
			sql = "insert into device (device_token,pushchk) values('"+regId+"',1)";
			db.execSQL(sql);

			Log.v("passone", "Already registered");
		}

	}
	public long folderMemoryCheck(String a_path){
		long totalMemory = 0;
		File file = new File(a_path);
		Util.debug("file exists:" + a_path+file.exists());
		File[] childFileList = file.listFiles();

		if(childFileList == null){
			return 0;
		}

		for(File childFile : childFileList){
			if(childFile.isDirectory()){
				totalMemory += folderMemoryCheck(childFile.getAbsolutePath());

			}
			else{
				totalMemory += childFile.length();
			}
		}
		return totalMemory;
	}

	@Override
	public void onResume() {
		init();
		super.onResume();
	}

	@Override
	public void onDestroy() {
		if(db!=null)
			db.close();
		super.onDestroy();
	}
	private void showDownload() {
		// TODO Auto-generated method stub
		dialog = new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.speed_dialog);
		dialog.setCancelable(false);

		//there are a lot of settings, for dialog, check them all out!
		down_progress=(ProgressBar)dialog.findViewById(R.id.down_progress);
		btn_dcancel = (ImageButton) dialog.findViewById(R.id.btn_dcancel);
		btn_dcancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				dialog.dismiss();
				if (drmdownload != null) {

					drmdownload.drm_download_cancel();
				}
				dialog = null;

			}

		});

		dialog.show();
	}
	public void testDownload()
	{

		if(dialog==null)
				showDownload();

			//다운로드 기능 선언부.
			drmdownload=  new DownLoad();
			drmdownload.setCommunicator(this);
	                 /*파일 다운로드 시작 url , 로컬저장경로 . */
		new File(filePath).delete();

		Util.debug("down filepath:" + filePath);
			drmdownload.drm_download_start(testurl, filePath);
		doTask();

//        filepath=Util.getDownloadPath(filename);


	}

	@Override
	public void drm_download_ing(final float total, final float fileLength) {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (down_progress != null) {
					bytetotal = (long) total;

					down_progress.setMax(Math.round(fileLength));
					down_progress.setProgress(Math.round(total));

					//					tv_progress.setText("( "+(int)bytesWritten+"MB/"+(int)bytesTotal+"MB )");
					//Log.i(TAG, String.format("onProgressUpdate() : %d / %d", bytesWritten, bytesTotal));
				}
			}
		});
	}

	@Override
	public void drm_download_cancel(int err) {
		drmdownload=null;
		if(dialog!=null)
		{
			dialog.dismiss();
			dialog=null;
		}
		if(mTimer!=null)
		{
			mTimer.cancel();
			mTimer=null;
		}
		getActivity().runOnUiThread(new Notifier(err));
	}

	@Override
	public void drm_download_end(String downfile) {


		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				long totalkb = bytetotal / Util.KiloBytes;
				long spd = totalkb / mainTime;
				String spdmessage = "";
				((TextView) dialog.findViewById(R.id.tv_speedrate)).setText("속도: " + spd + "kb/s");
				if (spd >= 300) {
					spdmessage = "원활한 수강이 가능합니다.";


				} else if (spd < 300 && spd >= 200) {
					spdmessage = "끊김이 발생할 수 있습니다.";

				} else {
					spdmessage = "수강에 적합하지 않습니다.";

				}
				((ImageButton)dialog.findViewById(R.id.btn_dcancel)).setImageResource(R.drawable.btn_poptestok);
				((TextView) dialog.findViewById(R.id.tv_speedstate)).setText(spdmessage);

			}
		});
		Util.debug("downfile:" + downfile);
		if(mTimer!=null)
		{
			mTimer.cancel();
			mTimer=null;
		}
		drmdownload=null;

	}
	class Notifier implements Runnable {

		private int error;

		public Notifier(int error) {
			this.error = error;
		}

		public void run() {
			switch (error)
			{
				case 1000:
					Util.alert(context, "다운로드 오류", "남은 여유 공간이 요청한 파일크기 보다 작습니다.", "확인", "null", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
						}
					}, null);
					break;
				case 1100:

					Util.alert(context, "알림", "다시 시도해주세요..", "확인", null, new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {

						}
					}, null);
					break;
				case 1200:


					break;
				default:
					break;
			}
			new File(filePath).delete();
			// err < 1000 보다 작을경우 에러 숫자는 http status code 을 의미함.
			// 참조 : http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html

			// err : 1000 = 남은 여유 공간이 요청한 파일크기 보다 작습니다.
			// err : 1100 = 파일 다운로드중 인터넷 환경이 변경되었거나 인터넷이 불안정하여 다운로드가 취소되었습니다.
			// err : 1200 = 다운로드 취소 요청으로 다운로드가 취소되었습니다.

		}
	};
	public void doTask()
	{
		mainTime=0;

		task = new TimerTask(){
			public void run() {
				handler.post(new Runnable() {
					public void run() {
						try {
							mainTime++;
							//Thread.sleep(1000);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}};

		mTimer = new Timer();
		mTimer.schedule(task, 0, 1000);
	}
	public void onResponseReceived(int api, Object result) {
		// TODO Auto-generated method stub
		Util.debug("Login result    :   " + result);
		try{
			switch(api) {
				case Api.REGISTERAPI:
					if(result!=null)
					{
						ApiResult regapi = (ApiResult) result;
						Calendar calendar = Calendar.getInstance();
						Date date = calendar.getTime();
						String nowtime = new SimpleDateFormat("yyyy년 MM월 dd일").format(date);
						String process="";
						if(pushchk==0)
							process="알림 수신거부 처리 완료";
						else if(pushchk==1)
							process="알림 수신동의 처리 완료";
						if(regapi.result.trim().equals("OK"))
						{
							Util.alert(getActivity(), "광고성 푸시 알림 수신 동의 안내", "1. 전송자: KG에듀원 \n\n"
									+ "2. 처리 일자: "+nowtime+"\n\n"
									+ "3. 처리 내용: 해당 기기 "+process+"\n\n"
									+"설정 메뉴에서 알림 설정 변경 가능합니다.", "확인", null, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									setPushchk(pushchk);
								}

							}, null);
						}
						else
						{
							chk_push.setChecked(!chk_push.isChecked());
							Util.ToastMessage(getActivity(), "서버와의 통신에 일시적으로 문제가 발생되었습니다. 차후 다시 시도해주세요.");

						}
					}
					else {
						chk_push.setChecked(!chk_push.isChecked());
						Util.ToastMessage(getActivity(), "서버와의 통신에 일시적으로 문제가 발생되었습니다. 차후 다시 시도해주세요.");

					}
					break;



			}
		}catch(NullPointerException e)
		{
		}
	}
	public void setPushchk(int pushchk)
	{
		DBmanager db_manager= new DBmanager(getActivity(),Constants.DATABASE_NAME);
		SQLiteDatabase db=db_manager.getReadableDatabase();
		ContentValues cv= new ContentValues();

		cv.put("pushchk", pushchk);
		int pushcnt=db.update("device",cv,null,null);
		if(pushcnt==0) {
			String sql = "insert into device (device_token,pushchk) values('',"+pushchk+")";
			db.execSQL(sql);
		}
		db.close();
		CUser.pushchk=pushchk;
	}
}