package net.passone;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import net.passone.adapter.OnResponseListener;
import net.passone.common.IntentModelActivity;

public class PassVocaDetailActivity extends IntentModelActivity implements OnClickListener,OnResponseListener {
	ImageButton btn_login;
	Context context;
	OnResponseListener callback;
	Handler handler;
	String mp3_url="";
	String word="",meaning="",desc="";
	public static String html_format = "<html><head><meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\"></meta><style>body { line-height: 22px; background-color: #f5f5f5; }</style></head><body>%s</body></html>";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.passvoca_detail);
		word = getIntent().getExtras().getString("word");
		meaning = getIntent().getExtras().getString("mean");
		desc = getIntent().getExtras().getString("desc");
		desc=desc.replace("color:#fff", "color:#323232");
		desc=desc.replace("color:#ff9d33", "color:#ff6835");
		desc=desc.replace("background-color:black;","");

		setText(R.id.wordText, word);
		setText(R.id.meaningText, meaning);
		WebView webView = (WebView) findViewById(R.id.webView);
		webView.setBackgroundColor(0x00000000);
		webView.loadData(String.format(html_format, desc), "text/html; charset=UTF-8", "UTF-8");
		((ImageView)findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_goback);
		((ImageView)findViewById(R.id.btn_back)).setOnClickListener(this);
		((TextView)findViewById(R.id.tv_apptitle)).setText("기출예문");

		super.onCreate(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		finish();
		super.onClick(v);
	}
}