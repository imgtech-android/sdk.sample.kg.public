package net.passone;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.passone.adapter.APIAgent;
import net.passone.adapter.WaitDialog;
import net.passone.common.CUser;
import net.passone.common.IntentModelActivity;
import net.passone.common.Util;

import org.apache.http.client.ClientProtocolException;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;


public class QnAWriteActivity extends IntentModelActivity implements AdapterView.OnItemSelectedListener {
    EditText subject,account;
    EditText contents;
    private String[] item = {"App강제종료","재생관련 문의","사용가능 기종문의","개선제안"};
    Spinner spinner_kind;
    ArrayAdapter<String> list;
    WaitDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_qnawrite);
        ((TextView) findViewById(R.id.tv_apptitle)).setText("문의등록");
        loadUserInfo();
        subject = (EditText) findViewById(R.id.edit_subject);
        contents= (EditText) findViewById(R.id.edit_contents);
        account= (EditText) findViewById(R.id.edit_account);
        spinner_kind = (Spinner) findViewById (R.id.spinner_kind);
        spinner_kind.setPrompt("문의종류");
        list = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, item);
        //스피너에 adapter 연결
        spinner_kind.setAdapter(list);
        //스피너가 선택 됫을 때 이벤트 처리
        spinner_kind.setOnItemSelectedListener(this);
        account.setFilters(new InputFilter[]{filterAlphaNum});
        if(CUser.userid.length()==0 || CUser.userid.equals("mobileguest"))
        {
            account.setText("");
            account.requestFocus();

        }
        else
        {
            account.setText(CUser.userid);
            account.setEnabled(false);
        }
        ((ImageView)findViewById(R.id.btn_rightmenu)).setVisibility(View.VISIBLE);
        ((ImageView)findViewById(R.id.btn_back)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ((ImageView)findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_qnaadd);
        findViewById(R.id.btn_rightmenu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (subject.getText().toString().trim().length() == 0) {
                    Util.ToastMessage(self, "제목을 입력해주세요.");
                } else if (contents.getText().toString().trim().length() == 0) {
                    Util.ToastMessage(self, "내용을 입력해주세요.");

                } else if (account.getText().toString().trim().length() == 0) {
                    Util.ToastMessage(self, "계정 ID를 입력해주세요.");

                } else {
                    uploadImprove();
                }
            }
        });


//		findViewById(R.id.layout_improve).setOnClickListener(new OnClickListener(){
//
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//
//				dialog = new Dialog(self);
//				dialog.setTitle("앱 개선문의사항");
//                dialog.setContentView(R.layout.improve);
//                dialog.setCancelable(true);
//                //there are a lot of settings, for dialog, check them all out!
//                subject = (EditText) dialog.findViewById(R.id.edit_subject);
//                contents= (EditText) dialog.findViewById(R.id.edit_contents);
//                account= (EditText) dialog.findViewById(R.id.edit_account);
//            	user = User.currentUser(self);
//            	if(user==null || user.getString("userid").equals("mobileguest"))
//            	{
//            		account.setText("");
//            		account.requestFocus();
//            	}
//            	else
//            	{
//            		account.setText(user.getString("userid"));
//            		account.setEnabled(false);
//            	}
//                Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
//                Button btn_cancle = (Button) dialog.findViewById(R.id.btn_cancle);
//                btn_cancle.setOnClickListener(new OnClickListener(){
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//						dialog.dismiss();
//					}
//                	
//                });
//                btn_ok.setOnClickListener(new OnClickListener(){
//
//					@Override
//					public void onClick(View v) {
//						// TODO Auto-generated method stub
//					
//						if(subject.getText().toString().trim().length()==0)
//						{
//							Util.ToastMessage(self, "제목을 입력해주세요.");
//						}
//						else if(contents.getText().toString().trim().length()==0)
//						{
//							Util.ToastMessage(self, "내용을 입력해주세요.");
//
//						}
//						else if(account.getText().toString().trim().length()==0)
//						{
//							Util.ToastMessage(self, "계정 ID를 입력해주세요.");
//
//						}
//						else
//						{
//							uploadImprove();
//						}
//						
//					
//					}
//                	
//                });
//                
//                dialog.show();
//			}
//
//		});
//		

    }

    public void uploadImprove()
    {
        findViewById(R.id.btn_rightmenu).setEnabled(false);
        if (progressDialog == null) {
            progressDialog=WaitDialog.show(this, "", "",true,true,null);

        }
        APIAgent agent=new APIAgent(self);
        String response;
        PackageManager pm = getPackageManager();
        PackageInfo packageInfo = null;
        String userid="";

        if (account.getText().toString().trim().length()<1)
        {
            Util.ToastMessage(this, "계정 ID를 입력해 주세요.");
        }
        else
        {
            userid=account.getText().toString();

            try {
                packageInfo = pm.getPackageInfo(getPackageName(), 0);

            } catch (PackageManager.NameNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                String url= String.format("http://mobilesotong.passone.net/mobile/V3/deviceInfoAct.php?uid=%s" +
                        "&division=%s&title=%s&contents=%s&osVersion=%s&appVersion=%s&deviceModel=%s&mode=%s&email=%s&category=%d"
                        , userid
                        , getString(R.string.api_prefix)
                        , subject.getText(),contents.getText()
                        , Build.VERSION.RELEASE
                        , packageInfo.versionName
                        , Build.MODEL, "1"
                        , CUser.device_token
                        , (spinner_kind.getSelectedItemPosition()+1));
                Util.debug(url);
//			response = agent.requestWithURL(url.trim().replaceAll("\\r\\n|\\r|\\n", " "));
                response = agent.requestPostWithURL("http://mobilesotong.passone.net/mobile/V3/deviceInfoAct.php","uid", userid,
                        "division", getString(R.string.api_prefix),"title",subject.getText().toString(),"contents",contents.getText().toString(),
                        "osVersion", Build.VERSION.RELEASE,"appVersion", packageInfo.versionName,"deviceModel", Build.MODEL,
                        "mode","1","email", CUser.device_token,"category", String.valueOf(spinner_kind.getSelectedItemPosition()+1));
                ObjectMapper mapper = new ObjectMapper();
                final Map data = mapper.readValue(response, Map.class);
                String result=data.get("result").toString();
                if(result.contains("OK"))
                {
//				dialog.dismiss();
                    finish();
                }
                else
                {
                    Util.ToastMessage(self, data.get("message").toString());
                }

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        findViewById(R.id.btn_rightmenu).setEnabled(true);
        if (progressDialog != null&&progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                               long arg3) {
        // TODO Auto-generated method stub

    }
    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub

    }
    // 영문만 허용 (숫자 포함)
    protected InputFilter filterAlphaNum = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {

            Pattern ps = Pattern.compile("^[a-zA-Z0-9]+$");
            if (!ps.matcher(source).matches()) {
                return "";
            }
            return null;
        }
    };
}
