package net.passone;

import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;

import net.passone.adapter.DBmanager;
import net.passone.adapter.HttpHelper;
import net.passone.adapter.OnResponseListener;
import net.passone.common.FirstPageFragmentListener;
import net.passone.common.FragmentModelActivity;
import net.passone.common.Util;

import org.andlib.helpers.Logger;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;
import kr.imgtech.lib.zoneplayer.data.IntentDataParserListener;

import static kr.imgtech.lib.zoneplayer.data.IntentDataDefine.SITE_ID;

/**
 * Created by passone on 2015-08-04.
 */
public class MainActivity  extends FragmentModelActivity implements View.OnClickListener,OnResponseListener,IntentDataDefine {

    public final static int REQUEST_CODE = 1004;

    // test user-id
    // todo 테스트 다운로드 데이터와 동일하게 guest 로 설정. 다운로드 데이터가 변경되면 같이 변경 요망
    public final static String GUEST = "guest";
    public final static String KG_ID = "72";

    public final static String KEY_INTENT = "intent";
    public final static String KEY_REQUEST_URL = "request-url";
    public final static String KEY_DELETE_URL = "delete-url";

    public final static int ID_TEST = 1;
    public final static int ID_BOX_COURSE = 11;
    public final static int ID_BOX_LECTURE = 12;
    public final static int ID_DOWNLOAD = 2;
    public final static int ID_SETTINGS = 3;

    private int NUM_PAGES = 4;		// 최대 페이지의 수

    /* Fragment numbering */
    public final static int FRAGMENT_PAGE1 = 0;
    //public final static int FRAGMENT_PAGE2 = 1;
    public final static int FRAGMENT_PAGE2 = 1;
    public final static int FRAGMENT_PAGE3 = 2;
    public final static int FRAGMENT_PAGE4 = 3;
    long backKeyPressedTime=0;
    ViewPager mViewPager;			// View pager를 지칭할 변수
    ImageButton page1Btn, page2Btn, page3Btn, page4Btn, page5Btn;
    HttpHelper hh=new HttpHelper();
    int lec_basket_seq,lecture_seq,content_seq,content_per,content_time_m,isDown,last_seek_pos;
    String content_title="",vod_widType="",filepath="";
    Context context;
    OnResponseListener callback;
    Fragment myClassFragment;
    Fragment vocaFragment;
    DBmanager db_manager;
    SQLiteDatabase db;
    pagerAdapter mPageAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        context=this;
        callback=this;
        // ViewPager를 검색하고 Adapter를 달아주고, 첫 페이지를 선정해준다.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mPageAdapter=new pagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPageAdapter);
        mViewPager.setCurrentItem(FRAGMENT_PAGE1);
        page1Btn = (ImageButton) findViewById(R.id.Page1Btn);
        page1Btn.setOnClickListener(this);
        //page2Btn = (ImageButton) findViewById(R.id.Page2Btn);
        //page2Btn.setOnClickListener(this);
        page3Btn = (ImageButton) findViewById(R.id.Page3Btn);
        page3Btn.setOnClickListener(this);
        page4Btn = (ImageButton) findViewById(R.id.Page4Btn);
        page4Btn.setOnClickListener(this);
        page5Btn = (ImageButton) findViewById(R.id.Page5Btn);
        page5Btn.setOnClickListener(this);
        db_manager = new DBmanager(self,"UserInfo.db");
        db = db_manager.getWritableDatabase();
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {



            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                page1Btn.setSelected(false);
                //page2Btn.setSelected(false);
                page3Btn.setSelected(false);
                page4Btn.setSelected(false);
                page5Btn.setSelected(false);

                switch(position){
                    case 0:
                        page1Btn.setSelected(true);
//                        ((Fragment)mPageAdapter.getItem(position)).onResume();
                        break;
                    //case 1:

                      //  page2Btn.setSelected(true);
                       // break;
                    case 1:
                        Util.debug("select voca");
                        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                        Date date=new Date(new Date().getTime());
                        String dateString=format.format(date);

                        Cursor date_cursor = db.query("vocaday", new String[]{"studydate"}, "studydate=?", new String[]{dateString}, null, null, null);
                        try {
                            if (date_cursor.getCount()== 0) {
                                ContentValues cv=new ContentValues();
                                cv.put("studydate",dateString);
                                db.insert("vocaday","",cv);
                            }
                        } catch(Exception e) {
                        }
                        date_cursor.close();

                        page3Btn.setSelected(true);
                        break;
                    case 2:

                        page4Btn.setSelected(true);
                        break;
                    case 3:

                        page5Btn.setSelected(true);
                        break;
                }
            }
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub
            }
        });

        page1Btn.setSelected(true);
    }
    // FragmentPageAdater : Fragment로써 각각의 페이지를 어떻게 보여줄지 정의한다.
    public class pagerAdapter extends FragmentPagerAdapter {
        private final FragmentManager mFragmentManager;
        FirstPageListener listener = new FirstPageListener();

        public pagerAdapter(FragmentManager fm) {
            super(fm);
            mFragmentManager=fm;
        }

        // 특정 위치에 있는 Fragment를 반환해준다.
        @Override
        public Fragment getItem(int position) {

                switch(position){
                    case 0:

                        return new MyClassFragment();
                    //case 1:
                        //return new TimeFragment();
                    case 1:
                        return  new PassVocaFragment();
                    case 2:
                        return  new FreeClassFragment();
                    case 3:
                        return  new SettingFragment();
                    default:
                        return null;
                }


        }
        @Override
        public int getItemPosition(Object object)
        {
            if (object instanceof MyClassFragment &&
                    myClassFragment instanceof PassVocaFragment) {
                return POSITION_NONE;
            }
            if (object instanceof PassVocaFragment &&
                    myClassFragment instanceof MyClassFragment) {
                return POSITION_NONE;
            }
            return POSITION_UNCHANGED;
        }
        // 생성 가능한 페이지 개수를 반환해준다.
        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return NUM_PAGES;
        }
        private final class FirstPageListener implements
                FirstPageFragmentListener {
            public void onSwitchToNextFragment() {
                mFragmentManager.beginTransaction().remove(myClassFragment)
                        .commit();
//                if (myClassFragment instanceof MyClassFragment){
//                    myClassFragment = new PassVocaFragment(listener);
//                }
//                else if(myClassFragment instanceof PassVocaFragment)
//                {
//
//                }
//                else{ // Instance of NextFragment
//                }
                notifyDataSetChanged();
            }
        }

    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        switch(v.getId()){
            case R.id.Page1Btn:
                mViewPager.setCurrentItem(FRAGMENT_PAGE1);
                break;
            //case R.id.Page2Btn:
                //mViewPager.setCurrentItem(FRAGMENT_PAGE2);
               // break;
            case R.id.Page3Btn:
                mViewPager.setCurrentItem(FRAGMENT_PAGE2);

                break;
            case R.id.Page4Btn:
                mViewPager.setCurrentItem(FRAGMENT_PAGE3);
                break;
            case R.id.Page5Btn:
                mViewPager.setCurrentItem(FRAGMENT_PAGE4);
                break;

        }
    }
    public void onBackPressed() {

                if (System.currentTimeMillis() > backKeyPressedTime + 2000) {
                    backKeyPressedTime = System.currentTimeMillis();
                    Util.ToastMessage(this,"'뒤로'버튼을 한번만 더 누르시면 종료됩니다.");
                    return;
                }
                if (System.currentTimeMillis() <= backKeyPressedTime + 2000) {
                    finish();
                }

    }

    @Override
    protected void onDestroy() {
        if(db!=null)
            db.close();
        super.onDestroy();
    }
}
