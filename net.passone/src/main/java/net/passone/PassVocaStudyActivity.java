package net.passone;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.gesture.Gesture;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.gesture.GestureStroke;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import net.passone.adapter.DBmanager;
import net.passone.adapter.VocaStudyAdapter;
import net.passone.common.IntentModelActivity;
import net.passone.common.UITools;
import net.passone.common.Util;
import net.passone.container.VocaItem;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PassVocaStudyActivity extends IntentModelActivity {
	private int currentIndex = 0;
	private ViewSwitcher switcher;
	private int day;
	private Timer timer;
	private static String[] filters = { "", "and isfinish = 1", "and isfinish < 1 " };
	private boolean auto = false;
	private boolean sound= true;
	private MediaPlayer mMediaPlayer;
	ArrayList<VocaItem> vlist=new ArrayList<VocaItem>();
	DBmanager db_manager;
	SQLiteDatabase db;
	Context context;
	VocaStudyAdapter vadapter;
	ImageButton btn_autospeak,btn_examword,btn_memoryno,btn_memoryok;
	TextView tv_word, tv_mean;
	Typeface typeface;
	ImageButton button;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		day = getIntent().getExtras().getInt("day");
		context=this;
		switcher = new ViewSwitcher(this);
		setContentView(switcher);
		currentIndex = 0;

		loadLocal();


		// setTitle("Day " + day + " (" + index + "/60)", Styles.ORANGE);

	}

	private void loadLocal() {
		db_manager = new DBmanager(this,"UserInfo.db");
		db = db_manager.getWritableDatabase();
		String filter = filters[getIntent().getExtras().getInt("filterType")];
		Util.debug("day"+day+",filtertype:"+filter);
		Cursor cursor = db.query("voca", new String[] {"day", "uid","vindex","word","desc","meaning","seen","isfinish"},"day = ? "+filter, new String[]{String.valueOf(day)}, null,null, null);
		try {
			vlist.clear();
			Log.d("passone", "voca db count=" + cursor.getCount());
			if (cursor.getCount()> 0) {
				cursor.moveToFirst();
				for(int i=0; i<cursor.getCount();i++)
				{
					VocaItem vitem=new VocaItem(cursor.getString(1),cursor.getInt(0),cursor.getInt(2),cursor.getString(3),cursor.getString(5),cursor.getString(4),cursor.getInt(6),cursor.getInt(7));
					vlist.add(vitem);
					cursor.moveToNext();

				}
			}
		} catch(Exception e) {
		}
		if(vlist.size()>0)
		{
			View view = prepareView(currentIndex);
			switcher.addView(view);
		}
		else {
			Util.ToastMessage(self, "학습가능한 목록이 없습니다.");
			finish();
		}
	}

	private View prepareView(final int index) {
		final View view = View.inflate(this, R.layout.passvoca_study, null);
		 button = (ImageButton)view.findViewById(R.id.btn_rightmenu);


		ImageButton rightButton = (ImageButton) view.findViewById(R.id.btn_rightmenu);
		rightButton.setOnClickListener(this);
		rightButton.setVisibility(View.VISIBLE);
		((ImageButton)view.findViewById(R.id.btn_back)).setOnClickListener(this);

		if (auto) rightButton.setImageResource(R.drawable.btn_t_manualpage);
		else rightButton.setImageResource(R.drawable.btn_t_autopage);
		tv_mean=(TextView)view.findViewById(R.id.meaningText);
		tv_word=(TextView)view.findViewById(R.id.wordText);
		btn_autospeak=(ImageButton)view.findViewById(R.id.btn_speak);
		btn_examword=(ImageButton)view.findViewById(R.id.btn_examword);
		btn_memoryno=(ImageButton)view.findViewById(R.id.btn_memoryno);
		btn_memoryok=(ImageButton)view.findViewById(R.id.btn_memoryok);
		btn_autospeak.setOnClickListener(this);
		btn_examword.setOnClickListener(this);
		btn_memoryok.setOnClickListener(this);
		btn_memoryno.setOnClickListener(this);
		((TextView)view.findViewById(R.id.tv_apptitle)).setText("Day " + day
				+ " (" + (index + 1) + "/" + vlist.size() + ")");
		typeface = Typeface.createFromAsset(getAssets(), "playregular.ttf");
		((TextView)view.findViewById(R.id.tv_apptitle)).setTypeface(typeface);
		final VocaItem item=vlist.get(index);
		tv_word.setText(item.getWord() + " ");
		tv_mean.setText("???");
		tv_mean.setClickable(true);
		tv_mean.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (tv_mean.getText().toString().equals("???"))
					tv_mean.setText(item.getMeaning());
				else
					tv_mean.setText("???");

			}
		});
		tv_word.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				playSound(item.getWord());
			}
		});
		if (sound) 
		{
			Util.debug("sound on");
			btn_autospeak.setImageResource(R.drawable.btn_autospeak_on);
			try{
				playSound(item.getWord());

			}
			catch(Exception e){}
		}

		else 
		{
			btn_autospeak.setImageResource(R.drawable.btn_autospeak_off);
			mMediaPlayer.stop();
		}
		if (item.getIsfinish() == 1)
		{
			((ImageView) view.findViewById(R.id.emoticon))
					.setVisibility(View.VISIBLE);
			((ImageView) view.findViewById(R.id.emoticon))
					.setImageResource(R.drawable.img_memoryok
					);
		}

		else
			((ImageView) view.findViewById(R.id.emoticon))
			.setVisibility(View.INVISIBLE);

		view.findViewById(R.id.btn_memoryok).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						item.setSeen(1);
						item.setIsfinish(1);
						ContentValues cv=new ContentValues();
						cv.put("seen",1);
						cv.put("isfinish",1);
						db.update("voca",cv,"uid=?",new String[]{item.getUid()});
						moveNext(index);
					}
				});

		btn_autospeak.setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						if(sound)
						{
							mMediaPlayer.stop();
							btn_autospeak.setImageResource(R.drawable.btn_autospeak_off);
						}
						else 
						{
							try{
								playSound(item.getWord());
							}
							catch(Exception e){}
							btn_autospeak.setImageResource(R.drawable.btn_autospeak_on);
						}

						sound=!sound;
					}
				});
		view.findViewById(R.id.btn_memoryno).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						item.setSeen(1);
						item.setIsfinish(0);
						ContentValues cv=new ContentValues();
						cv.put("seen",1);
						cv.put("isfinish",0);
						db.update("voca", cv, "uid=?", new String[]{item.getUid()});
						moveNext(index);
					}
				});
		((GestureOverlayView) view).setGestureColor(0xff00ff00);
		((GestureOverlayView) view).setUncertainGestureColor(0x00000000);
		((GestureOverlayView) view)
		.addOnGesturePerformedListener(new OnGesturePerformedListener() {
			@Override
			public void onGesturePerformed(GestureOverlayView overlay,
					Gesture gesture) {
				ArrayList<GestureStroke> strokes = gesture.getStrokes();
				GestureStroke gestureStroke = strokes.get(0);
				float diff = gestureStroke.points[0]
						- gestureStroke.points[gestureStroke.points.length - 2];
				if (diff > 100)
					moveNext(index);
				else if (diff < 100) 
					movePrev(index);
			}
		});

		view.findViewById(R.id.btn_examword).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(PassVocaStudyActivity.this, PassVocaDetailActivity.class);
				intent.putExtra("word",item.getWord());
				intent.putExtra("mean",item.getMeaning());
				intent.putExtra("desc",item.getDesc());
				startActivity(intent);
			}
		});
		return view;
	}

	protected void toggleAuto() {
		Util.debug("toggleAuto");
		final Handler handler = new Handler();
		final ImageButton button = (ImageButton) findViewById(R.id.btn_rightmenu);
		if (!auto) {
			timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					handler.post(new Runnable() {
						@Override
						public void run() {
							VocaItem item = vlist.get(currentIndex);
							item.setSeen(1);
							ContentValues cv=new ContentValues();
							cv.put("seen",1);
							db.update("voca", cv, "uid=?", new String[]{item.getUid()});
							moveNext(currentIndex);
						}
					});
				}
			}, 3000, 3000);
			button.setImageResource(R.drawable.btn_t_manualpage);
		} else {
			timer.cancel();
			button.setImageResource(R.drawable.btn_t_autopage);
		}

		auto = !auto;
	}

	protected void showComplete() {
		Intent intent = new Intent(this, PassVocaDayResultActivity.class);
		intent.putExtra("day", day);
		intent.putExtra("filterType", getIntent().getExtras().getInt(
				"filterType"));
		startActivity(intent);
		finish();
	}

	private void moveNext(final int index) {
		if (index == vlist.size() - 1) {
			showComplete();
			return;
		}
		switcher.addView(prepareView(index + 1));
		switcher.setInAnimation(UITools.slideFromRightAnimation());
		switcher.setOutAnimation(UITools.slideToLeftAnimation());
		switcher.showNext();
		switcher.removeViewAt(0);

		currentIndex = index + 1;
	}

	private void movePrev(final int index) {
		if (index == 0) {
			return;
		}
		switcher.addView(prepareView(index - 1));
		switcher.setInAnimation(UITools.slideFromLeftAnimation());
		switcher.setOutAnimation(UITools.slideToRightAnimation());
		switcher.showNext();
		switcher.removeViewAt(0);

		currentIndex = index - 1;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (timer != null) timer.cancel();
		button.setImageResource(R.drawable.btn_t_autopage);
		auto = false;
	}

	@Override
	public void onClick(View v) {
		switch(v.getId())
		{
			case R.id.btn_rightmenu:
				toggleAuto();
				break;
			case R.id.btn_back:
				finish();
				break;

		}
		super.onClick(v);
	}
	public void playSound(String word)
	{
		mMediaPlayer = new MediaPlayer();
		word = word.replace(" ", "");

		try {
			mMediaPlayer.setDataSource(context.getExternalFilesDir(null).toString()+"/voca/" + word + ".mp3");  // path 에 mp3 파일 경로
			mMediaPlayer.prepare();
			mMediaPlayer.seekTo(0);
			mMediaPlayer.start();
			mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				public void onCompletion(MediaPlayer mp) {
					mp.release();

				};
			});
		}catch (Exception e){}

	}
}
