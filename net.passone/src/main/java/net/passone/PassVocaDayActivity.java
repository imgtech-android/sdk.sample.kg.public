package net.passone;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import net.passone.adapter.DBmanager;
import net.passone.adapter.OnResponseListener;
import net.passone.adapter.VocaDayAdapter;
import net.passone.common.IntentModelActivity;
import net.passone.common.Util;
import net.passone.container.VocaDayItem;

import java.util.ArrayList;

public class PassVocaDayActivity extends IntentModelActivity implements OnClickListener,OnResponseListener {
	Context context;
	DBmanager db_manager;
	SQLiteDatabase db;
	ImageButton btn_tab_all,btn_tab_ok,btn_tab_no;
	OnResponseListener callback;
	VocaDayAdapter vadapter;
	ListView list_day;
	private ArrayList<VocaDayItem> vocaList = new ArrayList<VocaDayItem>();
	private int filterType = 0;
	private static String[] filters = {null, "finished > 0 and totalseen > 0", "finished < 60 and totalseen > 0" };
	private static String[] w_filters = {null, "finished > 0 and totalseen > 0", "finished < 300 and totalseen > 0" };

	int dw_mode=0; //0: day,  1: week
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.voca_daylist);
		context=this;
		callback=this;
		dw_mode = getIntent().getExtras().getInt("dw_mode");

		btn_tab_all=(ImageButton)findViewById(R.id.btn_daywordall);
		btn_tab_ok=(ImageButton)findViewById(R.id.btn_daywordok);
		btn_tab_no=(ImageButton)findViewById(R.id.btn_daywordno);
		btn_tab_all.setSelected(true);
		list_day=(ListView)findViewById(R.id.list_day);
		btn_tab_all.setOnClickListener(this);
		btn_tab_ok.setOnClickListener(this);
		btn_tab_no.setOnClickListener(this);
		db_manager = new DBmanager(this,"UserInfo.db");
		db = db_manager.getWritableDatabase();
		if(dw_mode>0)
		{
			((TextView)findViewById(R.id.tv_vocatitle)).setText("Week");
			((TextView)findViewById(R.id.tv_apptitle)).setText("주별학습");

			filters=w_filters;
		}
		else{
			((TextView)findViewById(R.id.tv_vocatitle)).setText("Day");
			((TextView)findViewById(R.id.tv_apptitle)).setText("일별학습");

		}
		list_day.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (dw_mode == 0)
					startActivity(new Intent(context, PassVocaStudyActivity.class).putExtra("day", vocaList.get(position).getDay()).putExtra("filterType", getFilterIndex()));
				else
					startActivity(new Intent(context, PassVocaWeekActivity.class).putExtra("week", vocaList.get(position).getWeek()).putExtra("filterType", getFilterIndex()));


			}
		});
		((ImageView)findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_goback);
		((ImageView)findViewById(R.id.btn_back)).setOnClickListener(this);
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
			case R.id.btn_daywordall:
				setFilterIndex(0);
				loadDatabase();
				btn_tab_all.setSelected(true);
				btn_tab_ok.setSelected(false);
				btn_tab_no.setSelected(false);
				break;
			case R.id.btn_daywordok:
				setFilterIndex(1);
				loadDatabase();
				btn_tab_all.setSelected(false);
				btn_tab_ok.setSelected(true);
				btn_tab_no.setSelected(false);
				break;
			case R.id.btn_daywordno:
				setFilterIndex(2);
				loadDatabase();
				btn_tab_all.setSelected(false);
				btn_tab_ok.setSelected(false);
				btn_tab_no.setSelected(true);
				break;
			case R.id.btn_back:
				finish();
					break;

		}
		super.onClick(v);
	}

	@Override
	protected void onDestroy() {
		if(db!=null)
			db.close();
		super.onDestroy();
	}

	public void loadDatabase()
	{
//		cursor = db.query("ZVOCABULARY", new String[] {"ZDAY", "sum(ZFINISH) finished", "sum(ZSEEN)"}, "ZUSER_ID = ?", new String[] { User.currentUser(context).getUserID() }, "ZDAY", getFilter(), "ZDAY");
		Cursor cursor;
		cursor = db.query("voca", new String[] {"day", "sum(isfinish) finished", "sum(seen) totalseen"},null, null, "day", getFilter(), "day");

		try {
			vocaList.clear();
			Log.d("passone", "db count=" + cursor.getCount());
			if(dw_mode==0)
			{
				if (cursor.getCount()> 0) {
					cursor.moveToFirst();
					for(int i=0; i<cursor.getCount();i++)
					{
						VocaDayItem vitem=new VocaDayItem(0,cursor.getInt(0),cursor.getInt(1),cursor.getInt(2));
						vocaList.add(vitem);

						cursor.moveToNext();

					}
				}
			}
			else
			{
				int weekcnt=0;
				if(cursor.getCount()<5)
				{
					weekcnt=1;
					for(int x=1; x <= weekcnt;x++)
					{
						int finished=0;
						int seen=0;
						for(int i=cursor.getCount();i>0;i--)
						{
							int position=x*cursor.getCount()-i;
							if(cursor.moveToPosition(position))
							{
								Util.debug("finished:"+cursor.getInt(1));
								finished+=cursor.getInt(1);
								seen+=cursor.getInt(2);

							}
						}
						VocaDayItem vitem=new VocaDayItem(x,cursor.getInt(0),finished,seen);
						vocaList.add(vitem);


					}
				}
				else
				{
					weekcnt=cursor.getCount()/5;
					for(int x=1;x<=weekcnt;x++)
					{
						int finished=0;
						int seen=0;
						for(int i=5;i>0;i--)
						{
							int position=x*5-i;
							if(cursor.moveToPosition(position))
							{
								finished+=cursor.getInt(1);
								seen+=cursor.getInt(2);

							}
						}
				/*	for(int i=0;i<cursor.getCount();i++)
				{
					if(cursor.moveToPosition(i))
					{

						if(cursor.getInt(2)%5==0)
						{
							if(cursor.getInt(2)/5==x && cursor.getInt(0)>0)
								finished+=cursor.getInt(0);

						}
						else
						{
							if(cursor.getInt(2)/5+1==x)
								finished+=cursor.getInt(0);
						}
					}
				}*/
						VocaDayItem vitem=new VocaDayItem(x,cursor.getInt(0),finished,seen);
						vocaList.add(vitem);

					}
				}
			}

			vadapter=new VocaDayAdapter(context,vocaList,dw_mode);
			list_day.setAdapter(vadapter);
		} catch(Exception e) {
		}
		vadapter.notifyDataSetChanged();
		cursor.close();
		if(vocaList.size()==0)
		{
			((TextView)findViewById(R.id.tv_vocatitle)).setText("해당하는 PassVoca 목록이 없습니다.");

		}
		else{
			if(dw_mode>0)
			{
				((TextView)findViewById(R.id.tv_vocatitle)).setText("Week");
			}
			else{
				((TextView)findViewById(R.id.tv_vocatitle)).setText("Day");

			}
		}
	}
	protected void setFilterIndex(int i) {
		this.filterType = i;
	}

	protected int getFilterIndex() {
		return filterType;
	}

	protected String getFilter() {
		return filters[filterType];
	}

	@Override
	protected void onResume() {
		loadDatabase();

		super.onResume();
	}
}