package net.passone.container;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonAutoDetect()
@JsonIgnoreProperties(ignoreUnknown = true) 
public class CourseDataInfo {

	@JsonProperty("lecCode")		public int lecCode;		
	@JsonProperty("lecName")		public String lecName;
	@JsonProperty("subjectCode")	public String subjectCode;
	@JsonProperty("professorMno")	public int professorMno;
	@JsonProperty("insDt")			public String insDt;
	@JsonProperty("readNum")		public int readNum;


}