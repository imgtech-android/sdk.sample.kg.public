package net.passone.container;


public class VocaDayItem {
	public int day;
	public int sum_finished;
	public int sum_seen;
	public int week;

	public VocaDayItem() {
	}
    public VocaDayItem(int week,int day, int sum_finished, int sum_seen) {
		this.day=day;
		this.sum_finished=sum_finished;
		this.sum_seen=sum_seen;
		this.week=week;
	}

	public int getSumSeen() {
		return sum_seen;
	}
	public int getSumFinished() {
		return sum_finished;
	}
	public int getDay() {
		return day;
	}
	public int getWeek() {
		return week;
	}


}
