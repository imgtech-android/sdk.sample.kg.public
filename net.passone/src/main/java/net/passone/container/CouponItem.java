package net.passone.container;


import java.util.ArrayList;

public class CouponItem {

	public String uid;
	public int remainedTime;
	public int boughtTime;
	public String startDate;
	public String expireDate;
	public String name;
	public String couponType;
	ArrayList<CouponUsedItem> couponUsedlist;

	public CouponItem() {
	}
    public CouponItem(String uid, int remainedTime, int boughtTime, String startDate, String expireDate, String name, String couponType) {
		this.uid=uid;
		this.name=name.replace("\\/","/");
		this.remainedTime=remainedTime;
		this.couponType=couponType;
		this.boughtTime=boughtTime;
		this.startDate=startDate;
		this.expireDate=expireDate;

	}

	public String getUid() {
		return uid;
	}
	public int getRemainedTime() {
		return remainedTime;
	}
	public int getBoughtTimet() {
		return boughtTime;
	}
	public String getStartDate() {
		return startDate;
	}
	public String getExpireDate() {
		return expireDate;
	}

	public String getCouponName() {
		return name;
	}
	public String getCouponType() {
		return couponType;
	}

	public ArrayList<CouponUsedItem> getCouponUsedList() {
		return couponUsedlist;
	}
	public void setCouponUsedList(ArrayList<CouponUsedItem> clist) {
		this.couponUsedlist=((ArrayList<CouponUsedItem>)clist.clone());
	}


}
