package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Login {

	@JsonProperty("login")		public String login;
	@JsonProperty("mem_seq")	public String mem_seq;
	@JsonProperty("mem_type1")	public String mem_type1;
	@JsonProperty("mem_type2")	public String mem_type2;
	@JsonProperty("mem_id")	public String mem_id;

}