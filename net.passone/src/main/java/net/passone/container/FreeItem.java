package net.passone.container;

import java.util.List;


public class FreeItem {
	public String courseCode;
	public String courseName;
	public List<CourseDataInfo> courseData;
	public FreeItem() {		
	}

	public String getCourseCode() {
		return courseCode;
	}

	public String getCourseName() {
		return courseName;
	}
	public List<CourseDataInfo> getCourseData() {
		return courseData;
	}
	
	public void setFree(String courseCode, String courseName, List<CourseDataInfo> courseData)
	{
		this.courseCode=courseCode;
		this.courseName=courseName;
		this.courseData=courseData;

	}	

}
