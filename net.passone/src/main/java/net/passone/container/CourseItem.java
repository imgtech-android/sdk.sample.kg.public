package net.passone.container;


public class CourseItem {

	public String uid;
	public String title;
	public String category;
	public String couponType;
	public String beginDate;
	public int days;
	public int lectureCount;
	public String subject;
	public String teacher;
	public int progress;
	public String orderid;
	public String status;
	public int totalTime;
	public String lastLectureId;
	public String soloStudy;
	public String package_id;

	public CourseItem() {
	}
    public CourseItem(String uid, String title, String category, String couponType, String beginDate, int days, int lectureCount, String subject, String teacher, int progress, String orderid, String status, int totalTime, String lastLectureId, String soloStudy, String package_id) {
		this.uid=uid;
		this.title=title.replace("\\/","/");
		this.category=category;
		this.couponType=couponType;
		this.beginDate=beginDate;
		this.days=days;
		this.lectureCount=lectureCount;
		this.subject=subject.replace("\\/","/");
		this.teacher=teacher.replace("\\/","/");
		this.progress=progress;
		this.orderid=orderid;
		this.status=status;
		this.totalTime=totalTime;
		this.lastLectureId=lastLectureId;
		this.soloStudy=soloStudy;
		this.package_id=package_id;
	}

	public String getUid() {
		return uid;
	}
	public String getTitle() {
		return title;
	}
	public String getCategory() {
		return category;
	}
	public String getCouponType() {
		return couponType;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public int getDays() {
		return days;
	}
	public int getLectureCount() {
		return lectureCount;
	}
	public String getSubject() {
		return subject;
	}
	public String getTeacher() {
		return teacher;
	}
	public int getProgress() {
		return progress;
	}
	public String getOrderid() {
		return orderid;
	}
	public String getStatus() {
		return status;
	}

	public int getTotalTime() {
		return totalTime;
	}
	public String getLastLectureId() {
		return lastLectureId;
	}
	public String getSoloStudy() {
		return soloStudy;
	}
	public String getPackage_id() {
		return package_id;
	}

}
