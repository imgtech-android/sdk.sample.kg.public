package net.passone.container;


public class VocaItem {
	public String uid;
	public int day;
	public int vindex;
	public String word="";
	public String meaning;
	public String desc;
	public int seen;
	public int isfinish;
	public int word_visible=1,mean_visible=0;

	public VocaItem() {
	}
    public VocaItem(String uid, int day, int vindex, String word, String meaning, String desc, int seen, int isfinish) {
		this.uid=uid;
		this.day=day;
		this.vindex=vindex;
		this.word=word;
		this.meaning=meaning;
		this.desc=desc;
		this.seen=seen;
		this.isfinish=isfinish;
	}

	public String getUid() {
		return uid;
	}
	public int getIndex() {
		return vindex;
	}
	public int getDay() {
		return day;
	}

	public String getWord() {
		return word;
	}
	public String getMeaning() {
		return meaning;
	}
	public String getDesc() {
		return desc;
	}
	public int getSeen() {
		return seen;
	}
	public int getIsfinish() {
		return isfinish;
	}
	public int getWordVisible(){return word_visible;}
	public int getMeanVisible(){return mean_visible;}
	public void setSeen(int seen)
	{
		this.seen=seen;
	}
	public void setIsfinish(int isfinish)
	{
		this.isfinish=isfinish;
	}
	public void setWordVisible(int visible)
	{
		this.word_visible=visible;
	}
	public void setMeanVisible(int visible)
	{
		this.mean_visible=visible;
	}
}
