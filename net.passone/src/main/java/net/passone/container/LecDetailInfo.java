package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LecDetailInfo {

	@JsonProperty("refund")		public int refund ;
	@JsonProperty("refundColor")	public int refundColor;
	@JsonProperty("uid")	public String uid;
	@JsonProperty("index")	public String lec_index;
	@JsonProperty("title")	public String title;
	@JsonProperty("duration")     	public String duration;
	@JsonProperty("updatedDate")     	public String updatedDate;
	@JsonProperty("progress")     	public int progress;
	@JsonProperty("orderid")     	public String orderid;
	@JsonProperty("courseid")     	public String courseid;
	@JsonProperty("leccode")     	public int leccode;
	@JsonProperty("etc")     	public String etc;
	@JsonProperty("sequence")     	public int sequence;
	@JsonProperty("currentTime")     	public int currentTime;
	@JsonProperty("periodLeft")     	public int periodLeft;
	@JsonProperty("preStudyText")     	public String preStudyText;
	@JsonProperty("postStudyText")     	public String postStudyText;
	@JsonProperty("soloStudyVideo")     	public String soloStudyVideo;
	@JsonProperty("soloStudyVideoType")     	public String soloStudyVideoType;


}