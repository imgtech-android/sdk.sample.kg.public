package net.passone.container;


public class DownloadItem {
	public String uid;
	private boolean selected;
	public String title="";
	public String orderid;
	public String courseid;
	public int leccode;
	public String etc;
	public String filepath;
	public String course_title;
	public  int isdown;
	public DownloadItem() {
	}
    public DownloadItem(String uid, String title, String orderid, String courseid, int leccode, String etc, String filepath, String course_title) {
		this.uid=uid;
		this.title=title.replace("\\/","/");
		this.orderid=orderid;
		this.courseid=courseid;
		this.leccode=leccode;
		this.etc=etc;
		this.filepath=filepath;
		this.course_title=course_title;
	}
	public void setIsDown(int isdown){
		this.isdown=isdown;
	}
	public void setFilePath(String filepath)
	{
		this.filepath=filepath;
	}
	public String getUid() {
		return uid;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public String getTitle() {
		return title;
	}
	public int getIsDown() {
		return isdown;
	}
	public String getFilepath() {
		return filepath;
	}
	public String getOrderId() {
		return orderid;
	}
	public String getCourseId() {
		return courseid;
	}
	public int getLeccode() {
		return leccode;
	}
	public String getEtc() {
		return etc;
	}
	public String getCourse_title()
	{
		return  course_title;
	}


}
