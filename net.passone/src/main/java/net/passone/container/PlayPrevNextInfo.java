package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayPrevNextInfo {

	@JsonProperty("status")						public String status ;
	@JsonProperty("title")						public String title;
	@JsonProperty("lastlectureIndex")							public String lastlectureIndex;
	@JsonProperty("result")						public String result;
	@JsonProperty("etc")							public String etc;
	@JsonProperty("currentTime")							public String currentTime;
	@JsonProperty("url")							public String url;
	@JsonProperty("message")							public String message;
	@JsonProperty("smiurl")							public String smiurl;



}