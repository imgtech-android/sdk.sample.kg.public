package net.passone.container;


public class TimeCourseItem {

	public String uid;
	public String title;
	public String category;
	public String couponType;
	public String beginDate;
	public int days;
	public int lectureCount;
	public String subject;
	public String teacher;
	public String status;
	public int totalTime;
	public String soloStudy;
	public String special;
	public String filmSchedule;
	public String book;
	public String price;
	public String etc;

	public TimeCourseItem() {
	}
    public TimeCourseItem(String uid, String title, String category, String couponType, String beginDate, int days, int lectureCount, String subject, String teacher, String etc, String status, int totalTime, String special, String soloStudy, String filmSchedule, String book, String price) {
		this.uid=uid;
		this.title=title.replace("\\/","/");
		this.category=category;
		this.couponType=couponType;
		this.beginDate=beginDate;
		this.days=days;
		this.lectureCount=lectureCount;
		this.subject=subject.replace("\\/","/");
		this.teacher=teacher.replace("\\/","/");
		this.etc=etc;
		this.special=special;
		this.status=status;
		this.totalTime=totalTime;
		this.filmSchedule=filmSchedule;
		this.soloStudy=soloStudy;
		this.book=book;
		this.price=price;

	}

	public String getUid() {
		return uid;
	}
	public String getTitle() {
		return title;
	}
	public String getCategory() {
		return category;
	}
	public String getCouponType() {
		return couponType;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public int getDays() {
		return days;
	}
	public int getLectureCount() {
		return lectureCount;
	}
	public String getSubject() {
		return subject;
	}
	public String getTeacher() {
		return teacher;
	}
	public String getEtc() {
		return etc;
	}
	public String getSpecial() {
		return special;
	}
	public String getStatus() {
		return status;
	}

	public int getTotalTime() {
		return totalTime;
	}
	public String getFilmSchedule() {
		return filmSchedule;
	}
	public String getSoloStudy() {
		return soloStudy;
	}
	public String getBook() {
		return book;
	}
	public String getPrice() {
		return price;
	}

}
