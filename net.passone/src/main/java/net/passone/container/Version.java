package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Version {

	@JsonProperty("version")			public String version;		// �����ڵ�
	@JsonProperty("link")			public String link;			// ���� �޽���
	@JsonProperty("new")			public String newcontent;			// ���� �޽���
	@JsonProperty("servercheck")	public int servercheck;			// ���� �޽���
	@JsonProperty("serverstr")	public String serverstr;			// ���� �޽���
	@JsonProperty("update")	public int update;			// ���� �޽���
	@JsonProperty("regdate")	public int regdate;			// ���� �޽���
	@JsonProperty("contents")	public String contents;			// ���� �޽���


}

