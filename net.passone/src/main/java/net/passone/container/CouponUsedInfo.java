package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CouponUsedInfo {

	@JsonProperty("uid")						public String uid ;
	@JsonProperty("courseTitle")					public String courseTitle;
	@JsonProperty("lectureTitle")				public String lectureTitle;
	@JsonProperty("date")						public String date;
	@JsonProperty("usedTime")				public int usedTime;



}