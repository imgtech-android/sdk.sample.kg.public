package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PackageInfo {

	@JsonProperty("uid")		public String uid;
	@JsonProperty("title")	public String title;
	@JsonProperty("category")	public String category;
	@JsonProperty("couponType")	public String couponType;
	@JsonProperty("beginDate")	public String beginDate;
	@JsonProperty("days")	public int days;
	@JsonProperty("progress")	public int progress;
	@JsonProperty("orderid")	public String orderid;
	@JsonProperty("soloStudy")	public String soloStudy;
}