package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MyPdsDetailInfo {

	@JsonProperty("subject")		public String subject;
	@JsonProperty("lecpds_seq")	public int lecpds_seq;
	@JsonProperty("filepath")	public String filepath;
	@JsonProperty("filename")	public String filename;
	@JsonProperty("down_cnt")	public int down_cnt;


}