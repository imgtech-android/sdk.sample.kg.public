package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CouponInfo {

	@JsonProperty("uid")						public String uid ;
	@JsonProperty("remainedTime")				public int remainedTime;
	@JsonProperty("boughtTime")				public int boughtTime;
	@JsonProperty("startDate")					public String startDate;
	@JsonProperty("expireDate")				public String expireDate;
	@JsonProperty("name")						public String name;
	@JsonProperty("couponType")				public String couponType;



}