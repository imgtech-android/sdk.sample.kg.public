package net.passone.container;


public class CourseDataItem {
private int lecCode;
public String subjectCode, lecName;
public int professorMno;
public String insDt;
public int readNum;
	public CourseDataItem() {		
	}
	
	public int getLecCode() {
		return lecCode;
	}
	
	public String getLecName() {
		return lecName;
	}
	public String getSubjectCode() {
		return subjectCode;
	}
	public int getProfessorMno() {
		return professorMno;
	}
	public String getInsDt() {
		return insDt;
	}
	public int getReadNum() {
		return readNum;
	}
	public void setCourseData(int lecCode, String lecName, String subjectCode, int professorMno, String insDt, int readNum)
	{
		this.lecCode=lecCode;
		this.lecName=lecName;
		this.subjectCode=subjectCode;
		this.professorMno=professorMno;
		this.insDt=insDt;
		this.readNum=readNum;
	}	

}
