package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TimeCourseInfo {

	@JsonProperty("uid")				public String uid ;
	@JsonProperty("title")				public String title;
	@JsonProperty("category")			public String category;
	@JsonProperty("couponType")		public String couponType;
	@JsonProperty("beginDate")			public String beginDate;
	@JsonProperty("days")     			public int days;
	@JsonProperty("lectureCount")    	public int lectureCount;
	@JsonProperty("subject")			public String subject;
	@JsonProperty("teacher")			public String teacher;
	@JsonProperty("etc")    			public String etc;
	@JsonProperty("status")			public String status;
	@JsonProperty("totalTime")			public int totalTime;
	@JsonProperty("soloStudy")			public String soloStudy;
	@JsonProperty("special")			public String special;
	@JsonProperty("filmSchedule")		public String filmSchedule;
	@JsonProperty("book")				public String book;
	@JsonProperty("price")				public String price;


}