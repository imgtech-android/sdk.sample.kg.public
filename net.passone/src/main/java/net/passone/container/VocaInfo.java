package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VocaInfo {

	@JsonProperty("uid")		public String uid;
	@JsonProperty("day")		public int day;
	@JsonProperty("index")		public int vindex;
	@JsonProperty("word")		public String word;
	@JsonProperty("meaning")		public String meaning;
	@JsonProperty("desc")		public String desc;


}