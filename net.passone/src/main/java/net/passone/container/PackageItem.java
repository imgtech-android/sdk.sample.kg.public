package net.passone.container;


import java.util.ArrayList;

public class PackageItem {
	public String uid;
	public String title;
	public String category;
	public String couponType;
	public String beginDate;
	public int days;
	public int progress;
	public String orderid;
	public String soloStudy;
	public ArrayList<CourseItem> courseList=new ArrayList<CourseItem>();
	public boolean isPackage=true;
	String teacher="";
	public PackageItem() {
	}
    public PackageItem(String uid, String title , String category, String couponType, String beginDate, int days, int progress, String orderid, String soloStudy, ArrayList<CourseItem> courseList, boolean isPackage, String teacher) {
		this.uid=uid;
		this.title=title.replace("\\/","/");
		this.category=category.replace("\\/","/");
		this.couponType=couponType;
		this.beginDate=beginDate;
		this.days=days;
		this.progress=progress;
		this.orderid=orderid;
		this.soloStudy=soloStudy;
		this.courseList=courseList;
		this.isPackage=isPackage;
		this.teacher=teacher;
    }

	public String getUid() {
		return uid;
	}
	public String getTitle() {
		return title;
	}
	public String getCategory() {
		return category;
	}
	public String getCouponType() {
		return couponType;
	}
	public String getBeginDate() {
		return beginDate;
	}
	public int getDays() {
		return days;
	}
	public int getProgress() {
		return progress;
	}
	public String getOrderid() {
		return orderid;
	}
	public String getSoloStudy() {
		return soloStudy;
	}
	public String getTeacher() {
		return teacher;
	}

	public ArrayList<CourseItem> getCourseList() {
		return courseList;
	}
	public void setCourseList(ArrayList<CourseItem> clist) {
		this.courseList=((ArrayList<CourseItem>)clist.clone());
	}
	public void setIsPackage(boolean isPackage) {
		this.isPackage=isPackage;
	}
}
