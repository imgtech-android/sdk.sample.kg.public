package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayStatusInfo {

	@JsonProperty("result")						public String result ;
	@JsonProperty("message")						public String message;
	@JsonProperty("lectureprev")					public PlayPrevNextInfo playPrevInfo;
	@JsonProperty("lecturenext")					public PlayPrevNextInfo playNextInfo;



}