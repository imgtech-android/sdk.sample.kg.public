package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseInfo {

	@JsonProperty("uid")				public String uid ;
	@JsonProperty("title")				public String title;
	@JsonProperty("category")			public String category;
	@JsonProperty("couponType")		public String couponType;
	@JsonProperty("beginDate")			public String beginDate;
	@JsonProperty("days")     			public int days;
	@JsonProperty("lectureCount")    	public int lectureCount;
	@JsonProperty("subject")			public String subject;
	@JsonProperty("teacher")			public String teacher;
	@JsonProperty("progress")    		public int progress;
	@JsonProperty("orderid")			public String orderid;
	@JsonProperty("status")			public String status;
	@JsonProperty("totalTime")			public int totalTime;
	@JsonProperty("lastLectureId")	public String lastLectureId;
	@JsonProperty("soloStudy")			public String soloStudy;


}