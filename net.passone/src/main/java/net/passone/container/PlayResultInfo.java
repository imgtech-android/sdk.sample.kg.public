package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlayResultInfo {

	@JsonProperty("result")						public String result ;
	@JsonProperty("message")						public String message;
	@JsonProperty("url")							public String url;
	@JsonProperty("smiurl")						public String smiurl;
	@JsonProperty("etc")							public String etc;
	@JsonProperty("currentTime")							public int currentTime;



}