package net.passone.container;


public class LecDataItem {
	public int uid ;
	public String url;
	public String filename;
	public int filesize;
	public LecDataItem() {
	}
    public LecDataItem(int uid, String url, String filename, int filesize) {
		this.uid=uid;
		this.url=url.replace("\\/","/");
		this.filename=filename.replace("\\/","/");
		this.filesize=filesize;

	}

	public int getUid() {
		return uid;
	}
	public String getUrl() {
		return url;
	}
	public String getFileName() {
		return filename;
	}
	public int getFileSize() {
		return filesize;
	}


}
