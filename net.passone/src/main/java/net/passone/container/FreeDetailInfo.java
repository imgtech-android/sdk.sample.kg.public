package net.passone.container;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true) 
@JsonAutoDetect()

public class FreeDetailInfo {

	@JsonProperty("lecInfo")	public String lecInfo;
	@JsonProperty("url")	public String url;
	@JsonProperty("lecDtlCode")	public String lecDtlCode;


}