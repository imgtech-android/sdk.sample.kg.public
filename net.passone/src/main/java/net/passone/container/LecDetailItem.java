package net.passone.container;


public class LecDetailItem {
	public int refund;
	public int refundColor;
	public String uid;
	private boolean selected;
	public String lec_index;
	public int isdown;
	public String title="";
	public String duration;
	public String updatedDate;
	public int progress;
	public String orderid;
	public String courseid;
	public int leccode;
	public String etc;
	public int currentTime;
	public String filepath;

	public LecDetailItem() {
	}
    public LecDetailItem(int refund, int refundColor, String uid, String lec_index, String title, String duration, String updatedDate, int progress, String orderid, String courseid, int leccode, String etc, int currentTime) {
		this.refund=refund;
		this.refundColor=refundColor;
		this.uid=uid;
		this.lec_index=lec_index.replace("\\/","/");
		this.title="["+lec_index+"] "+title.replace("\\/","/");
		this.duration=duration;
		this.updatedDate=updatedDate;
		this.progress=progress;
		this.orderid=orderid;
		this.courseid=courseid;
		this.leccode=leccode;
		this.etc=etc;
		this.currentTime=currentTime;
	}
	public void setIsDown(int isdown){
		this.isdown=isdown;
	}
	public void setFilePath(String filepath)
	{
		this.filepath=filepath;
	}
	public int getRefund() {
		return refund;
	}
	public int getRefundColor() {
		return refundColor;
	}
	public String getUid() {
		return uid;
	}
	public String getLecIndex() {
		return lec_index;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public String getTitle() {
		return title;
	}
	public int getIsDown() {
		return isdown;
	}
	public String getFilepath() {
		return filepath;
	}
	public int getProgress() {
		return progress;
	}
	public String getOrderId() {
		return orderid;
	}
	public String getCourseId() {
		return courseid;
	}
	public int getLeccode() {
		return leccode;
	}
	public String getEtc() {
		return etc;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public String getDuration() {
		return duration;
	}

	public int getCurrentTime() {
		return currentTime;
	}

}
