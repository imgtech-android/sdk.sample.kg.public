package net.passone.container;


public class FreeDetailItem{
	public String lecInfo;
	public String url;
	public String lecDtlCode;
	public int isdown;
	public boolean selected;
	public String filepath;

	public FreeDetailItem() {		
	}

	public String getLecInfo() {
		return lecInfo;
	}

	public String getUrl() {
		return url;
	}
	public String getLecDtlCode() {
		return lecDtlCode;
	}
	public void setFree(String lecInfo, String url, String lecDtlCode)
	{
		this.lecInfo=lecInfo;
		this.url=url;
		this.lecDtlCode=lecDtlCode;
	}
	public int getIsDown() {
		return isdown;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public void setIsDown(int isdown){
		this.isdown=isdown;
	}
	public void setFilePath(String filepath)
	{
		this.filepath=filepath;
	}

	public String getFilepath() {
		return filepath;
	}
}
