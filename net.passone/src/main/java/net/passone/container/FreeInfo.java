package net.passone.container;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


/*
 * API ó�� ���?
 */

@JsonAutoDetect()
@JsonIgnoreProperties(ignoreUnknown = true) 
public class FreeInfo {

	@JsonProperty("courseCode")	public String courseCode;
	@JsonProperty("courseName")	public String courseName;
	@JsonProperty("courseData")	public List<CourseDataInfo> courseData;


}