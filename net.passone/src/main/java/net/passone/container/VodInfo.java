package net.passone.container;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/*
 * API ó�� ���?
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VodInfo {

	@JsonProperty("lecture_able")		public String lecture_able;
	@JsonProperty("lecture_message")	public String lecture_message;
	@JsonProperty("lecture_url")	public String lecture_url;
	@JsonProperty("lecture_last_position")	public int lecture_last_position;
	@JsonProperty("log_seq")	public int log_seq;
	@JsonProperty("total_possible_time") 	public int total_possible_time;


}