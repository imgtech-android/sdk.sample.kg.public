package net.passone.container;


public class CouponUsedItem {

	public String uid;
	public String courseTitle;
	public String lectureTitle;
	public String date;
	public int usedTime;
	public String coupon_id;

	public CouponUsedItem() {
	}
    public CouponUsedItem(String uid, String courseTitle, String lectureTitle, String date, int usedTime, String coupon_id) {
		this.uid=uid;
		this.courseTitle=courseTitle.replace("\\/","/");
		this.lectureTitle=lectureTitle.replace("\\/","/");
		this.date=date;
		this.usedTime=usedTime;
		this.coupon_id=coupon_id;
	}

	public String getUid() {
		return uid;
	}

	public String getCourseTitle() {
		return courseTitle;
	}
	public String getLectureTitle() {
		return lectureTitle;
	}

	public String getDate() {
		return date;
	}
	public int getUsedTime() {
		return usedTime;
	}


}
