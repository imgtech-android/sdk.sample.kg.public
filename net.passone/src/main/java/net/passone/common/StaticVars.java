package net.passone.common;



import net.passone.container.CouponItem;
import net.passone.container.CouponUsedItem;
import net.passone.container.CourseItem;
import net.passone.container.FreeDetailItem;
import net.passone.container.FreeItem;
import net.passone.container.LecDataItem;
import net.passone.container.LecDetailItem;
import net.passone.container.PackageItem;
import net.passone.container.TimeCourseItem;
import net.passone.container.VocaItem;

import java.util.ArrayList;


public class StaticVars {

	/**
	 * Temporary
	 */
    public static ArrayList<PackageItem> packageItems=new ArrayList<PackageItem>();
    public static ArrayList<LecDetailItem> lecDetailItems=new ArrayList<LecDetailItem>();
    public static ArrayList<PackageItem> courseItems=new ArrayList<PackageItem>();
    public static ArrayList<CourseItem> pkgcourseItems=new ArrayList<CourseItem>();
    public static ArrayList<TimeCourseItem> timecourseItems=new ArrayList<TimeCourseItem>();
    public static ArrayList<VocaItem> vocaItems=new ArrayList<VocaItem>();
    public static ArrayList<CouponItem> couponItems=new ArrayList<CouponItem>();
    public static ArrayList<CouponUsedItem> couponUsedItems=new ArrayList<CouponUsedItem>();
    public static ArrayList<LecDataItem> lecDataItems=new ArrayList<LecDataItem>();
    public static ArrayList<FreeItem> freeItems
            = new ArrayList<FreeItem>();
    public static ArrayList<FreeDetailItem> freeDetailItems
            = new ArrayList<FreeDetailItem>();
    public static String uid = null;
//	

}
