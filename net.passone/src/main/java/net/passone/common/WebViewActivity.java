package net.passone.common;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Browser;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.MimeTypeMap;
import android.webkit.WebBackForwardList;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.passone.MainActivity;
import net.passone.QnAWriteActivity;
import net.passone.R;
import net.passone.SettingFragment;
import net.passone.adapter.Api;
import net.passone.adapter.HttpHelper;
import net.passone.adapter.OnResponseListener;
import net.passone.container.Version;

import org.andlib.helpers.Logger;
import org.apache.http.cookie.Cookie;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;


public class WebViewActivity extends IntentModelActivity implements IntentDataDefine,OnClickListener,OnResponseListener {
	WebViewActivity self;
	WebView mWebView;
	CustomWebViewClient webViewClient;
	CustomWebChromeClient webChromeClient;
	private final Handler handler = new Handler();
	String _url="",title="",sharemsg="",shareimgUrl="",shareUrl="",shareLinkTxt="";
	Context context;
	HttpHelper hh=new HttpHelper();

	ProgressBar progressBar;
	long lasttime=0;
	boolean chk_session=true;
    Dialog dialog;
	CookieManager cookieManager;
	OnResponseListener callback;
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		Bundle bundle=getIntent().getExtras();
		CookieSyncManager.createInstance(this);
		cookieManager= CookieManager.getInstance();
		if(bundle!=null)
		{
			if(bundle.getString("url")!=null)
				_url=bundle.getString("url");
			if(bundle.getString("title")!=null)
				title=bundle.getString("title");

		}

        self=this;
		context=self;
		callback=this;
		loadUserInfo();
		Util.debug(_url);
//		_api.Version(context,callback);

		((TextView)findViewById(R.id.tv_apptitle)).setText(title);
		((ImageView)findViewById(R.id.btn_back)).setVisibility(View.VISIBLE);
				((ImageView)findViewById(R.id.btn_back)).setOnClickListener(this);
		loadWebView(_url);
	}

	private void loadWebView(String url) {
		webViewClient=new CustomWebViewClient(self);
		webChromeClient = new CustomWebChromeClient(self);
		mWebView=(WebView)findViewById(R.id.webView);
		mWebView.setWebViewClient(webViewClient);
		mWebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
		mWebView.getSettings().setJavaScriptEnabled(true);  // ���信�� �ڹٽ�ũ��Ʈ���డ��
		// Bridge 인스턴스 등록
		mWebView.addJavascriptInterface(new AndroidBridge(), "android");
		mWebView.setHorizontalScrollBarEnabled(true); // 세로 scroll 제거
		mWebView.setVerticalScrollBarEnabled(true); // 가로 scroll 제거
		mWebView.getSettings().setBuiltInZoomControls(false);
		mWebView.setWebChromeClient(webChromeClient);
		mWebView.getSettings().setAppCacheEnabled(true);
		Map<String, String> extraHeaders = new HashMap<String, String>();
		extraHeaders.put("Referer", Constants.mainUrl);
		Util.debug(url);
		cookieManager = CookieManager.getInstance();
		List<Cookie> cookies=hh.getCookies();
		Cookie sessionInfo = null;

		for (Cookie cookie : cookies ) {
			sessionInfo = cookie;
			String cookieString = sessionInfo.getName() + "="
					+ sessionInfo.getValue() ;
			cookieManager.setCookie(sessionInfo.getDomain(),cookieString);

			CookieSyncManager.getInstance().sync();
		}

		CookieSyncManager.getInstance().startSync();
		mWebView.loadUrl(url, extraHeaders);
		mWebView.setDownloadListener(new DownloadListener() {

			@Override
			public void onDownloadStart(String url, String userAgent,
                                        String contentDisposition, String mimeType, long contentLength) {

				Log.d("MAIL", "===============onDownloadStart()================================");
				Log.d("MAIL", "url : " + url);
				Log.d("MAIL", "userAgent : " + userAgent);
				Log.d("MAIL", "contentDisposition : " + contentDisposition);
				Log.d("MAIL", "mimeType : " + mimeType);
				Log.d("MAIL", "contentLength : " + contentLength);
				MimeTypeMap mtm = MimeTypeMap.getSingleton();

				DownloadManager downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

				Uri downloadUri = Uri.parse(url);



// 파일 이름을 추출한다. contentDisposition에 filename이 있으면 그걸 쓰고 없으면 URL의 마지막 파일명을 사용한다.

				String fileName = downloadUri.getLastPathSegment();

				int pos = 0;



				if ((pos = contentDisposition.toLowerCase().lastIndexOf("filename=")) >= 0) {

					fileName = contentDisposition.substring(pos + 9);

					pos = fileName.lastIndexOf(";");

					if (pos > 0) {

						fileName = fileName.substring(0, pos - 1);

					}

				}



// MIME Type을 확장자를 통해 예측한다.

				String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length()).toLowerCase();

				String mime = mtm.getMimeTypeFromExtension(fileExtension);



// Download 디렉토리에 저장하도록 요청을 작성

				DownloadManager.Request request = new DownloadManager.Request(downloadUri);

				request.setTitle(fileName);

				request.setDescription(url);

				request.setMimeType(mime);

				request.setDestinationInExternalPublicDir( Environment.DIRECTORY_DOWNLOADS, fileName);

				Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS).mkdirs();

// 다운로드 매니저에 요청 등록

				downloadManager.enqueue(request);
				Util.analyticsSend(self, "메인 웹", "첨부파일 다운로드", "ID: " + CUser.userid);


			}

		});

	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch(v.getId())
		{
		case R.id.btn_back:
            onBackPressed();
			break;
			case R.id.btn_rightmenu:
				startActivity(new Intent(this, QnAWriteActivity.class));
				break;
		}
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		if(mWebView!=null)
			mWebView.reload();
		// 앱이 실행되면 리시버 등록

		IntentFilter completeFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);

		registerReceiver(completeReceiver, completeFilter);
		if(_url.contains("complainList.html"))
		{
			showQnaButton();
		}

		super.onResume();
	}
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
	private class AndroidBridge {

		@JavascriptInterface
		public void go_login() { // must be final
			handler.post(new Runnable() {
				public void run() {
					finish();
				}
			});
		}
		@JavascriptInterface
		public void goVideo(final String url, final String title) { // must be final
			handler.post(new Runnable() {
				public void run() {

					/*	Intent intent =
								new Intent(context, YoondiskPlayerActivity.class);*/
					String movietitle="";
					try {
						movietitle= URLDecoder.decode(title,"UTF-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					/*intent.putExtra("vurl", url);
					intent.putExtra("title", movietitle);
					*/
						if(url.contains(".wmv"))
						{

							Util.ToastMessage(context,context.getString(R.string.wmv_alert));
						}
						else
						{
							//context.startActivity(intent);
                            testPlayer();

						}
					}
			});
		}
		@JavascriptInterface
		public void goSetting() { // must be final
			handler.post(new Runnable() {
				public void run() {
					startActivity(new Intent(context, SettingFragment.class));
				}
			});
		}
	}
	@Override
	public void onBackPressed() {
		//		 TODO Auto-generated method stub
		if(mWebView.canGoBack())
		{
			if(mWebView.getUrl().equals(Constants.faqUrl) || mWebView.getUrl().contains(Constants.qnaUrl) || mWebView.getUrl().contains("smart.html"))
			{
				finish();
			}
			else
			{
				String historyUrl="";
				WebBackForwardList mWebBackForwardList = mWebView.copyBackForwardList();
				if (mWebBackForwardList.getCurrentIndex() > 0)
					historyUrl = mWebBackForwardList.getItemAtIndex(mWebBackForwardList.getCurrentIndex()-1).getUrl();
				if(historyUrl.contains("complainList"))
				{
					showQnaButton();
				}
				else {
					hideQnaButton();
				}
				mWebView.goBack();
			}

// Previous url is in historyUrl
		}
		else
			finish();
		return;

	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		Util.debug("destroy");
		finish();
		super.onDestroy();

    }
	 public void onStart() {
		 CookieSyncManager.createInstance(this);
	        super.onStart();


	 }
		
		@Override

	    public void onPause(){
			CookieSyncManager.getInstance().stopSync();

	    	super.onPause();
// 앱이 중단 되면 리시버 등록 해제

			unregisterReceiver(completeReceiver);

	    }

	public void onResponseReceived(int api, Object result) {
		// TODO Auto-generated method stub
		Util.debug("Login result    :   "+result);
		switch(api) {

			case Api.VERSION :
				if (result != null) {
					final Version ver = (Version)result;
					if(ver.servercheck>0)
					{
						String serverStr="";
						if(ver.serverstr.length()>0)
						{
							serverStr=ver.serverstr;
						}
						else
							serverStr="서버 점검중입니다.";
						Util.alert(self, "New Version", serverStr, "확인", null, new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int which) {
								finish();
							}
						}, new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int which) {
							}
						});
					}
					else {

						if (ver.version.trim().length()>0) {
							String str_current=Util.getVersion(self);
							if(Double.parseDouble(ver.version.trim())> Double.parseDouble(str_current))
							{
								String alert_str="현재버전: "+str_current+"\n최신버전: "+ver.version.trim()+"\n\n- What's New -\n"+ver.newcontent;

								Util.alert(self, "New Version", alert_str, "업데이트하기", "취소", new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog, int which) {
										if(!ver.link.equals(""))
										{
											startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(ver.link.replace("\\/", "/"))));
										}
									}
								}, new DialogInterface.OnClickListener() {

									public void onClick(DialogInterface dialog, int which) {
									}
								});

							}


						} else {
							Util.ToastMessage(this, "버전 정보 로딩 실패");

						}
					}
				} else {
					// 수신, 파싱 오류
					Util.PopupMessage(this, getResources().getString(R.string.api_http_alert));
				}
				break;



		}
	}

	private BroadcastReceiver completeReceiver = new BroadcastReceiver() {

		@Override

		public void onReceive(Context context, Intent intent) {

			Resources res = context.getResources();

// 다운로드 완료 토스트 출력

			Util.ToastMessage(context,"파일 다운로드 완료");

// 다운로드 완료 창으로 이동

			startActivity(new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS));

		}

	};
	public void showQnaButton()
	{
		((ImageView)findViewById(R.id.btn_rightmenu)).setVisibility(View.VISIBLE);
		((ImageView)findViewById(R.id.btn_rightmenu)).setOnClickListener(this);
		((ImageView)findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_qna);
	}
	public void hideQnaButton()
	{
		((ImageView)findViewById(R.id.btn_rightmenu)).setVisibility(View.GONE);

	}

	/*
     테스트 앱 - 스트리밍 실행
     */

    private void testPlayer() {

        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(getString(R.string.scheme_kg))
                .authority(getString(R.string.host_player))
                .appendQueryParameter(SITE_ID, MainActivity.KG_ID)

                // info-url 및 data 는 아래 샘플 참조해서 고객사에서 설정
                .appendQueryParameter(INFO_URL, "http://m.imgtech.co.kr/mobile/kg/test/info_url.php")
                .appendQueryParameter(DATA, "play;guest;이명학.Prestart");

        Logger.d(uriBuilder.toString());

        // 플레이어 실행
        Intent intent;
        try {
            // Intent Scheme 실행
            intent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);

            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getApplication().getPackageName());
            intent.setPackage(getApplication().getPackageName());

            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}