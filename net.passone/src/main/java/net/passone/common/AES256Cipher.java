package net.passone.common;

import android.util.Base64;
import android.util.Log;

import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class AES256Cipher {

	public static byte[] ivBytes = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	public static String encrypt(String str, String key) {
		String enc = "";
		try {
			byte[] textBytes = str.getBytes("UTF-8");
			AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
			     SecretKeySpec newKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			     Cipher cipher = null;
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, newKey, ivSpec);
			
			return Base64.encodeToString(cipher.doFinal(textBytes), 0);
		} catch (Exception e) {
			Log.e("encrypt Exception", e.getMessage());
		}
		return enc;
	}
	//복호화
	public static String decrypt (String str, String key){
		String dec="";
		try{
			byte[] textBytes = Base64.decode(str,0);
			//byte[] textBytes = str.getBytes("UTF-8");
			AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivBytes);
			SecretKeySpec newKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, newKey, ivSpec);
			return new String(cipher.doFinal(textBytes), "UTF-8");
		} catch (Exception e) {
			Log.e("encrypt Exception", e.getMessage());

		}
		return dec;
	}
}