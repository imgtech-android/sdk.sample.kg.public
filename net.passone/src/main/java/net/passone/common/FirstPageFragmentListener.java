package net.passone.common;

/**
 * Created by passone on 2015-08-14.
 */
public interface FirstPageFragmentListener {
    void onSwitchToNextFragment();

}
