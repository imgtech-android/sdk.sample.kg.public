package net.passone.common;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

import net.passone.R;


public class CustomWebChromeClient extends WebChromeClient {
	Context context;
	ProgressBar progressBar;
	public CustomWebChromeClient(Context context) {
		// TODO Auto-generated constructor stub
		this.context=context;
	}
	public CustomWebChromeClient(Context context, ProgressBar progress) {
		// TODO Auto-generated constructor stub
		this.context=context;
		this.progressBar=progress;
	}
	@Override
	public boolean onJsConfirm(WebView view, String url, String message, final JsResult result)
	{
		new AlertDialog.Builder(context)
		.setTitle(context.getText(R.string.app_name))
		.setMessage(message)
		.setPositiveButton(android.R.string.ok,
				new AlertDialog.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				result.confirm();
			}
		})
		.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				result.cancel();
			}
		}).setCancelable(false)
		.create()
		.show();

		return true;
	};
	public boolean onJsAlert(WebView view, String url, final String message, JsResult result)
	{
		final JsResult r = result;
		if(null == context)return false;

		new AlertDialog.Builder(context)
		.setMessage(message).setPositiveButton(android.R.string.ok, new AlertDialog.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int which)
			{
				if(message.contains("로그인")|| message.contains("Session"))
				{//세션이 만료됬을 경우 쿠키세션 초기화 후 앱 종료
					if(context.getClass().getName().contains("MainActivity"))
					{
                    }
				}
				r.confirm();
			}
		}).setCancelable(false).create().show();
		return true;
	}
	public void onProgressChanged(WebView view, int newProgress) {

    }
}
