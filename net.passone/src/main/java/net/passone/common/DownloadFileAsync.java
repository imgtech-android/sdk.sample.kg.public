package net.passone.common;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import net.passone.adapter.Adapter;
import net.passone.adapter.WaitDialog;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadFileAsync extends AsyncTask<String, String, String> {
	protected Adapter _api= new Adapter();


	private Context mContext;
	String filePath="";
	WaitDialog dialog;
	String pds_seq="",lec_seq="";
	public DownloadFileAsync(Context context, String lec_seq, String pds_seq) {
		mContext = context;
		this.pds_seq=pds_seq;
		this.lec_seq=lec_seq;
	}

	@Override
	protected void onPreExecute() {
		dialog= WaitDialog.show(mContext, "", "");
		
	
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... params) {

		int count = 0;

		try {
			Thread.sleep(100);
			URL url = new URL(params[0].toString());
			URLConnection conexion = url.openConnection();
			conexion.connect();

			int lenghtOfFile = conexion.getContentLength();

			Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
			filePath=mContext.getExternalFilesDir(null).toString()+"/"+params[1];
			Util.debug("file path:"+filePath);
			Util.debug("file url:"+params[0].toString());

			InputStream input = new BufferedInputStream(url.openStream());
			OutputStream output = new FileOutputStream(filePath);

			byte data[] = new byte[1024];

			long total = 0;

			while ((count = input.read(data)) != -1) {
				total += count;
				Util.debug("value"+ (int) ((total * 100) / lenghtOfFile));
//				publishProgress("" + (int) ((total * 100) / lenghtOfFile));
				output.write(data, 0, count);
			}

			output.flush();
			output.close();
			input.close();

			// 작업이 진행되면서 호출하며 화면의 업그레이드를 담당하게 된다
//			publishProgress("progress", 1, "Task " + 1 + " number");

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// 수행이 끝나고 리턴하는 값은 다음에 수행될 onProgressUpdate 의 파라미터가 된다
		return null;
	}

	@Override
	protected void onProgressUpdate(String... progress) {
	
//		 if (progress[0].equals("progress")) {
//			 	dialog.setProgress(Integer.parseInt(progress[1]));
//			 	dialog.setMessage(progress[2]);
//		    } else if (progress[0].equals("max")) {
//		    	dialog.setMax(Integer.parseInt(progress[1]));
//		    }
	}

	@Override
	protected void onPostExecute(String unused) {
		
		dialog.dismiss();
		Toast.makeText(mContext, "파일 다운로드 완료" , Toast.LENGTH_SHORT).show();
//		_api.MyPdsDetail(CUser.mno, lec_seq, String.valueOf(pds_seq), mContext, (OnResponseListener)mContext);

		try {
			FileOpen.openFile(mContext,new File(filePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}