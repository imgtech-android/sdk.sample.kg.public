package net.passone.common;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ImageButton;

import net.passone.R;
import net.passone.adapter.Adapter;
import net.passone.adapter.DBmanager;
import net.passone.adapter.HttpHelper;
import net.passone.adapter.OnResponseListener;
import net.passone.container.ApiResult;

public class IntentModelFragment extends Fragment implements OnClickListener, OnResponseListener {
	protected static final int HOME = 15;
	public boolean isMain=false;
	protected IntentModelFragment self = this;
	protected Adapter _api= new Adapter();
	DBmanager db_manager;
	SQLiteDatabase db;
	public CookieManager cookieManager;
    HttpHelper hh=new HttpHelper();

	protected boolean finish_alert=false;
	//	public Model getIntentModel() {
	//		Bundle extras = getIntent().getExtras();
	//		return new Model(this, (Map) extras.getSerializable("model"));
	//	}


//	@Override
//	public void onBackPressed() {
//				TabBar tabBar = (TabBar) findViewById(R.id.tabBar);
//				
//				if (TabRegistry.globalTabBar && tabBar != null && this.belongsTo(tabBar.tabInfos)) {
//					Intent intent = new Intent(this, MainActivity.class);
//					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//					startActivity(intent);
//					overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
//				} 
//				else if(isMain) {
//					if(finish_alert)
//					{
//						finish();
//		//				CUser.uid="";
//		//				CUser.userid="";
//		//				CUser.target="";
//					}
//					else
//					{
//						Util.ToastMessage(this, "?�로버튼???�번 ???�르�?종료?�니??");
//						finish_alert=!finish_alert;
//					}
//				}
//				else
//					finish();
//	}
		
	public static void closeActivity(final Context context) {


		((Activity)context).finish();
		System.exit(0);
	}
	
	
	public void setTitleButton(ImageButton btn, int resource)
	{
		btn.setImageResource(resource);
		btn.setVisibility(View.VISIBLE);
		btn.setOnClickListener(this);
	}

	@Override
	public void onConfigurationChanged (Configuration newConfig)
	{

	super.onConfigurationChanged(newConfig);

	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	public void logout()
	{

        DBmanager db_manager= new DBmanager(getActivity(),Constants.DATABASE_NAME);
        SQLiteDatabase db=db_manager.getReadableDatabase();
        hh.clearCookie();
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookie();
        CUser.mno="";
        CUser.userid="";
        CUser.username="";
        String sql = "delete from userinfo";
        db.execSQL(sql);
        if(db!=null) db.close();


    }
	public void loadUserInfo() {
		DBmanager db_manager= new DBmanager(getActivity(),"UserInfo.db");
		SQLiteDatabase db=db_manager.getReadableDatabase();
		Cursor tw_cursor = db.query("userinfo", new String[] {"mno","userid","autochk","nick","pushchk"}, null, null, null, null, null);
		//	values.put("uid", StaticVars.uid);

		try {
//			Log.d("passone","db count="+tw_cursor.getCount());
			if (tw_cursor.getCount()!= 0) {
				tw_cursor.moveToFirst();
				for(int i=0; i<tw_cursor.getCount();i++)
				{
					CUser.mno=tw_cursor.getString(0);
					CUser.userid=tw_cursor.getString(1);
					CUser.autochk=tw_cursor.getInt(2);
                    CUser.nicname=tw_cursor.getString(3);
                    CUser.pushchk=tw_cursor.getInt(4);

                    tw_cursor.moveToNext();
				}
//				_api.login(CUser.userid, CUser.userpw, Util.getDeviceID(this),CUser.device_token, this, this);
				HttpHelper hh=new HttpHelper();
				CookieSyncManager.createInstance(getActivity());
				cookieManager= CookieManager.getInstance();
				  hh.updateCookies("PASSONE_UID", CUser.userid);
				  hh.updateCookies("PASSONE_IDX", String.valueOf(CUser.mno));

				  CookieSyncManager.getInstance().sync();


			}
		} catch(Exception e) {
		}

		tw_cursor.close();
		Cursor cursor = db.query("device", new String[] {"device_token","pushchk"}, null, null, null, null, null);
		try {
			Log.d("passone", "db count=" + cursor.getCount());
			if (cursor.getCount()!= 0) {
				cursor.moveToFirst();
				for(int i=0; i<cursor.getCount();i++)
				{
					CUser.device_token=cursor.getString(0);
					CUser.pushchk=cursor.getInt(1);
					cursor.moveToNext();
				}
//				_api.login(CUser.userid, CUser.userpw, Util.getDeviceID(this),CUser.device_token, this, this);
			}

		} catch(Exception e) {
		}

		cursor.close();
		Cursor appinfo_cursor = db.query("appinfo", new String[] {"jumprate","useSD"}, null, null, null, null, null);
		//	values.put("uid", StaticVars.uid);

		try {
			Log.d("passone","db count="+appinfo_cursor.getCount());
			if (appinfo_cursor.getCount()!= 0) {
				appinfo_cursor.moveToFirst();
				for(int i=0; i<appinfo_cursor.getCount();i++)
				{
					CUser.jumprate=appinfo_cursor.getInt(0);
					if(appinfo_cursor.getInt(1)==0)
						CUser.isExternal=false;
					else
						CUser.isExternal=true;

					appinfo_cursor.moveToNext();
				}


			}
		} catch(Exception e) {
		}

		appinfo_cursor.close();
		if(db!=null)
			db.close();

	}
	public void onResponseReceived(int api, Object result) {
		// TODO Auto-generated method stub
		if (result != null) {

			ApiResult signIn = (ApiResult)result;

			if (signIn.result.trim().equals("OK")) {  			
//				self.onRestart();
			} else if (signIn.result.trim().equals("FAIL")) {    	
				
				Util.ToastMessage(getActivity(), signIn.message);

			}  

		} else {
			Util.PopupMessage(getActivity(), getResources().getString(R.string.api_http_alert));
		}		
	}

	
}
