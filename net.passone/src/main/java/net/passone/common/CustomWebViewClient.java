package net.passone.common;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.passone.R;
import net.passone.adapter.HttpHelper;
import net.passone.adapter.WaitDialog;


public class CustomWebViewClient extends WebViewClient {
	Context context;
	protected static WaitDialog progressDialog;
	CookieManager cookieManager;
	HttpHelper hh= new HttpHelper();

	public CustomWebViewClient(Context context){

		super();
		this.context = context;

	}

	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		Util.debug("shouldOverride:" + url);


		if(url.startsWith("tel:"))
		{
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			context.startActivity( intent );
		}
        else if(url.startsWith("intent:kakaolink:"))
        {
            Util.goKakaoTalk(context,url);
        }
		else if(!url.startsWith("http://api.passone.net"))
		{
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
			context.startActivity( intent );
		}

		else
		{
			if(url.contains("complainList.html"))
			{
				((WebViewActivity)context).showQnaButton();
			}
			else
			{
				((WebViewActivity)context).hideQnaButton();

			}


					view.loadUrl(url);



		}
		return true; 
	} 

	@Override
	public void onPageStarted(WebView view, String url, Bitmap favicon) {
		// TODO Auto-generated method stub
		try{

			if (progressDialog == null ) {
				//				progressDialog = new WaitDialog(getParent());
				//				progressDialog.setMessage("로딩중입니다...");
				progressDialog=WaitDialog.show(context, "", "",true,true,null);
				//				progressDialog.show();
			}

		}catch(Exception e){}
		super.onPageStarted(view, url, favicon);
	}


	public void onPageFinished(WebView view, String url) {
		try{
			if (progressDialog != null&& progressDialog.isShowing()) {
				progressDialog.dismiss();
				progressDialog = null;
			}

			if(url.endsWith(".mp4")) {   
				Intent i = new Intent(Intent.ACTION_VIEW);     
				Uri uri = Uri.parse(url);             
				i.setDataAndType(uri, "video/mp4");          
				context.startActivity(i);          
			}
		}catch(Exception e){}
		CookieSyncManager.getInstance().sync();

	}


	
	public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
		Util.ToastMessage(context, context.getResources().getString(R.string.api_http_alert));

	}
/*
	public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
		int keyCode = event.getKeyCode();
		if ((keyCode == KeyEvent.KEYCODE_DPAD_LEFT) && view.canGoBack()) {
			view.goBack();
			return true;
		} else if ((keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) && view.canGoForward()) {
			view.goForward();
			return true;
		}
		else if ((keyCode == KeyEvent.KEYCODE_BACK) && view.canGoBack()) {
			Util.debug("back");
			view.goBack();
			return true;
		}
		return false;
	}
	*/
}
