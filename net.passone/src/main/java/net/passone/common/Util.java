package net.passone.common;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import net.passone.R;
import net.passone.adapter.DBmanager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class Util {

	public static final long KiloBytes = 1024;
	public static final long MegaBytes = KiloBytes * KiloBytes;
	public static final long GigaBytes = MegaBytes * KiloBytes;
	public static final String PREF_SET = "set";
	public static final String PREF_USEAUTOLOGIN = "useAutoLogin";
	public static final String PREF_USEALARMNETCHANGE = "useAlarmNetChanege";
	public static SharedPreferences pref;
	public static String MT_PREFS = "MY_PREFS";
	static DBmanager db_manager;
	static SQLiteDatabase db;

	////video play�� ���� API

	public static Handler handler;
	//public static com.inka.ncg.passone.sdk.NCG_Agent ncg;

	//////////////////////////////////////////////////////////////////////////////////////////
	// UI
	//////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * �ȵ���̵� ���ø����̼� ����
	 */
	public static void runApplication(Context context, String packageName)
			throws NameNotFoundException, Exception
			{		
		PackageManager pm = context.getPackageManager();

		/**
		 * Package�� ���� ���� �˻�
		 * ������ �������� ������ NameNotFoundException �߻�		
		 */
		pm.getApplicationInfo(packageName, 0);

		/**
		 * ���ø����̼� ������ ����� Exception ���� ó��
		 */
		try {
			Intent intent = pm.getLaunchIntentForPackage(packageName);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		} catch (Exception e) {
			throw new Exception(e);
		}
			}

	/**
	 * Toast �޽��� ���� <br><br>
	 * @param context
	 * @param msg - �޽���
	 */
	public static void ToastMessage(final Context context, String msg)	{
		Toast toast = Toast.makeText(context.getApplicationContext(), msg, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	/**
	 * Toast �޽��� �˾� ����<br><br>
	 * @param context
	 * @param msg - �޽���
	 */
	public static void PopupMessage(final Context context, String msg) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(msg);
		builder.setPositiveButton("확인", new OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
			} 
		});
		builder.show();
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	// UI
	//////////////////////////////////////////////////////////////////////////////////////////

	public static void alertYes(final Context context, int message) {
		alert(context, 0, message, "확인", null);
	}

	public static void alertYes(final Context context, int message, OnClickListener onClickListener) {
		alert(context, 0, message, "확인", onClickListener);
	}

	public static void alertYes(final Context context, int title, int message, OnClickListener onClickListener) {
		alert(context, title, message, "확인", onClickListener);
	}

	public static void alertOk(final Context context, int message) {
		alert(context, 0, message, "확인", null);
	}	

	public static void alertOk(final Context context, int message, OnClickListener onClickListener) {
		alert(context, 0, message, "확인", onClickListener);
	}

	public static void alertOk(final Context context, int title, int message, OnClickListener onClickListener) {
		alert(context, title, message, "확인", onClickListener);
	}

	public static void alertYesNo(final Context context, int message,
                                  OnClickListener onDissmissClickListener, OnClickListener onCancelClickListener)
	{
		alert(context, 0, message, "예", "아니오", onDissmissClickListener, onCancelClickListener);
	}

	public static void alertYesNo(final Context context, int title, int message,
                                  OnClickListener onDissmissClickListener, OnClickListener onCancelClickListener)
	{
		alert(context, title, message, "예", "아니오", onDissmissClickListener, onCancelClickListener);
	}

	public static void alertOkCancel(final Context context, int message,
                                     OnClickListener onDissmissClickListener, OnClickListener onCancelClickListener)
	{
		alert(context, 0, message, "확인", "취소", onDissmissClickListener, onCancelClickListener);
	}

	public static void alertOkCancel(final Context context, int title, int message,
                                     OnClickListener onDissmissClickListener, OnClickListener onCancelClickListener)
	{
		alert(context, title, message, "확인", "취소", onDissmissClickListener, onCancelClickListener);
	}

	/**
	 * alert
	 * 
	 * @param context - The context.
	 * @param message - The resource name.
	 * @param buttonText - The text to display in the button.
	 * @param listener - The DialogInterface.OnDismissListener to use.
	 */
	public static void alert(final Context context, int title, int message, String buttonText, OnClickListener onClickListener) {
		AlertDialog dialog = new AlertDialog.Builder(context).create();
		if (title > 0)
			dialog.setTitle(context.getString(title));
		dialog.setMessage(context.getString(message));
		dialog.setButton(buttonText, onClickListener);
		dialog.setCancelable(false);
		dialog.show();
	}

	/**
	 * alert
	 * 
	 * @param context - The context.
	 * @param message - The resource name.
	 * @param positiveButton - The text to display in the positive button.
	 * @param negativeButton - The text to display in the negative button.
	 * @param onDissmissClickListener - The DialogInterface.OnDismissListener to use.
	 * @param onCancelClickListener - The DialogInterface.OnCancelListener to use.
	 */
	public static void alert(final Context context, int title, int message, String positiveButtonText, String negativeButtonText,
                             OnClickListener onDissmissClickListener, OnClickListener onCancelClickListener)
	{	
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(context.getString(message));
		if (title > 0)
			builder.setTitle(context.getString(title));
		builder.setPositiveButton(positiveButtonText, onDissmissClickListener);					
		builder.setNegativeButton(negativeButtonText, onCancelClickListener);
		AlertDialog dialog = builder.create();
		dialog.setCancelable(false);

		dialog.show();
	}

	/**
	 * alert
	 * 
	 * @param context - The context.
	 * @param message - The resource name.
	 * @param positiveButton - The text to display in the positive button.
	 * @param negativeButton - The text to display in the negative button.
	 * @param onDissmissClickListener - The DialogInterface.OnDismissListener to use.
	 * @param onCancelClickListener - The DialogInterface.OnCancelListener to use.
	 */
	public static void alert(final Context context, String title, String message, String positiveButtonText, String negativeButtonText,
                             OnClickListener onDissmissClickListener, OnClickListener onCancelClickListener)
	{	
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message);
		if (title != null && !title.equals(""))
			builder.setTitle(title);
		builder.setPositiveButton(positiveButtonText, onDissmissClickListener);					
		builder.setNegativeButton(negativeButtonText, onCancelClickListener);
		AlertDialog dialog = builder.create();
		dialog.setCancelable(false);

		dialog.show();
	}	
	/**
	 * ���ڿ��� �ؽ��ڵ带 ����.<br><br>
	 * @param values
	 * @return int
	 */
	public static int getHahscode(String... values) {
		String str = "";
		for (String v : values)
			str += v;	
		return (str.hashCode());
	}

	/**
	 * ���ڿ��� MD5 HEX ���ڿ��� ��ȯ
	 * @param s
	 * @return
	 */
	public static String md5(String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i=0; i<messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);				
			}
			return hexString.toString();			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	public static byte[] encodeBase64(byte [] binaryData) {
		byte [] buf = null;

		try {
			Class Base64 = Class.forName("org.apache.commons.codec.binary.Base64");
			Class[] parameterTypes = new Class[] { byte[].class };
			Method encodeBase64 = Base64.getMethod("encodeBase64", parameterTypes);
			buf = (byte[])encodeBase64.invoke(Base64, binaryData);
		} catch (Exception e) {
			e.printStackTrace();
		}        

		return buf;
	}
	/**
	 * URL ���� ���ϸ� ��ȯ <br><br>
	 * @param url
	 * @return filename
	 */
	public static String getFileName(String url) {
		return (url.substring(url.lastIndexOf('/') + 1)); 
	}	

	/**
	 * src �� URL ���� ���ϸ��� dest ����ο� �����Ͽ� ��ȯ . <br><br>
	 * @param src
	 * @param dest
	 */
	private String getFileName(String src, String dest) {
		return (Util.absolutePath(dest) + "/" + src.substring(src.lastIndexOf('/') + 1));
	}

	/**
	 * ���� �ٿ�ε� ũ�⸦  0/0 MB ������ ���ڿ��� ��ȯ<br><br>
	 * @param current - Byte(s)
	 * @param total - Byte(s)
	 * @return
	 */
	public static String toDisplayMB(long current, long total) {
		return Long.toString(current/MegaBytes) + "/" + Long.toString(total/MegaBytes) + " MB";
	}
	public static String toDisplayKB(long current, long total) {
		return "0."+ Long.toString(current/MegaBytes) + "/" + "0."+ Long.toString(total/MegaBytes) + " MB";
	}
	public static String toDisplayTime(String time) {
		int h=0, m=0, s=0;
		int sec = Integer.parseInt(time);
		String result="";

		if(sec > 60){                                                                                                         
			m = sec/60;
			sec %= 60;
			result+=m+"분 ";
		}
		else
			result+="0분 ";
		if(sec < 60){                                                                                                           
			s = sec;
			result+=s+"초";

		}
		return result;
	}
	/**
	 * ����� ��ȯ<br><br>
	 * @param current - Byte(s)
	 * @param total - Byte(s)
	 * @return
	 */
	public static int getProgress(long current, long total) {
		return (total != 0) ? ((int)((double)current / (double)total * 100.0)) : (0);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// Network & Device
	//////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * WI-FI ���� ����.<br><br>
	 * @param context
	 * @return WifiInfo
	 * @see WifiInfo
	 */
	public static WifiInfo getWifiInfo(final Context context) {

		// Connectivity Service Manager
		WifiManager wManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wManager.getConnectionInfo();
		// WI-FI is enabled.
		if (wManager.isWifiEnabled() && wifiInfo.getSSID() != null)
			return (wifiInfo);

		// disabled
		return (null);
	}

	/**
	 * Mobile ���� ����.<br><br>
	 * @param context
	 * @return true-����
	 */
	public static boolean isMobileConnected(final Context context) {
		ConnectivityManager connectivityManager =
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		// Mobile
		boolean mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected();
		return (mobile);
	}

	/**
	 * WI-FI ���� ����.<br><br>
	 * @param context
	 * @return true-����
	 */
	public static boolean isWifiConnected(final Context context) {
		ConnectivityManager connectivityManager =
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		// WI-FI
		boolean wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
		return (wifi);
	}

	/**
	 * Mobile (3G) ���� ����.<br><br>
	 * @param context
	 * @return WifiInfo
	 * @see WifiInfo
	 */
	public static WifiInfo getMobileInfo(final Context context) {

		// Connectivity Service Manager
		WifiManager wManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wManager.getConnectionInfo();
		// WI-FI is enabled.
		if (wManager.isWifiEnabled() && wifiInfo.getSSID() != null)
			return (wifiInfo);

		// disabled
		return (null);
	}

	/**
	 * ����̽� ���� ��������.<br><br>
	 * @param context
	 * @return ����̽� ID
	 */
	public static String getDeviceID(final Context context) {

		String uid = "35" + //we make this look like a valid IMEI
					Build.BOARD.length()%10+ Build.BRAND.length()%10 +
					Build.CPU_ABI.length()%10 + Build.DEVICE.length()%10 +
					Build.DISPLAY.length()%10 + Build.HOST.length()%10 +
					Build.ID.length()%10 + Build.MANUFACTURER.length()%10 +
					Build.MODEL.length()%10 + Build.PRODUCT.length()%10 +
					Build.TAGS.length()%10 + Build.TYPE.length()%10 +
					Build.USER.length()%10 ; //13 digits


		return uid;
	}

	/**
	 * ���ķ����� ���� �˻�<br><br>
	 * @return true:enulator, false:device
	 */
	public static boolean isEmulator(final Context context) {
		String id = Util.getDeviceID(context);
		if (id == null)
			return (true);
		return (id.equals("000000000000000"));
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	// File
	//////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * SD Card ����Ʈ ���� Ȯ���ϱ�.<br><br>
	 * @return true-����, false-����ȵ�
	 */
	public static boolean isMountSDCard() {
		String sDCardStatus = Environment.getExternalStorageState();
		return (sDCardStatus.equals(Environment.MEDIA_MOUNTED)) ? (true) : (false);
	}

	/**
	 * SD Card ��� ��θ� ���Ѵ�.<br><br>
	 * @return "mnt/sdcard"
	 */
	public static String absolutePath() {
		return (Environment.getExternalStorageDirectory().getAbsolutePath());
	}

	/**
	 * SD Card ��� ��θ� ���Ѵ�.<br><br>
	 * @return "mnt/sdcard"
	 */
	public static String absolutePath(String path) {
		return (Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + path);
	}

	/**
	 * ���丮 �� �Ϻ��� ���丮, ��� ��� �����Ѵ�.<br><br>
	 * @param
	 * @return
	 */
	public static boolean deleteData(String directory) {
		if (!(isMountSDCard()) || (directory.equals("")))
			return false;
		String absDir = absolutePath() + "/" + directory;
		return deleteTree(new File(absDir), true);
	}

	private static boolean deleteTree(File base, boolean deleteBase) {
		boolean result = true;
		if (base.isDirectory()) {
			for (File child : base.listFiles()) {
				result &= deleteTree(child, true);
			}
		}
		if (deleteBase) {
			result &= base.delete();
		}
		return (result);
	}

	/**
	 * ������ ���� ������ �˻��Ѵ�.<br><br>
	 * @param path
	 * @return
	 */
	public static boolean fileExists (String path) {
		if (!(isMountSDCard()) || (path.equals("")))
			return false;
		String absPath = absolutePath() + "/" + path;
		return ((new File(absPath)).exists());
	}

	/**
	 * ������ ũ�⸦ ���Ѵ�.<br><br>
	 * @param path
	 * @return
	 */
	public static long getFileSize (String path) {
		if (!(isMountSDCard()) || (path.equals("")))
			return 0;
		String absPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + path;
		return ((new File(absPath)).length());
	}

	/**
	 * ������ �����Ѵ�.<br><br>
	 * @param file
	 * @return
	 */
	public static Boolean deleteFile(String path, String file) {
		if (!(isMountSDCard()) || (path.equals("")))
			return false;
		String absPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + path;
		return ((new File(absPath, file)).delete());
	}


	/////////////////////////////////////////////////////////////////////
	////  video play�� ���� API
	/////////////////////////////////////////////////////////////////////

	/**
	 * DRM �ٿ�ε� ��� ����
	 */

	public static String getDownloadPath(String path) {
		if(canUseExternalMemory())
			return Constants.rootDirectory+path;
		else
			return Constants.InternalrootDirectory+path;
	}
	//내부메모리 이용가능 용량 체크
	public static long getInternalMemoryAvailableSize(){
		long size = -1;


		File internalMemPath = Environment.getDataDirectory();
		if( internalMemPath != null ){
			StatFs fs = new StatFs(internalMemPath.getPath());


			long blockSize = fs.getBlockSize();
			long availableBlockSize = fs.getAvailableBlocks();


			size = blockSize * availableBlockSize;
		}
		return size;
	}
	public static long getInternalMemoryTotalSize(){
		long size = -1;


		File internalMemPath = Environment.getDataDirectory();
		if( internalMemPath != null ){
			StatFs fs = new StatFs(internalMemPath.getPath());


			long blockSize = fs.getBlockSize();
			long totalBlockSize = fs.getBlockCount();


			size = blockSize * totalBlockSize;
		}
		return size;
	}
	// 외부 메모리가 사용가능한지 판단
	public static boolean canUseExternalMemory() {
		String state = Environment.getExternalStorageState();
		return state.equals(Environment.MEDIA_MOUNTED);
	}


	// 외부 메모리가 사용가능하다면 마운트 Path GET
	public static File getExternalMemoryMoutedPath(){
		if( canUseExternalMemory() ){
			return Environment.getExternalStorageDirectory();
		}
		return null;
	}


	// 외부 메모리 사용가능 용량
	public static long getExteranlMemoryAvailableSize(){
		long size = -1;


		if( canUseExternalMemory() ){
			File exmemPath = getExternalMemoryMoutedPath();
			if( exmemPath != null ){
				StatFs fs = new StatFs(exmemPath.getPath());


				long bkSize = fs.getBlockSize();
				long avaBlocks = fs.getAvailableBlocks();


				size = bkSize * avaBlocks;
			}
		}
		return size;
	}
	public static long getExteranlMemoryTotal(){
		long size = -1;


		if( canUseExternalMemory() ){
			File exmemPath = getExternalMemoryMoutedPath();
			if( exmemPath != null ){
				StatFs fs = new StatFs(exmemPath.getPath());


				long bkSize = fs.getBlockSize();
				long totalBlocks = fs.getBlockCount();


				size = bkSize * totalBlocks;
			}
		}
		return size;
	}
	/**
	 * DRM ���ϸ� ��
	 * @param url
	 * @return
	 */
	public static String getFilename(String url) {
		String str = url.substring(url.lastIndexOf("/")+1, url.length());
		return str;
	}

	/**
	 * ���� ���� Ȯ��
	 * @param path
	 * @return
	 */
	public static boolean isDownFile(String path) {
		File file = new File(path);
		if (file.exists()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * ���丮 ���� Ȯ��
	 * @param path
	 * @return
	 */
	public static boolean isDirectory(String path) {
		File file = new File(path);
		if (file.isDirectory()) {
			return true;
		} else {
			return false;
		}
	}

	// DEBUG log print
	public static void debug(String log) {
		if (Constants.debuggable)
			Log.d(Constants.TAG, log);

	}

	// SDī�� ��뷮
	static final int ERROR = -1;

	/**
	 * ��밡���� �ܺ� �޸� ũ��
	 * @return - long
	 */
	static public long getAvailableExternalMemorySize() {
		if (isMountSDCard()) {
			File path = Environment.getExternalStorageDirectory();
			StatFs stat = new StatFs(path.getPath());
			long blockSize = stat.getBlockSize();
			long availableBlocks = stat.getAvailableBlocks();
			return availableBlocks * blockSize;
		} else {
			return ERROR;
		}
	}

	/**
	 * �ܺ� �޸� ��ü ũ��
	 * @return - long
	 */
	static public long getTotalExternalMemorySize() {
		if (isMountSDCard()) {
			File path = Environment.getExternalStorageDirectory();
			StatFs stat = new StatFs(path.getPath());
			long blockSize = stat.getBlockSize();
			long totalBlocks = stat.getBlockCount();
			return totalBlocks * blockSize;
		} else {
			return ERROR;
		}
	}

	/**
	 * PREFERENCE ������ �ҷ�����
	 * @param context
	 * @param prefcase
	 * @return
	 */
	public static boolean getPref(final Context context, String prefcase){
		pref = context.getSharedPreferences(MT_PREFS, Context.MODE_PRIVATE);
		return pref.getBoolean(prefcase, true);
	}

	/**
	 * DB ���� ���̵� �ҷ�����
	 */
	public static String getUserid(Context context, String _uid) {
		db_manager = new DBmanager(context,"UserInfo.db");
		String userid = "";
		String sql = "Select id from userinfo where uid="+_uid;
		Util.debug("query="+sql);
		if (_uid.equals("")) {
			//			userid = context.getResources().getString(R.string.config_no_userid);
		} else {
			db = db_manager.getReadableDatabase();
			Cursor cursor = db.rawQuery(sql, null);
			if (cursor.moveToFirst()) {
				userid = cursor.getString(0);
			} else {
				//				userid = context.getResources().getString(R.string.config_no_userid);
			}
			cursor.close();
			db.close();
		}
		return userid;
	}

	/**
	 * ������ ��������
	 * @return
	 */
	public static String getModel() {
		return Build.MODEL.replace(" ", "_");
	}

	public static Options getBitmapSize(Options options)
	{
		int targetWidth = 0;
		int targetHeight = 0;

		if(options.outWidth > options.outHeight){
			targetWidth = (int)(600 * 1.3);
			targetHeight = 600;
		}else{
			targetWidth = 600;
			targetHeight = (int)(600 * 1.3);
		}

		Boolean scaleByHeight = Math.abs(options.outHeight - targetHeight) >= Math.abs(options.outWidth - targetWidth);
		if(options.outHeight * options.outWidth * 2 >= 16384){
			double sampleSize = scaleByHeight
					? options.outHeight / targetHeight
							: options.outWidth / targetWidth;
			options.inSampleSize = (int) Math.pow(2d, Math.floor(Math.log(sampleSize)/ Math.log(2d)));
		}
		options.inJustDecodeBounds = false;
		options.inTempStorage = new byte[16*1024];

		return options;
	}
	public byte[] readBytes(InputStream inputStream) throws IOException
	{
		MyByteArrayOutputStream byteBuffer = new MyByteArrayOutputStream(); // note the change!
		int bufferSize = 1024;
		byte[] buffer = new byte[bufferSize];
		int len = 0;
		while ((len = inputStream.read(buffer)) != -1)
		{
			byteBuffer.write(buffer, 0, len);
		}
		inputStream.close(); // hopefully this will release some more memory
		return byteBuffer.toByteArray();
	}
	public static String getVersion(Context context) {
		String version="";
		try {
			PackageInfo i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			version = i.versionName;
		} catch(NameNotFoundException e) { }
		return version;
	}
	public static boolean saveImage(String saveName, String urlPath, Context context)
	{
		String fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+Constants.imageDirectory;
		try {
			(new File(fileName)).mkdirs();
			InputStream inputStream = new URL(urlPath).openStream();

			File file = new File(fileName+"/"+saveName);
			OutputStream out = new FileOutputStream(file);
			int c = 0;
			while((c = inputStream.read()) != -1)
				out.write(c);
			out.flush();

			out.close();

			return true;

		} catch (Exception e) {
			return false;
		}

	}
	public static boolean saveImageStore(String saveName, String urlPath, Context context)
	{
		String fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/"+Constants.imageDirectory;
		File f = new File(fileName);
		File[] fList = f.listFiles();

		if (fList != null) {
			for (int i = 0; i < fList.length; i++) {
				String sName = fList[i].getName();
				if(sName.equals(saveName))
					return false;
			}

		}
		try {
			(new File(fileName)).mkdirs();
			InputStream inputStream = new URL(urlPath).openStream();

			File file = new File(fileName+"/"+saveName);
			OutputStream out = new FileOutputStream(file);
			int c = 0;
			while((c = inputStream.read()) != -1)
				out.write(c);
			out.flush();

			out.close();

			return true;

		} catch (Exception e) {
			return false;
		}

	}
	public static void FinishAllActivity(Context context)
	{
		ActivityManager am = (ActivityManager)context.getSystemService(Activity.ACTIVITY_SERVICE);
		String name = context.getPackageName();
		List<ActivityManager.RunningAppProcessInfo> list = am.getRunningAppProcesses();

		for(ActivityManager.RunningAppProcessInfo i : list){

			if(i.processName.equals(name) == true){
				((Activity)context).moveTaskToBack(true);
				((Activity)context).finish();
				//		        	android.os.Process.killProcess(android.os.Process.myPid());
				//		            i.importance = ActivityManager.RunningAppProcessInfo.IMPORTANCE_EMPTY;
				//		            am.killBackgroundProcesses(i.processName);
			}
		}
	}
	public static Bitmap ResizeBitmap(Bitmap bm)
	{
		int height = bm.getHeight();
		int width = bm.getWidth();
		// Toast.makeText(this, width + " , " + height, Toast.LENGTH_SHORT).show();
		Bitmap resized = bm;

		if(height<=800)
		{
			resized=bm;
		}
		else
		{
			while (height > 800) {
				resized = Bitmap.createScaledBitmap(bm, (width * 800) / height, 800, true);
				height = resized.getHeight();
				width = resized.getWidth();
			}
		}
		if(width>800)
		{
			while (width > 800) {
				resized = Bitmap.createScaledBitmap(bm, 800,(height * 800) / width, true);
				height = resized.getHeight();
				width = resized.getWidth();
			}
		}
		if(resized.getHeight() < resized.getWidth()){
			Matrix matrix = new Matrix();
			//				matrix.postRotate(90);
			resized = Bitmap.createBitmap(resized, 0, 0, resized.getWidth(), resized.getHeight(), matrix, true);
		}
		try {
			File copyFile = new File(Constants.tempImage);
			if(!copyFile.exists())
				copyFile.createNewFile();
			OutputStream out = new FileOutputStream(copyFile);

			resized.compress(CompressFormat.JPEG, 100, out);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}


		return resized;
	}
	//To check whether network connection is available on device or not
	public static boolean checkInternetConnection(Activity _activity) {
		ConnectivityManager conMgr = (ConnectivityManager) _activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (conMgr.getActiveNetworkInfo() != null
				&& conMgr.getActiveNetworkInfo().isAvailable()
				&& conMgr.getActiveNetworkInfo().isConnected())
			return true;
		else
			return false;
	}//checkInternetConnection()
	public synchronized static int GetExifOrientation(String filepath)
	{
		int degree = 0;
		ExifInterface exif = null;

		try
		{
			exif = new ExifInterface(filepath);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		if (exif != null)
		{
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);

			if (orientation != -1)
			{
				// We only recognize a subset of orientation tag values.
				switch(orientation)
				{
				case ExifInterface.ORIENTATION_ROTATE_90:
					degree = 90;
					break;

				case ExifInterface.ORIENTATION_ROTATE_180:
					degree = 180;
					break;

				case ExifInterface.ORIENTATION_ROTATE_270:
					degree = 270;
					break;
				}

			}
		}

		return degree;
	}
	public synchronized static Bitmap GetRotatedBitmap(Bitmap bitmap, int degrees)
	{
		if ( degrees != 0 && bitmap != null )
		{
			Matrix m = new Matrix();
			m.setRotate(degrees, (float) bitmap.getWidth() / 2, (float) bitmap.getHeight() / 2);
			try
			{
				Bitmap b2 = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
				if (bitmap != b2)
				{
					bitmap.recycle();
					bitmap = b2;
				}
			}
			catch (OutOfMemoryError ex)
			{
				// We have no memory to rotate. Return the original bitmap.
			}
		}

		return bitmap;
	}
	public static void setStrictMode()
	{
		int SDK_INT = Build.VERSION.SDK_INT;

		if (SDK_INT>8){

			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

			StrictMode.setThreadPolicy(policy);

		}
	}
	public static String setDecodeUrl(String url)
	{
		url=url.replace("\\/", "/");

		try {
			String sUrl="";
			String eUrl="";
			sUrl = url.substring(0, url.lastIndexOf("/")+1);
			eUrl = url.substring(url.lastIndexOf("/")+1, url.length()); // 한글과 공백을 포함한 부분
			eUrl = URLEncoder.encode(eUrl,"EUC-KR").replace("+", "%20");
			url=sUrl+eUrl;

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return url;
	}
	/**
	 * 이미지뷰 라운드
	 */
	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);
		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
		final RectF rectF = new RectF(rect);
		final float roundPx = 5;
		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);

		return output;
	}
	//원형 이미지
	public static Bitmap getRoundedShape(Bitmap scaleBitmapImage) {

		int targetWidth = scaleBitmapImage.getWidth();
		int targetHeight = scaleBitmapImage.getHeight();
		if(targetHeight>targetWidth)
		{
			targetHeight=targetWidth;
		}
		else
			targetWidth=targetHeight;

		Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
				targetHeight, Config.ARGB_8888);

		Canvas canvas = new Canvas(targetBitmap);
		Path path = new Path();
		path.addCircle(((float) targetWidth - 1) / 2,
				((float) targetHeight - 1) / 2,
				(Math.min(((float) targetWidth),
						((float) targetHeight)) / 2),
				Path.Direction.CCW);

		canvas.clipPath(path);
		Bitmap sourceBitmap = scaleBitmapImage;
		canvas.drawBitmap(sourceBitmap,
				new Rect(0, 0, sourceBitmap.getWidth(),
						sourceBitmap.getHeight()),
						new Rect(0, 0, targetWidth, targetHeight), null);
		return targetBitmap;
	}
	/**
	 * 지정한 패스의 파일을 화면 크기에 맞게 읽어서 Bitmap을 리턴
	 *
	 * @param context
	 *      application context
	 * @param imgFilePath
	 *      bitmap file path
	 * @return Bitmap
	 * @throws IOException
	 */
	public static Bitmap loadBackgroundBitmap(Bitmap bitmap) throws Exception, OutOfMemoryError {
		// 폰의 화면 사이즈를 구한다.
		if(bitmap!=null){
			int w=bitmap.getWidth();
			int h=bitmap.getHeight();

			// 읽어들일 이미지의 사이즈를 구한다.
			Options options = new Options();
			//        options.inPreferredConfig = Config.RGB_565;
			//        options.inJustDecodeBounds = true;
			//        BitmapFactory.decodeFile(bitmap, options);
			//        BitmapFactory.
			// 화면 사이즈에 가장 근접하는 이미지의 스케일 팩터를 구한다.
			// 스케일 팩터는 이미지 손실을 최소화하기 위해 짝수로 한다.
			bitmap = Bitmap.createScaledBitmap(bitmap,680, (h * 680) / w, true);

		}
		return bitmap;
	}
	public static boolean fileCopy(final InputStream input, final OutputStream output) {
		try {
			int length;
			final byte[] buffer = new byte[4096];
			while ((length = input.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}
			// Flushing is only necessary if the stream is not immediately closed afterwards.
			// We rely on all callers to do that correctly outside of this method
		} catch (IOException e) {
			Log.e("LocalStorage.copy: error when copying data", e.toString());
			return false;
		}

		return true;
	}
	public static Animation fadeInAnimation()
	{
		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
		fadeIn.setDuration(1000);
		return fadeIn;
	}
	public static Animation fadeOutAnimation()
	{
		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
		fadeOut.setStartOffset(500);
		fadeOut.setDuration(500);

		return fadeOut;
	}
	public static AnimationSet getFade(int mode)
	{
		AnimationSet animation = new AnimationSet(true);

		switch (mode) {
		case 1: //페이드인만
			animation.addAnimation(fadeInAnimation());

		case 2: //페이드아웃만
			animation.addAnimation(fadeOutAnimation());
			return animation;
		case 3: //둘다
			animation.addAnimation(fadeInAnimation());
			animation.addAnimation(fadeOutAnimation());
			return animation;

		default:
			return animation;

		}
	}
	public static void setGlobalFont(ViewGroup root, Context context) {
		Typeface mTypeface = Typeface.createFromAsset(context.getAssets(), "BM-JUA.ttf");

		for (int i = 0; i < root.getChildCount(); i++) {
			View child = root.getChildAt(i);
			if(!(child instanceof EditText))
			{
				if (child instanceof TextView)
					((TextView)child).setTypeface(mTypeface);
				else if (child instanceof ViewGroup)
					setGlobalFont((ViewGroup)child,context);
			}
		}
	}
	public static Map parseJSON(String response) {
		ObjectMapper mapper = new ObjectMapper();

		try {
			return mapper.readValue(response, Map.class);
		} catch (JsonParseException e) {
			throw new RuntimeException(e);
		} catch (JsonMappingException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
    public static void goSmartPassone(Context context, String scheme)
    {
        scheme=scheme.replace("intent:","");
        int endstr = scheme.indexOf("#Intent;"); // 문자열이 시작하는 위치를 찾음
        scheme=scheme.substring(0, endstr);
        Util.debug("url:"+scheme);
        try{
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo("net.passone", PackageManager.GET_ACTIVITIES);
            if(pi!=null)
            {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(scheme));
                context.startActivity(intent);
            }
        }
        catch(ActivityNotFoundException e)
        {
            Util.ToastMessage(context, "스마트패스원 앱을 먼저 설치해주세요.");
            Intent intent = new Intent( Intent.ACTION_VIEW );
            intent.setData( Uri.parse( "market://details?id=net.passone" ) );
            context.startActivity(intent);
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            Util.ToastMessage(context, "스마트패스원 앱을 먼저 설치해주세요.");
            Intent intent = new Intent( Intent.ACTION_VIEW );
            intent.setData( Uri.parse( "market://details?id=net.passone" ) );
            context.startActivity(intent);
            e.printStackTrace();
        }
    }
	public static void goKakaoTalk(Context context, String scheme)
	{
		scheme=scheme.replace("intent:","");
		int endstr = scheme.indexOf("#Intent;"); // 문자열이 시작하는 위치를 찾음
		scheme=scheme.substring(0, endstr);
		Util.debug("url:" + scheme);
		try{
			PackageManager pm = context.getPackageManager();
			PackageInfo pi = pm.getPackageInfo("com.kakao.talk", PackageManager.GET_ACTIVITIES);
			if(pi!=null)
			{
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(scheme));
				context.startActivity(intent);
			}
		}
		catch(ActivityNotFoundException e)
		{
			Util.ToastMessage(context, "앱을 먼저 설치해주세요.");

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			Util.ToastMessage(context, "앱을 먼저 설치해주세요.");

			e.printStackTrace();
		}
	}
	public static void analyticsSend(Context context, String screenName, String action, String label) {
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);
		Tracker tracker = analytics.newTracker(Constants.trackerId); // Send hits to tracker id UA-XXXX-Y

// All subsequent hits will be send with screen name = "main screen"
		tracker.setScreenName(screenName);

		tracker.send(new HitBuilders.EventBuilder()
				.setCategory(screenName)
				.setAction(action)
				.setLabel(label)
				.build());

	}
	private static boolean isAvailableFileSystem(String fileSystemName) {
		final String[]  unAvailableFileSystemList = {"/dev", "/mnt/asec", "/mnt/obb", "/system", "/data", "/cache", "/efs", "/firmware"};   // 알려진 File System List입니다.

		for (String name : unAvailableFileSystemList) {
			if (fileSystemName.contains(name) == true) {
				return false;
			}
		}

		if (Environment.getExternalStorageDirectory().getAbsolutePath().equals(fileSystemName) == true) {
			/** 안드로이드에서 제공되는 getExternalStorageDirectory() 경로와 같은 경로일 경우에는 추가로 삽입된 SDCard가 아니라고 판단하였습니다. **/
			return false;
		}

		return true;
	}
	private static List<String> readMountsFile() {
		/**
		 * Scan the /proc/mounts file and look for lines like this:
		 * /dev/block/vold/179:1 /mnt/sdcard vfat rw,dirsync,nosuid,nodev,noexec,relatime,uid=1000,gid=1015,fmask=0602,dmask=0602,allow_utime=0020,codepage=cp437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro 0 0
		 *
		 * When one is found, split it into its elements
		 * and then pull out the path to the that mount point
		 * and add it to the arraylist
		 */


		List<String> mMounts = new ArrayList<String>();

		try {
			Scanner scanner = new Scanner(new File("/proc/mounts"));
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.KITKAT)
				{
					if (line.startsWith("/dev/fuse")) {
						String[] lineElements = line.split("[ \t]+");
						String element = lineElements[1];

						mMounts.add(element);

					}
				}
				else
				{
					if (line.startsWith("/dev/block/vold/")) {
						String[] lineElements = line.split("[ \t]+");
						String element = lineElements[1];

						mMounts.add(element);
						Util.debug("mMounts:"+element);

					}
				}


			}
		} catch (Exception e) {
			// Auto-generated catch block
			e.printStackTrace();
		}



		return mMounts;
	}

	private static List<String> readVoldFile() {
		/**
		 * Scan the /system/etc/vold.fstab file and look for lines like this:
		 * dev_mount sdcard /mnt/sdcard 1 /devices/platform/s3c-sdhci.0/mmc_host/mmc0
		 *
		 * When one is found, split it into its elements
		 * and then pull out the path to the that mount point
		 * and add it to the arraylist
		 */
		List<String> mVold = new ArrayList<String>();
		Scanner scanner;
		if(Build.VERSION.SDK_INT<= Build.VERSION_CODES.JELLY_BEAN_MR2)
		{
			try {
				scanner = new Scanner(new File("/system/etc/vold.fstab"));

				while (scanner.hasNext()) {
					String line = scanner.nextLine();

					if (line.startsWith("dev_mount")) {
						String[] lineElements = line.split("[ \t]+");
						String element = lineElements[2];

						if (element.contains(":")) {
							element = element.substring(0, element.indexOf(":"));
						}
						Util.debug("mVold:"+element);

						mVold.add(element);

					}
				}
			} catch (Exception e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			try {
				scanner = new Scanner(new File("/proc/mounts"));
				while (scanner.hasNext()) {
					String line = scanner.nextLine();
					if (line.startsWith("/dev/block/vold/")) {
						String[] lineElements = line.split("[ \t]+");
						String element = lineElements[1];

						mVold.add(element.replace("/mnt/media_rw/", ""));

					}


				}
			} catch (Exception e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
		}
		return mVold;
	}
	/**
	 * 추가 적인 외부 확장 SDCard path 얻기.
	 * 조건으로 걸러진 최종 String List size가 1이 아니면 null 리턴
	 * @return
	 */
	public static String getMicroSDCardDirectory() {
		List<String> mMounts = readMountsFile();
		List<String> mVold = readVoldFile();
		if(mMounts.size()==0 || mVold.size()==0)
			return null;
		if(Build.VERSION.SDK_INT< Build.VERSION_CODES.KITKAT)
		{
			for (int i=0; i < mMounts.size(); i++) {
				String mount = mMounts.get(i);

				if (!mVold.contains(mount)) {
					mMounts.remove(i--);
					Util.debug("mMount test");

					continue;
				}

				File root = new File(mount);
				if (!root.exists() || !root.isDirectory()) {
					mMounts.remove(i--);
					continue;
				}

				if (!isAvailableFileSystem(mount)) {
					mMounts.remove(i--);
					continue;
				}

				if (!checkMicroSDCard(mount)) {
					mMounts.remove(i--);
				}
			}

			if (mMounts.size() == 1) {
				return mMounts.get(0);
			}
		}
		else {
			for (int i=0; i < mMounts.size(); i++) {
				String mount = mMounts.get(i);
				boolean isMatch=true;

				for(int j=0;j<mVold.size();j++)
				{
					if(!mount.contains(mVold.get(j)))
					{
						isMatch=false;
					}
					else
					{
						isMatch=true;
						break;
					}
				}

				if (!isMatch) {
					mMounts.remove(i--);
					continue;
				}


				File root = new File(mount);
				if (!root.canRead() || !root.canWrite()) {
					mMounts.remove(i--);
					continue;
				}
				if (!root.exists() || !root.isDirectory()) {
					mMounts.remove(i--);
					continue;
				}
				if (!isAvailableFileSystem(mount)) {
					mMounts.remove(i--);
					continue;
				}

				if (!checkMicroSDCard(mount)) {
					mMounts.remove(i--);
				}
			}

			if (mMounts.size() == 1) {

				return mMounts.get(0);
			}

		}

		return null;
	}
	private static boolean checkMicroSDCard(String fileSystemName) {
		StatFs statFs = new StatFs(fileSystemName);

		long totalSize = (long)statFs.getBlockSize() * statFs.getBlockCount();

		if (totalSize < GigaBytes) {
			return false;
		}

		return true;
	}

	public static String secondToTime(int seconds)
	{
		int hr = seconds/3600;
		int rem = seconds%3600;
		int mn = rem/60;
		int sec = rem%60;
		String hrStr = (hr<10 ? "0" : "")+hr;
		String mnStr = (mn<10 ? "0" : "")+mn;
		String secStr = (sec<10 ? "0" : "")+sec;
		return hrStr+":"+mnStr+":"+secStr;
	}
	public static Date parseDate(String text) {
		try {
			return new SimpleDateFormat("yyyy-MM-dd").parse(text);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
	public static String getFilePath(Context context, String filename)
	{
		if(! new File(context.getExternalFilesDir(null).toString()+"/free").exists())
			new File(context.getExternalFilesDir(null).toString()+"/free").mkdirs();

		if(! new File(context.getExternalFilesDir(null).toString()+"/voca").exists())
			new File(context.getExternalFilesDir(null).toString()+"/voca").mkdirs();
		String sdPath="";
		if(Build.VERSION.SDK_INT< Build.VERSION_CODES.KITKAT)
		{
			sdPath=getMicroSDCardDirectory()+"/"+context.getString(R.string.api_prefix)+"/data";
		}
		else {
			if(context.getExternalFilesDirs(null).length<=1)
				sdPath=null;

			else
			{
				if(context.getExternalFilesDirs(null)[1]!=null)
					sdPath=context.getExternalFilesDirs(null)[1].toString();
				else
					sdPath=null;
			}
		}


		if(sdPath!=null) //외장 있으면
		{
			if(Build.VERSION.SDK_INT< Build.VERSION_CODES.KITKAT)
			{
				if(! new File(sdPath).exists())
					new File(sdPath).mkdirs();

			}


			if(! new File(sdPath+"/free").exists())
				new File(sdPath+"/free").mkdirs();

			if(! new File(sdPath+"/voca").exists())
				new File(sdPath+"/voca").mkdirs();



			if(new File(context.getExternalFilesDir(null).toString()+"/"+filename).exists()) //이미 내장에 존재할 경우
			{
				return context.getExternalFilesDir(null).toString()+"/"+filename;
			}
			else if(new File(sdPath+"/"+filename).exists())
			{
				//외장에 이미 있는 경우
				return sdPath+"/"+filename;

			}
			else //둘다 없이 새로 받을 경우
			{


				if(CUser.userid.length()>0 && CUser.isExternal) //외장사용체크시
				{
					Util.debug("sd chk");
					return sdPath+"/"+filename;

				}
				else { //외장에 이미 있는 경우
					return context.getExternalFilesDir(null).toString()+"/"+filename;
				}
			}

		}
		else { //외장없음
			return context.getExternalFilesDir(null).toString()+"/"+filename;
		}

	}
	// 내장 메모리 총 용량
	public static long getSDCardMemoryTotalSize(Context context){
		long size = -1;
		if(Build.VERSION.SDK_INT< Build.VERSION_CODES.KITKAT)
		{
			if( getMicroSDCardDirectory()!=null ){
				File exmemPath = new File(getMicroSDCardDirectory());
				if( exmemPath != null ){
					StatFs fs = new StatFs(exmemPath.getPath());


					long bkSize = fs.getBlockSize();
					long totalBlocks = fs.getBlockCount();

					size =totalBlocks * bkSize;

				}
			}

		}
		else {
			if(context.getExternalFilesDirs(null).length>1)
			{

				if(context.getExternalFilesDirs(null)[1]!=null)
				{
					File exmemPath = new File(context.getExternalFilesDirs(null)[1].toString());
					if( exmemPath != null ){
						StatFs fs = new StatFs(exmemPath.getPath());


						long bkSize = fs.getBlockSize();
						long totalBlocks = fs.getBlockCount();

						size =totalBlocks * bkSize;
					}
				}

			}
		}

		return size;
	}
	// 내장 메모리 총 용량
	public static long getExteranlMemoryTotalSize(){
		long size = -1;


		if( canUseExternalMemory() ){
			File exmemPath = getExternalMemoryMoutedPath();
			if( exmemPath != null ){
				StatFs fs = new StatFs(exmemPath.getPath());


				long bkSize = fs.getBlockSize();
				long totalBlocks = fs.getBlockCount();

				size =totalBlocks * bkSize;

			}
		}
		return size;
	}
	// 내장 메모리 사용중 용량
	public static long getExteranlMemoryUseSize(){
		long size = -1;


		if( canUseExternalMemory() ){
			File exmemPath = getExternalMemoryMoutedPath();
			if( exmemPath != null ){
				StatFs fs = new StatFs(exmemPath.getPath());


				long bkSize = fs.getBlockSize();
				long avaBlocks = fs.getAvailableBlocks();
				long totalBlocks = fs.getBlockCount();


				size =(totalBlocks * bkSize) - (bkSize * avaBlocks);

			}
		}
		return size;
	}
	// 내장 메모리 사용가능 용량
	public static long getSDCardMemoryUseSize(Context context){
		long size = -1;

		if(Build.VERSION.SDK_INT< Build.VERSION_CODES.KITKAT)
		{
			if( getMicroSDCardDirectory()!=null ){
				File exmemPath = new File(getMicroSDCardDirectory());
				if( exmemPath != null ){
					StatFs fs = new StatFs(exmemPath.getPath());


					long bkSize = fs.getBlockSize();
					long avaBlocks = fs.getAvailableBlocks();
					long totalBlocks = fs.getBlockCount();


					size =(totalBlocks * bkSize) - (bkSize * avaBlocks);
				}
			}

		}
		else {
			if(context.getExternalFilesDirs(null).length>1)
			{

				if(context.getExternalFilesDirs(null)[1]!=null)
				{
					File exmemPath = new File(context.getExternalFilesDirs(null)[1].toString());
					if( exmemPath != null ){
						StatFs fs = new StatFs(exmemPath.getPath());


						long bkSize = fs.getBlockSize();
						long avaBlocks = fs.getAvailableBlocks();
						long totalBlocks = fs.getBlockCount();


						size =(totalBlocks * bkSize) - (bkSize * avaBlocks);
					}

				}

			}
		}
		return size;



	}
	// 내장 메모리 사용가능 용량
	public static long getSDCardMemoryAvailableSize(Context context){
		long size = -1;
		if(Build.VERSION.SDK_INT< Build.VERSION_CODES.KITKAT)
		{
			if( getMicroSDCardDirectory()!=null ){
				File exmemPath = new File(getMicroSDCardDirectory());
				if( exmemPath != null ){
					StatFs fs = new StatFs(exmemPath.getPath());


					long bkSize = fs.getBlockSize();
					long avaBlocks = fs.getAvailableBlocks();


					size = bkSize * avaBlocks;
				}
			}

		}
		else
		{
			if(context.getExternalFilesDirs(null).length>1)
			{

				if(context.getExternalFilesDirs(null)[1]!=null)
				{
					File exmemPath = new File(context.getExternalFilesDirs(null)[1].toString());
					if( exmemPath != null ){
						StatFs fs = new StatFs(exmemPath.getPath());


						long bkSize = fs.getBlockSize();
						long avaBlocks = fs.getAvailableBlocks();


						size = bkSize * avaBlocks;
					}

				}

			}
		}


		return size;
	}
	public static int StringToTime(String inputString) {
		int ltime = 0;
		String[] myDate = inputString.split(":");
		if(myDate.length==3){
			int lhour = Integer.parseInt(myDate[0])*3600;
			int lmin = Integer.parseInt(myDate[1])*60;
			int lsec = Integer.parseInt(myDate[2]);
			ltime = lhour+lmin+lsec;
		}else if(myDate.length==2){

			int lmin = Integer.parseInt(myDate[0])*60;
			int lsec = Integer.parseInt(myDate[1]);
			ltime = lmin+lsec;
		}

		return ltime;
	}
}
