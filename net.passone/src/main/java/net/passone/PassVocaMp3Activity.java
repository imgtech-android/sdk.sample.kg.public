package net.passone;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.passone.adapter.Api;
import net.passone.adapter.DBmanager;
import net.passone.adapter.OnResponseListener;
import net.passone.adapter.WaitDialog;
import net.passone.common.IntentModelActivity;
import net.passone.common.Util;
import net.passone.container.Mp3Info;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PassVocaMp3Activity extends IntentModelActivity implements OnClickListener,OnResponseListener {
	ImageButton btn_login;
	Context context;
	CheckBox chk_push;
	LinearLayout layout_gochange, layout_manage, layout_change;
	File sdcardPath;
	File dirPath;
	DBmanager db_manager;
	SQLiteDatabase db;
	ImageButton btn_cancle;
	TextView tv_mp3cnt;
	ProgressBar progress_mp3;
	OnResponseListener callback;
	Handler handler;
	String mp3_url="";
	private List<String> mp3_list = new ArrayList<String>();
	Mp3DownloadAsync downloadAsync;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.passvoca_mp3);
		mp3_list=getIntent().getStringArrayListExtra("mp3_list");
		context=this;
		callback=this;
		btn_cancle=(ImageButton)findViewById(R.id.btn_voca_down_cancle);
		tv_mp3cnt=(TextView)findViewById(R.id.tv_vocamp3);
		progress_mp3=(ProgressBar)findViewById(R.id.progress_mp3);
		((TextView)findViewById(R.id.tv_apptitle)).setVisibility(View.INVISIBLE);
		((ImageView)findViewById(R.id.iv_vocatitle)).setVisibility(View.VISIBLE);
		((ImageView)findViewById(R.id.btn_back)).setVisibility(View.GONE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		if(! new File(context.getExternalFilesDir(null).toString()+"/voca").exists())
			new File(context.getExternalFilesDir(null).toString()+"/voca").mkdirs();
		if(Util.canUseExternalMemory())
		{
			sdcardPath = Environment.getExternalStorageDirectory();
		}
		else
		{
			sdcardPath = Environment.getDataDirectory();

		}
		ImageButton cancelBtn=(ImageButton)findViewById(R.id.btn_voca_down_cancle);
		cancelBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

			onBackPressed();

			}
		});
		downloadAsync=new Mp3DownloadAsync(context);
		dirPath=new File(context.getExternalFilesDir(null).toString()+"/voca");
		_api.VocaMp3(context,callback);
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onResponseReceived(int api, Object result) {
		switch(api) {

			case Api.PASSVOCAMP3:

				if (result != null) {
					Mp3Info apiResult = (Mp3Info)result;
					if(apiResult.url.length()>0)
					{
						mp3_url=apiResult.url.replace("\\/","/");

						downloadAsync.execute();

						handler = new Handler();


					}

				}
				break;


		}
	}
	public class Mp3DownloadAsync extends AsyncTask<String, String, String> {


		private Context mContext;
		String filePath="";
		WaitDialog dialog;

		public Mp3DownloadAsync(Context context) {
			mContext = context;
		}

		@Override
		protected void onPreExecute() {
			progress_mp3.setMax(mp3_list.size());
			progress_mp3.setProgress(0);
			tv_mp3cnt=(TextView)findViewById(R.id.tv_vocamp3);
			tv_mp3cnt.setText(progress_mp3.getProgress()+"/"+progress_mp3.getMax());
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... params) {
			try {
				int total = mp3_list.size();
				for (int i = 0; i < mp3_list.size(); i++) {
					String filename = mp3_list.get(i) + ".mp3";
					//다운로드 받을 URL에 대한 객체를 생성한다.
					//
					URL url = new URL(mp3_url + filename);
					// URL로 부터 connection 객체를 를 생성한다.
					HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
					urlConnection.setRequestMethod("GET");
					//				    urlConnection.setDoOutput(true);  //ics 보안강화 오류때문에 주석

					//웹서버에 연결한다.
					urlConnection.connect();
					if (urlConnection.getResponseCode() == 200) {
						//생성된 커넥션에 대해 추가적인 설정을 지정한다. 여기서는 get방식의 요청을 설정


						//SDCard root 디렉토리에 somefile.txt 파일에 파일을 저장하기 위해
						//파일 객체를 생성한다.
						File file = new File(dirPath, filename);
						//파일을 오픈한다.
						FileOutputStream fileOutput = new FileOutputStream(file);

						//인터넷으로 부터 데이터를 읽어들이기 위한 입력스트림을 얻어온다.
						InputStream inputStream = urlConnection.getInputStream();

						//파일의 전체 크기를 얻어온다.
						int totalSize = urlConnection.getContentLength();

						//다운로드 받을 전체 바이트 크기를 변수에 저장한다.
						int downloadedSize = 0;

						//버퍼를 생성한다.
						byte[] buffer = new byte[1024];
						int bufferLength = 0; //임시로 사용할 버퍼의 크기 지정

						//입력버퍼로 부터 데이터를 읽어서 내용을 파일에 쓴다.
						while ((bufferLength = inputStream.read(buffer)) > 0) {
							//버퍼에 읽어들인 데이터를 파일에 쓴다.
							fileOutput.write(buffer, 0, bufferLength);
							//다운로드 받은 바이트수를 계산한다.
							downloadedSize += bufferLength;
							//progressDialog에 다운로드 받은 바이트수를 표시해 준다.  <-따로 progressDialog스레드를 작성해둘것

						}

						//작업이 끝나면 파일을 close하여 저장한다.
						fileOutput.close();

					}
					setProgressText(i);
					if(isCancelled())
						break;

				}

			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// 수행이 끝나고 리턴하는 값은 다음에 수행될 onProgressUpdate 의 파라미터가 된다
			return null;
		}

		@Override
		protected void onProgressUpdate(String... progress) {

//			 if (progress[0].equals("progress")) {
//				 	dialog.setProgress(Integer.parseInt(progress[1]));
//				 	dialog.setMessage(progress[2]);
//			    } else if (progress[0].equals("max")) {
//			    	dialog.setMax(Integer.parseInt(progress[1]));
//			    }
		}


		@Override
		protected void onPostExecute(String unused) {
			finish();

		}

	}


	public void setProgressText(final int i)
	{
		handler.post(new Runnable() {
			@Override
			public void run() {
				progress_mp3.setProgress(i);
				tv_mp3cnt.setText(i+"/"+progress_mp3.getMax());
			}
		});
	}

	@Override
	protected void onDestroy() {
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if(downloadAsync!=null)
		{
			downloadAsync.cancel(true);
			downloadAsync=null;
		}
		finish();
	}
}