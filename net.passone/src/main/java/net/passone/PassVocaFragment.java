package net.passone;

import android.app.Fragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.DonutProgress;

import net.passone.adapter.Adapter;
import net.passone.adapter.Api;
import net.passone.adapter.DBmanager;
import net.passone.adapter.OnResponseListener;
import net.passone.adapter.WaitDialog;
import net.passone.common.FirstPageFragmentListener;
import net.passone.common.IntentModelFragment;
import net.passone.common.Util;
import net.passone.container.VocaInfo;
import net.passone.container.VocaItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LectureFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LectureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PassVocaFragment extends IntentModelFragment implements OnResponseListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    int MAX_DAY=30,remain_day=0;
    Adapter _api;
    ArrayList<VocaItem> vList;
    Context context;
    OnResponseListener callback;
    ListView listView;
    DBmanager db_manager;
    SQLiteDatabase db;
    static FirstPageFragmentListener firstPageListener;
    File sdcardPath;
    File dirPath;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ArrayList<String> mp3_list;
    boolean isMp3Alert=false;
    TextView tv_remain, tv_myjindo, tv_recomjindo;
    DonutProgress jindo_progress,recom_progress;
    Typeface typeface;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LectureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PassVocaFragment newInstance(String param1, String param2) {
        PassVocaFragment fragment = new PassVocaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    public static PassVocaFragment newInstance() {
        PassVocaFragment fragment = new PassVocaFragment();

        return fragment;
    }
    public PassVocaFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View getView() {
        Util.debug("getview");
        return super.getView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_daystudy:
                loadmp3Local(0);

//                startActivity(new Intent(context,PassVocaDayActivity.class).putExtra("dw_mode",0));
                break;
            case R.id.btn_weekstudy:
                loadmp3Local(1);


                break;
        }
        super.onClick(v);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_passvoca, container, false);

        _api=new Adapter();
        context=getActivity();
        callback =this;
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "playregular.ttf");

        loadUserInfo();
        ((TextView)rootView.findViewById(R.id.tv_apptitle)).setVisibility(View.INVISIBLE);
        ((ImageView)rootView.findViewById(R.id.iv_vocatitle)).setVisibility(View.VISIBLE);
        ((ImageView)rootView.findViewById(R.id.btn_back)).setVisibility(View.GONE);
        ((TextView)rootView.findViewById(R.id.tv_myjindototal)).setTypeface(typeface);
        ((TextView)rootView.findViewById(R.id.tv_recjindototal)).setTypeface(typeface);

        tv_myjindo=(TextView)rootView.findViewById(R.id.tv_myjindo);
            tv_recomjindo=(TextView)rootView.findViewById(R.id.tv_recomjindo);
            tv_remain=(TextView)rootView.findViewById(R.id.tv_remain);
            jindo_progress=(DonutProgress)rootView.findViewById(R.id.jindo_progress);
            recom_progress=(DonutProgress)rootView.findViewById(R.id.recom_progress);
        tv_myjindo.setTypeface(typeface);
        tv_recomjindo.setTypeface(typeface);
        tv_remain.setTypeface(typeface);

        ((ImageButton)rootView.findViewById(R.id.btn_daystudy)).setOnClickListener(this);
        ((ImageButton)rootView.findViewById(R.id.btn_weekstudy)).setOnClickListener(this);

//        lectureAdapter=new MyLectureAdapter(context, StaticVars.myLectureItems);
        vList=new ArrayList<VocaItem>();
        mp3_list=new ArrayList<String>();
        db_manager = new DBmanager(getActivity(),"UserInfo.db");
        db = db_manager.getWritableDatabase();

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Util.debug("onViewCreated");

        super.onViewCreated(view, savedInstanceState);
    }

    private void loadDataBase() {
        Cursor tw_cursor = db.query("voca", new String[] {"uid"}, null, null, null, null, null);
        //	values.put("uid", StaticVars.uid);

        try {
            Log.d("passone", "db count=" + tw_cursor.getCount());
            if (tw_cursor.getCount()== 0) {

                _api.PassVoca(context, callback); //패스보카 정보가 없을 때만 부르기
            }

        } catch(Exception e) {
        }

        tw_cursor.close();
        mp3_list.clear();

        if(Util.canUseExternalMemory())
        {
            sdcardPath = Environment.getExternalStorageDirectory();
        }
        else
        {
            sdcardPath = Environment.getDataDirectory();

        }

        dirPath=new File(context.getExternalFilesDir(null).toString()+"/voca");

        Cursor v_cursor = db.query("voca", new String[] {"uid","word"}, null, null, null, null, null);
        try {
            Log.d("passone","db v count="+v_cursor.getCount());
            if (v_cursor.getCount()!= 0) {
                v_cursor.moveToFirst();
                mp3_list.clear();
                for(int i=0; i<v_cursor.getCount();i++)
                {

                    String word=v_cursor.getString(1).replace("-", "");
                    word=word.replace(" ", "");
                    String filename=word+".mp3";
                    if(!dirPath.exists())
                        dirPath.mkdirs();
                    File dataPath=new File(dirPath.getAbsolutePath(),filename);
                    if(!dataPath.exists())
                    {
//                        Util.debug(dirPath.getAbsolutePath()+"word:"+word);
                        mp3_list.add(word);

                    }
                    v_cursor.moveToNext();

                }



            }
        } catch(Exception e) {
        }
        Cursor date_cursor = db.query("vocaday", new String[]{"studydate"}, null, null, null, null, null);
        try {
            if(date_cursor.getCount()<=30)
            {
                remain_day=MAX_DAY-date_cursor.getCount();
                Util.debug("cnt day"+date_cursor.getCount());
            }

            else remain_day=0;
        } catch(Exception e) {
        }

        showStatus();
        v_cursor.close();
        date_cursor.close();
    }

    @Override
    public void onDestroy() {
        if(db!=null)
            db.close();
        super.onDestroy();
    }

    @Override
    public void onResponseReceived(int api, Object result) {
        Util.debug("Login result    :   " + result);
        switch(api) {

            case Api.PASSVOCA:

                if (result != null) {
                   new WordSaveAsync(context).execute((List<VocaInfo>)result);


                }
                break;

        }
    }

    public class PackageAsync extends AsyncTask<String, String, String> {


        private Context mContext;
        String filePath="";
        WaitDialog dialog;

        public PackageAsync(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            dialog= WaitDialog.show(mContext, "", "");


            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {





            // 수행이 끝나고 리턴하는 값은 다음에 수행될 onProgressUpdate 의 파라미터가 된다
            return null;
        }

        @Override
        protected void onProgressUpdate(String... progress) {

//			 if (progress[0].equals("progress")) {
//				 	dialog.setProgress(Integer.parseInt(progress[1]));
//				 	dialog.setMessage(progress[2]);
//			    } else if (progress[0].equals("max")) {
//			    	dialog.setMax(Integer.parseInt(progress[1]));
//			    }
        }

        @Override
        protected void onPostExecute(String unused) {
            dialog.dismiss();


        }
    }
    public void backPressed() {
        firstPageListener.onSwitchToNextFragment();
    }
    public class WordSaveAsync extends AsyncTask<List<VocaInfo>, String, String> {


        private Context mContext;
        String filePath="";
        WaitDialog dialog;

        public WordSaveAsync(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            dialog= WaitDialog.show(mContext, "", "");


            super.onPreExecute();
        }

        @Override
        protected String doInBackground(List<VocaInfo>... params) {
            ContentValues cv=new ContentValues();
            for(VocaInfo vinfo : (List< VocaInfo >) params[0])
            {
                cv.put("uid",vinfo.uid);
                cv.put("vindex",vinfo.vindex);
                cv.put("day",vinfo.day);
                cv.put("desc",vinfo.desc);
                cv.put("meaning",vinfo.meaning);
                cv.put("word", vinfo.word);
                int update=db.update("voca",cv,"uid=?",new String[]{vinfo.uid});
                if(update==0)
                    db.insert("voca","",cv);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... progress) {

//			 if (progress[0].equals("progress")) {
//				 	dialog.setProgress(Integer.parseInt(progress[1]));
//				 	dialog.setMessage(progress[2]);
//			    } else if (progress[0].equals("max")) {
//			    	dialog.setMax(Integer.parseInt(progress[1]));
//			    }
        }

        @Override
        protected void onPostExecute(String unused) {
            dialog.dismiss();

        }
    }
    private void loadmp3Local(final  int dw_mode) {

        Util.debug("mp3_list:"+mp3_list.size());
        if(mp3_list.size()>0)
        {

                Util.alert(getActivity(), "음성파일 다운로드", "다운로드 하시겠습니까?", "확인", "취소", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(getActivity(), PassVocaMp3Activity.class).putStringArrayListExtra("mp3_list",mp3_list));

                    }
                },  new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(context,PassVocaDayActivity.class).putExtra("dw_mode",dw_mode));

                    }
                });



        }
        else
        {
            startActivity(new Intent(context,PassVocaDayActivity.class).putExtra("dw_mode",dw_mode));

        }

    }
    public void showStatus() {
        Cursor finished_cursor = db.query("voca", new String[]{"uid"}, "isfinish=?", new String[]{"1"}, null, null, null);
        Cursor total_cursor = db.query("voca", new String[]{"uid"}, null, null, null, null, null);
        int expectedCount = (30 - remain_day) * 60;
        tv_remain.setText(""+remain_day);
        tv_myjindo.setText(finished_cursor.getCount()+"");
        tv_recomjindo.setText(expectedCount+"");
        jindo_progress.setMax(total_cursor.getCount());
        recom_progress.setMax(total_cursor.getCount());
        jindo_progress.setProgress(finished_cursor.getCount());
        recom_progress.setProgress(expectedCount);
        tv_myjindo.setTypeface(typeface);
        tv_recomjindo.setTypeface(typeface);
        tv_remain.setTypeface(typeface);
        finished_cursor.close();
        total_cursor.close();

    }

    @Override
    public void onResume() {
        loadDataBase();

        super.onResume();
    }
}
