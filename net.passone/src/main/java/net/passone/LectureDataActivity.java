package net.passone;

import android.app.Fragment;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import net.passone.adapter.Adapter;
import net.passone.adapter.AdapterItemManager;
import net.passone.adapter.Api;
import net.passone.adapter.DBmanager;
import net.passone.adapter.LecDataAdapter;
import net.passone.adapter.OnResponseListener;
import net.passone.adapter.WaitDialog;
import net.passone.common.FileOpen;
import net.passone.common.IntentModelActivity;
import net.passone.common.StaticVars;
import net.passone.common.Util;
import net.passone.container.LecDataInfo;
import net.passone.container.LecDataItem;
import net.passone.container.TimeCourseItem;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LectureFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LectureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LectureDataActivity extends IntentModelActivity implements OnResponseListener, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    Adapter _api;
    ArrayList<LecDataItem> dataList;
    Context context;
    OnResponseListener callback;
    LecDataAdapter dAdapter;
    ListView listView;
    DBmanager db_manager;
    SQLiteDatabase db;
    boolean mode_search=false;
    LinearLayout layout_search, layout_timetab,layout_timeinfo;
    EditText edit_search;
    String lecture="";
    int remained=0,bought=0;
    TextView tv_hourtime, tv_usetime;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lecturedata);
        lecture = getIntent().getExtras().getString("lecture");
        self=this;
        _api=new Adapter();
        context=this;
        callback=this;
        loadUserInfo();
        ((TextView)findViewById(R.id.tv_apptitle)).setText("강의 자료");
        ((ImageButton)findViewById(R.id.btn_back)).setOnClickListener(this);
//        lectureAdapter=new MyLectureAdapter(context, StaticVars.myLectureItems);
        dataList=new ArrayList<LecDataItem>();
//        _api.MyPackage(CUser.userid, context, callback);
        listView=(ListView)findViewById(R.id.list_data);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LecDataItem item=dataList.get(position);
                File downFile=new File(Util.getFilePath(self,item.getFileName()));
                if(!downFile.exists())
                    new DownloadFileAsync(self).execute(item.getUrl(),item.getFileName());
                else{
                    try {
                        FileOpen.openFile(self, downFile);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });
        _api.LectureData(lecture,context,this);
    }


    @Override
    public void onResponseReceived(int api, Object result) {
        Util.debug("Login result    :   " + result);
        switch(api) {

            case Api.LECDATA:

                if (result != null) {
                    AdapterItemManager.AddLecDataList((List<LecDataInfo>) result);
                    if(StaticVars.lecDataItems.size()>0)
                    {
                        dataList=(ArrayList<LecDataItem>)StaticVars.lecDataItems.clone();
                        dAdapter=new LecDataAdapter(context,dataList);
                        listView.setAdapter(dAdapter);
                    }
                    else
                    {
                        Util.alertOk(this,R.string.nolecdata);
                    }

                }
                break;


        }
    }
    private final static Comparator<TimeCourseItem> categoryComparator= new Comparator<TimeCourseItem>() {

        private final Collator collator = Collator.getInstance();
        @Override

        public int compare(TimeCourseItem object1, TimeCourseItem object2) {
            return collator.compare(object1.getCategory(), object2.getCategory());

        }
    };
    private final static Comparator<TimeCourseItem> profComparator= new Comparator<TimeCourseItem>() {

        private final Collator collator = Collator.getInstance();
        @Override

        public int compare(TimeCourseItem object1, TimeCourseItem object2) {
            return collator.compare(object1.getTeacher(), object2.getTeacher());

        }
    };
    private final static Comparator<TimeCourseItem> lecComparator= new Comparator<TimeCourseItem>() {

        private final Collator collator = Collator.getInstance();
        @Override

        public int compare(TimeCourseItem object1, TimeCourseItem object2) {
            return collator.compare(object1.getSubject(), object2.getSubject());

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_rightmenu:


                break;
            case R.id.btn_back:
                finish();
                break;
        }
        super.onClick(v);
    }

    public String secondTohour(int seconds)
    {
        int hr = seconds/3600;

        String hrStr = String.valueOf(hr);

        return hrStr;
    }
    public class DownloadFileAsync extends AsyncTask<String, String, String> {


        private Context mContext;
        String filePath="";
        WaitDialog dialog;

        public DownloadFileAsync(Context context) {
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            dialog= WaitDialog.show(mContext, "", "");


            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            int count = 0;

            try {
                Thread.sleep(100);
                URL url = new URL(params[0].toString());
                URLConnection conexion = url.openConnection();
                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();

                Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);
                filePath= Util.getFilePath(mContext, params[1]);
                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(filePath);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    Util.debug("value"+ (int) ((total * 100) / lenghtOfFile));
//					publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();

                // 작업이 진행되면서 호출하며 화면의 업그레이드를 담당하게 된다
//				publishProgress("progress", 1, "Task " + 1 + " number");

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            // 수행이 끝나고 리턴하는 값은 다음에 수행될 onProgressUpdate 의 파라미터가 된다
            return null;
        }

        @Override
        protected void onProgressUpdate(String... progress) {

//			 if (progress[0].equals("progress")) {
//				 	dialog.setProgress(Integer.parseInt(progress[1]));
//				 	dialog.setMessage(progress[2]);
//			    } else if (progress[0].equals("max")) {
//			    	dialog.setMax(Integer.parseInt(progress[1]));
//			    }
        }

        @Override
        protected void onPostExecute(String unused) {

            dialog.dismiss();
            Toast.makeText(mContext, "파일 다운로드 완료", Toast.LENGTH_SHORT).show();
            dAdapter.notifyDataSetChanged();
            try {
                FileOpen.openFile(self, new File(filePath));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
