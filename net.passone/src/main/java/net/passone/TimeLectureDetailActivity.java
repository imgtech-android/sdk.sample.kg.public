package net.passone;


import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Browser;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.passone.adapter.AdapterItemManager;
import net.passone.adapter.Api;
import net.passone.adapter.DBmanager;
import net.passone.adapter.OnResponseListener;
import net.passone.adapter.TimeLectureAdapter;
import net.passone.common.CUser;
import net.passone.common.DownLoad;
import net.passone.common.IntentModelActivity;
import net.passone.common.StaticVars;
import net.passone.common.Util;
import net.passone.container.CourseInfo;
import net.passone.container.LecDetailInfo;
import net.passone.container.LecDetailItem;
import net.passone.container.Mp3Info;
import net.passone.container.PlayResultInfo;

import org.andlib.helpers.Logger;

import java.io.File;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import kr.imgtech.lib.zoneplayer.data.IntentDataDefine;

public class TimeLectureDetailActivity extends IntentModelActivity implements IntentDataDefine,OnClickListener,OnResponseListener,DownLoad.Communicator {
	ImageButton btn_login;
	Context context;
	OnResponseListener callback;
	Handler handler;
	ListView list_lecture;
	ArrayList<LecDetailItem> lectureList=new ArrayList<LecDetailItem>();
	int cindex=0,progress,totaltime,days;
	Dialog dialog;
	boolean mode_complete=false;

	TimeLectureAdapter lAdapter;
	String course,beginDate,lastLectureId,title,orderid;
	LinearLayout layout_downtools;
	boolean isDown=false,mode_all=false,isCancel=false;
	int chk_cnt=0,down_cnt=0,chk_total=0,complete_cnt=0,ing_cnt=0;
	long totalsize=0;
	Button btn_all, btn_down, btn_del;
	ArrayList<LecDetailItem> down_list=new ArrayList<LecDetailItem>();
	int recent_position=0,leccode=0;
	String filename="",tmp_file="",filepath="",uid="",etc="",packageid="",coupontype="",category="",status=""
			,enddate="",teacher="",lecturecnt="",subject="",price="",book="";
	private int down_no = 0;
	public String err_msg = "";
	public String down_msg = "";
	private int down_load_end=0;
	public String down_arg = "";
	DBmanager db_manager;
	SQLiteDatabase db;
	DownLoad drmdownload;
	ImageButton btn_dcancel;
	TextView tv_total;
	TextView tv_subject;
	TextView tv_progress,tv_result;
	ProgressBar down_progress,total_progress;
	LecDetailItem selectItem;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.timeclass_detail);
		((TextView)findViewById(R.id.tv_apptitle)).setText("강의목록");
		((ImageView)findViewById(R.id.btn_rightmenu)).setVisibility(View.VISIBLE);
		((ImageView)findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_down);
		((ImageView)findViewById(R.id.btn_rightmenu)).setOnClickListener(this);
		((ImageView)findViewById(R.id.btn_back)).setVisibility(View.VISIBLE);
		((ImageView)findViewById(R.id.btn_back)).setImageResource(R.drawable.btn_gotime);
		((ImageView)findViewById(R.id.btn_back)).setOnClickListener(this);
		((ImageView)findViewById(R.id.btn_lecsample)).setOnClickListener(this);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		course = getIntent().getExtras().getString("course");
		etc=getIntent().getExtras().getString("etc");
		beginDate = getIntent().getExtras().getString("beginDate");
		days = getIntent().getExtras().getInt("days");
		totaltime = getIntent().getExtras().getInt("totalTime");
		title = getIntent().getExtras().getString("title");
		coupontype=getIntent().getExtras().getString("couponType");
		category=getIntent().getExtras().getString("category");
		status=getIntent().getExtras().getString("status");
		teacher=getIntent().getExtras().getString("teacher");
		subject=getIntent().getExtras().getString("subject");
		lecturecnt=getIntent().getExtras().getString("lectureCount");
		price=getIntent().getExtras().getString("price");
		book=getIntent().getExtras().getString("book");

		((TextView)findViewById(R.id.tv_time_coursename)).setText(title);
		((TextView)findViewById(R.id.tv_price)).setText(price);
		((TextView)findViewById(R.id.tv_book)).setText(book);

		Date d = Util.parseDate(beginDate);
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.DATE, days);
		Date today = new Date();
		Date eDate=new Date();
		Date bDate = getDate(beginDate);
		eDate = c.getTime();
		enddate=formatDate(eDate);
		if(today.getTime()>eDate.getTime())
		{
			today=eDate;
		}
		int max = (int) ((eDate.getTime() - bDate.getTime()) / 86400);
		int recprogress = (int) ((today.getTime()-bDate.getTime()) / 86400);
		Util.debug("beginDate:"+bDate.getTime()+bDate.toString());
		Util.debug("endDate:"+eDate.getTime()+eDate.toString());
		Util.debug("Date" + (today.getTime() / 26400) + today.toString());

		Util.debug("max:endDate- bDate" + max);
		Util.debug("progress:endDate- Date()" + progress);
		context=this;
		callback = this;
		db_manager = new DBmanager(this,"UserInfo.db");
		db=db_manager.getReadableDatabase();

		btn_all=(Button)findViewById(R.id.btn_allselect);
		btn_all.setOnClickListener(this);
		btn_del=(Button)findViewById(R.id.btn_delete);
		btn_down=(Button)findViewById(R.id.btn_download);
		btn_del.setOnClickListener(this);
		btn_down.setOnClickListener(this);
		layout_downtools=(LinearLayout)findViewById(R.id.layout_downtools);
		((ImageButton)findViewById(R.id.btn_lecsample)).setOnClickListener(this);
		((ImageButton)findViewById(R.id.btn_info)).setOnClickListener(this);

		list_lecture=(ListView)findViewById(R.id.list_lecture);
		list_lecture.setItemsCanFocus(true);
		list_lecture.setFocusable(false);
		list_lecture.setFocusableInTouchMode(false);
		list_lecture.setClickable(false);
		list_lecture.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				LecDetailItem item = StaticVars.lecDetailItems.get(position);
				CheckBox checkbox = (CheckBox)view.getTag(R.id.chk_lec);
				Util.debug("click:"+position);
				selectItem=item;
					if (layout_downtools.getVisibility()== View.VISIBLE) {
						checkbox.setChecked(!checkbox.isChecked());
						item.setSelected(checkbox.isChecked());
						countChk(checkbox.isChecked());
						//					 dadapter.chkItem(position);
					} else {

						_api.playStream(item.getUid(), item.getCourseId(), item.getEtc(), context, callback);

					}

			}
		});

		super.onCreate(savedInstanceState);
	}
	@Override
	public void onResponseReceived(int api, Object result) {
		Util.debug("Login result    :   " + result);
		switch(api) {
			case Api.PLAYSTREAM:
				PlayResultInfo playResult = (PlayResultInfo)result;
				if(playResult.result.equals("OK")) {
					/*final Intent intentSubActivity =
							new Intent(context, YoondiskPlayerActivity.class);*/
					if (selectItem.getIsDown() == 0){
                        testPlayer();
						//intentSubActivity.putExtra("vurl", playResult.url.replace("\\/", "/"));
//						intentSubActivity.putExtra("vurl","http://heesomobile.wjpass.hd.stream.scs.skcdn.co.kr/ssamplussp/15/03/11ks-ir/11ks-ir-1504001.mp4?SM4558_38087044");
						//intentSubActivity.putExtra("playtype", "stream");
						if(playResult.url.trim().length()==0 || !playResult.url.contains(".mp4"))
						{
							Util.alert(context, "재생 안내", "파일이 올바르지 않습니다. 해당 문제가 반복되면 학습지원센터로 문의 바랍니다. 02-870-8500", "확인", null, null, null);
							break;
						}
					}
					else {
						//intentSubActivity.putExtra("vurl", "file://" + Util.getFilePath(getApplicationContext(), selectItem.getFilepath()));
						//intentSubActivity.putExtra("playtype", "download");

					}
					//intentSubActivity.putExtra("title", selectItem.getTitle());
					//intentSubActivity.putExtra("uid", selectItem.getUid());
					//intentSubActivity.putExtra("price", true);
					//intentSubActivity.putExtra("position", selectItem.getCurrentTime());
					//intentSubActivity.putExtra("etc", playResult.etc);


					if (!Util.isWifiConnected(context) && selectItem.getIsDown() == 0) {
						Util.alert(context, getString(R.string.app_name), getString(R.string.video_3g_alert), "확인", "취소", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int which) {
								//startActivity(intentSubActivity);

							}
						}, new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int which) {
							}
						});
					} else{

                    }
						//startActivity(intentSubActivity);
				}
				else
				{
					Util.alert(context, "재생 안내", playResult.message, "확인", null, null, null);

				}
				break;
			case Api.LECTURE:

				if (result != null) {
					AdapterItemManager.AddLectureList((List<LecDetailInfo>) result);
					if(StaticVars.lecDetailItems.size()>0)
					{
						chk_total=StaticVars.lecDetailItems.size();

						Cursor cursor=null;
						for(LecDetailItem lecItem:StaticVars.lecDetailItems)
						{
							cursor = db.query("time_download",null,"lecture=? and courseid=? ",new String[]{lecItem.getUid(), String.valueOf(course)},null,null,null);
							if(cursor.getCount()>0)
							{
								Util.debug("already down"+lecItem.getUid());
								lecItem.setIsDown(1);
								lecItem.setFilePath(course+"_"+lecItem.getUid()+".mp4");
							}

						}
						cursor.close();

						lAdapter=new TimeLectureAdapter(context,StaticVars.lecDetailItems);
						lAdapter.setCourseId(course);
						lAdapter.setMode(isDown);
						list_lecture.setAdapter(lAdapter);

					}

				}
				break;
			case Api.COURSESINGLE:
				int size=((List<CourseInfo>) result).size();
				if(size>0)
				{


				}
				break;
			case Api.LECDOWN:
				PlayResultInfo resultInfo=(PlayResultInfo)result;
				if(resultInfo.result.equals("OK"))
				{
					String vodUrl=resultInfo.url.replace("\\/","/");
					etc=resultInfo.etc;
					down_arg=vodUrl;
//					down_arg="http://heesomobile.wjpass.hd.stream.scs.skcdn.co.kr/ssamplussp/15/03/11ks-ir/11ks-ir-1504001.mp4?SM4558_38087044";

					if(dialog==null)
						showDownload();
					if(down_progress!=null)
					{
						total_progress.setProgress(down_cnt);
						tv_total.setText("전체 ( "+(down_cnt + 1)+"/"+down_list.size()+" )");
						tv_subject.setText(filename);
					}
					//다운로드 기능 선언부.
					drmdownload=  new DownLoad();
					drmdownload.setCommunicator(this);
	                 /*파일 다운로드 시작 url , 로컬저장경로 . */
					String filePath= Util.getFilePath(context, filepath);
					Util.debug("down filepath:"+filePath);

					drmdownload.drm_download_start(down_arg, filePath);
				}
				else
				{
					Util.ToastMessage(this,resultInfo.message);
					if(dialog!=null)
					{
						dialog.dismiss();

					}
				}
				break;
			case Api.PLAYSAMPLE:
				Mp3Info mp3Info=(Mp3Info)result;
				/*final Intent intentSubActivity =
						new Intent(context, YoondiskPlayerActivity.class);*/
					//intentSubActivity.putExtra("vurl", mp3Info.url.replace("\\/", "/"));
//							intentSubActivity.putExtra("vurl","http://heesomobile.wjpass.hd.stream.scs.skcdn.co.kr/ssamplussp/15/03/11ks-ir/11ks-ir-1504001.mp4?SM4558_38087044");
					//intentSubActivity.putExtra("playtype", "stream");

				//intentSubActivity.putExtra("title", title+" (샘플)");
				//intentSubActivity.putExtra("uid", "");
				//intentSubActivity.putExtra("price", false);
				//intentSubActivity.putExtra("position", 0);
				//intentSubActivity.putExtra("etc", "");


				if (!Util.isWifiConnected(context)) {
					Util.alert(context, getString(R.string.app_name), getString(R.string.video_3g_alert), "확인", "취소", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							//startActivity(intentSubActivity);

						}
					}, new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
						}
					});
				} else{

                }
					//startActivity(intentSubActivity);
				break;
		}
	}

	@Override
	protected void onResume() {

		_api.TimeLecture(course, etc, context, callback);

		super.onResume();
	}

	private void showDownload() {
		// TODO Auto-generated method stub
		dialog = new Dialog(context,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.down_dialog);
		dialog.setCancelable(false);

		//there are a lot of settings, for dialog, check them all out!
		tv_total = (TextView) dialog.findViewById(R.id.tv_total);
		tv_subject = (TextView) dialog.findViewById(R.id.tv_downtitle);
		tv_progress = (TextView) dialog.findViewById(R.id.tv_progress);
		down_progress=(ProgressBar)dialog.findViewById(R.id.down_progress);
		total_progress =(ProgressBar)dialog.findViewById(R.id.total_progress);
		total_progress.setMax(down_list.size());
		total_progress.setProgress(down_cnt);
		tv_total.setText("전체 ( "+(down_cnt+1)+"/"+down_list.size()+" )");
		tv_subject.setText(filename);
		btn_dcancel = (ImageButton) dialog.findViewById(R.id.btn_dcancel);
		btn_dcancel.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				dialog.dismiss();
				if (drmdownload!=null  )
				{
					if(!mode_complete)
					{
						isCancel=true;
						drmdownload.drm_download_cancel();
					}

				}
				clearDown();
				mode_complete=false;
				dialog=null;

			}

		});

		dialog.show();
	}
	public Date getDate(String key) {
		if (key == null) return null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return format.parse(key);
		} catch (ParseException e) {
			return null;
		}

	}
	public static String formatDate(Date date) {
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
			case R.id.btn_rightmenu:
				if(lAdapter!=null) {
					isDown = !isDown;

					if (isDown) {
						setDownloadTools();
						findViewById(R.id.layout_downtools).setVisibility(View.VISIBLE);
						((ImageButton) findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_ok);
						lAdapter.setMode(true);

					} else {
						findViewById(R.id.layout_downtools).setVisibility(View.GONE);
						((ImageButton) findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_down);

						lAdapter.setMode(false);

					}
				}
				break;
			case R.id.btn_allselect:
				mode_all=!mode_all;
				chk_cnt=0;
				Util.debug("all click" + mode_all);
				if(mode_all)
				{
					for(LecDetailItem item:StaticVars.lecDetailItems)
					{
						item.setSelected(true);
					}
					btn_all.setText("선택해제");
				}
				else
				{
					for(LecDetailItem item:StaticVars.lecDetailItems)
					{
						item.setSelected(false);
					}
					down_list.clear();
					down_cnt=0;
					chk_cnt=0;
					btn_all.setText("전체선택");
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						lAdapter.setList(StaticVars.lecDetailItems);
						lAdapter.notifyDataSetChanged();

					}
				});

				break;
			case R.id.btn_delete:
				Util.alert(this, "다운로드 삭제", "삭제하시겠습니까?", "확인", "취소", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {
						makeChkList(false);

					}
				}, null);
				break;
			case R.id.btn_download:
				if (Util.isWifiConnected(context)) {
					makeChkList(true);
				} else {
					Util.alert(context, "3G/LTE 다운로드", "다운로드 하시겠습니까?", "확인", "취소", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							makeChkList(true);

						}
					}, null);
				}
				break;

			case R.id.btn_info:
				startActivity(new Intent(this,LectureInfoActivity.class).putExtra("prof",teacher).putExtra("course_title", title).putExtra("class_period",beginDate+" ~ "+enddate)
				.putExtra("class_cnt", lecturecnt).putExtra("class_state", status).putExtra("class_kind",category).putExtra("class_type",coupontype).putExtra("class_total", String.valueOf(totaltime)).putExtra("class_subject",subject));
				break;
			case R.id.btn_back:
				finish();
				break;
			case R.id.btn_lecsample:
					_api.PlaySample(course,context,callback);
				break;
		}
		super.onClick(v);
	}
	public void countChk(boolean ischk)
	{

		if(ischk)
		{
			chk_cnt++;
		}
		else
		{
			if(chk_cnt>0)
				chk_cnt--;
		}
		if(chk_cnt==0)
		{

//			btn_down.setEnabled(false);
//			btn_del.setEnabled(false);
		}
		else
		{
//			btn_down.setEnabled(true);
//			btn_del.setEnabled(true);

			if(chk_total==chk_cnt)
			{
				btn_all.setText("선택해제");
			}
			else
			{
				btn_all.setText("전체선택");
			}

		}

	}
	public void makeChkList(boolean isdown)
	{
		down_list.clear();

		int size=StaticVars.lecDetailItems.size();
		for(LecDetailItem item:StaticVars.lecDetailItems)
		{
			if(item.isSelected())
			{
				down_list.add(item);
				Util.debug("item:" + item.getUid());
			}


		}

		if(isdown) //다운로드
		{

			if(down_list.size()>0)
			{
				down_cnt=0;
				sendDownload(down_cnt);
			}
		}
		else //삭제
		{

			for(LecDetailItem lec : down_list)
			{
				filepath=course+"_"+lec.getUid()+".mp4";
				delDownload(filepath,lec.getUid());
			}
			clearDown();
			Util.ToastMessage(context, "삭제되었습니다.");

		}

	}
	public void clearDown()
	{
		for (LecDetailItem item : StaticVars.lecDetailItems)
		{
			item.setSelected(false);
			countChk(false);

		}
		Util.debug("dd");
		down_list.clear();
		down_cnt=0;
		chk_cnt=0;
		if(dialog!=null && dialog.isShowing())
		{
			dialog.dismiss();
			dialog=null;

		}
		setDownloadTools();
		if(!isCancel)
		{
			lAdapter.setMode(false);
			isDown=!isDown;
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					findViewById(R.id.layout_downtools).setVisibility(View.GONE);
					((ImageButton) findViewById(R.id.btn_rightmenu)).setImageResource(R.drawable.btn_t_down);
				}
			});
		}

		lAdapter.notifyDataSetChanged();

	}
	public void sendDownload(int position)
	{

		LecDetailItem item=down_list.get(position);

		filename=item.getTitle();
			uid=item.getUid();
			filepath=course+"_"+uid+".mp4";
//        filepath=Util.getDownloadPath(filename);
		if(item.getIsDown()==0)
			_api.PlayDownload(String.valueOf(uid),item.getOrderId(), CUser.userid,course,item.getEtc(),context,callback);
		else
		{
			if(down_cnt<(down_list.size()-1))
			{
				Util.ToastMessage(this,"이미 다운로드 받은 강의입니다.");
				down_cnt++;
				sendDownload(down_cnt);
			}
			else
			{

				clearDown();

			}
		}

	}
	public void delDownload(String filename, String cseq)
	{

		String ext = Environment.getExternalStorageState();
		String Save_folder = Util.getFilePath(getApplicationContext(), filename);

		Util.debug("file exist:" + Save_folder + filename);

		File file = new File(Save_folder);
		if(file.isFile() && file.exists()) {
			file.delete();
		}

				int delete=db.delete("time_download","lecture=? and courseid=? ",new String[]{String.valueOf(cseq), String.valueOf(course)});
				Util.debug("update:" + delete);
				for(LecDetailItem item : StaticVars.lecDetailItems)
				{
					if(item.getUid()==cseq)
					{
						item.setIsDown(0);
					}
				}


	}

	@Override
	public void drm_download_ing(final float total, final float fileLength) {
		Util.debug("total:" + total + ",fileLength:" + fileLength);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (down_progress != null) {
					down_progress.setMax(Math.round(fileLength));

					down_progress.setProgress(Math.round(total));
					if (tv_progress != null)
						tv_progress.setText("( " + Util.toDisplayMB((long) total, (long) fileLength) + " )");

					//					tv_progress.setText("( "+(int)bytesWritten+"MB/"+(int)bytesTotal+"MB )");
					//Log.i(TAG, String.format("onProgressUpdate() : %d / %d", bytesWritten, bytesTotal));
				}
			}
		});

	}

	@Override
	public void drm_download_cancel(int err) {
		mode_complete=false;
		isCancel=true;
		ContentValues cv=new ContentValues();
		cv.clear();
		cv.put("filename", "");

		int update=db.update("time_download",cv,"lecture=? and courseid=?",new String[]{String.valueOf(uid), String.valueOf(course)});
		Util.debug("update:" + update);
		runOnUiThread(new Notifier(err));
		drmdownload=null;
	}
	class Notifier implements Runnable {

		private int error;

		public Notifier(int error) {
			this.error = error;
		}

		public void run() {
			switch (error)
			{
				case 1000:
					Util.alert(context, "다운로드 오류", "남은 여유 공간이 요청한 파일크기 보다 작습니다.", "확인", "null", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							clearDown();
						}
					}, null);
					break;
				case 1100:
					Util.alert(context, "다운로드 오류", "이미 다운로드 받은 파일이거나 인터넷이 불안정하여 다운로드가 취소되었습니다.", "확인", null, new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							clearDown();
						}
					}, null);
					Util.ToastMessage(context,"err:"+error);
					break;
				case 1200:
					clearDown();


					break;
				default:
					Util.ToastMessage(context, "http error code:" + error);
					clearDown();
					break;
			}

			// err < 1000 보다 작을경우 에러 숫자는 http status code 을 의미함.
			// 참조 : http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html

			// err : 1000 = 남은 여유 공간이 요청한 파일크기 보다 작습니다.
			// err : 1100 = 파일 다운로드중 인터넷 환경이 변경되었거나 인터넷이 불안정하여 다운로드가 취소되었습니다.
			// err : 1200 = 다운로드 취소 요청으로 다운로드가 취소되었습니다.

		}
	};
	@Override
	public void drm_download_end(String downfile) {
		mode_complete=true;
		isCancel=false;

		ContentValues cv=new ContentValues();
		cv.clear();
		cv.put("mno", CUser.mno);
		cv.put("etc", etc);
		cv.put("courseid", course);
		cv.put("lecture",uid);
		cv.put("filename", Util.getFilePath(getApplicationContext(), filepath));
		cv.put("course_title",title);
		cv.put("lec_title", filename);
		Util.debug("update:" +  uid + "," + course);

		int update=db.update("time_download",cv,"lecture=? and courseid=?",new String[]{String.valueOf(uid), String.valueOf(course)});
		Util.debug("update:" + update);
		if(update==0)
			db.insert("time_download","",cv);
		for(LecDetailItem item : StaticVars.lecDetailItems)
		{
			if(item.getUid()==uid)
			{
				item.setIsDown(1);
				item.setFilePath(filepath);
			}
		}
		Util.debug("down cnt:" + down_cnt + "/down list:" + down_list.size());
		if(down_cnt<(down_list.size()-1))
		{
			down_cnt++;
			sendDownload(down_cnt);
		}
		else
		{

			clearDown();

		}
		drmdownload=null;
	}

	@Override
	protected void onDestroy() {
		if(db!=null)
			db.close();
		getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		super.onDestroy();
	}
	public void setDownloadTools()
	{
		complete_cnt=0;
		ing_cnt=0;
		for (LecDetailItem lecItem : StaticVars.lecDetailItems) {
			String chkfilename=	course+"_"+lecItem.getUid()+".mp4";
			String chkfilepath= Util.getFilePath(context, chkfilename);
			File dfile=new File(chkfilepath);
			if(dfile.isFile() && dfile.exists())
			{
				if(lecItem.getIsDown()>0)
					complete_cnt++;
				else
					ing_cnt++;

			}

		}

		if(chk_total==complete_cnt)
		{
			btn_del.setEnabled(true);
			btn_down.setEnabled(false);
		}
		else
		{
			if(complete_cnt>0 || ing_cnt>0)
			{
				btn_del.setEnabled(true);
				btn_down.setEnabled(true);
			}
			else {
				btn_del.setEnabled(false);
				btn_down.setEnabled(true);
			}
		}
	}

    /**
     * 테스트 앱 - 스트리밍 실행
     */
    private void testPlayer() {
        Log.d("qwer123456","겟패키지네임값:" + getPackageName());
        Uri.Builder uriBuilder = new Uri.Builder()
                .scheme(getString(R.string.scheme_kg))
                .authority(getString(R.string.host_player))
                .appendQueryParameter(SITE_ID, MainActivity.KG_ID)

                // info-url 및 data 는 아래 샘플 참조해서 고객사에서 설정
                .appendQueryParameter(INFO_URL, "http://m.imgtech.co.kr/mobile/kg/test/info_url.php")
                .appendQueryParameter(DATA, "play;guest;이명학.Prestart");

        Logger.d(uriBuilder.toString());

        // 플레이어 실행
        Intent intent;
        try {
            // Intent Scheme 실행
            intent = Intent.parseUri(uriBuilder.toString(), Intent.URI_INTENT_SCHEME);

            intent.addCategory(Intent.CATEGORY_BROWSABLE);
            intent.putExtra(Browser.EXTRA_APPLICATION_ID, getApplication().getPackageName());

            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}