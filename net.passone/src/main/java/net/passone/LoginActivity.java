package net.passone;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;

import net.passone.adapter.Api;
import net.passone.adapter.DBmanager;
import net.passone.adapter.HttpHelper;
import net.passone.adapter.OnResponseListener;
import net.passone.common.AES256Cipher;
import net.passone.common.CUser;
import net.passone.common.Constants;
import net.passone.common.Environments;
import net.passone.common.IntentModelActivity;
import net.passone.common.Util;
import net.passone.container.ApiResult;

import org.apache.http.cookie.Cookie;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class LoginActivity extends IntentModelActivity implements View.OnClickListener, OnResponseListener {
    ImageButton btn_login,btn_join;
    EditText edit_id,edit_pwd;
    CheckBox chk_auto,chk_push;
    LoginActivity self=null;
    Context context;
    DBmanager db_manager;
    SQLiteDatabase db;
    OnResponseListener callback;
    String userid="",userpw="",device_token="";
    CookieManager cookieManager;
    HttpHelper hh=new HttpHelper();
    String aesKey="",encode_pwd="";
    int pushchk=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        //		setVisibleTitle(View.VISIBLE);
        //		setTitleImage(R.drawable.login_title);
        self=this;
        context=this;
        callback=this;
        CookieSyncManager.createInstance(this);
        cookieManager= CookieManager.getInstance();
        CUser.autochk=1;
        initializeDebuggable();
        loadEnvironments();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        final String dateString=format.format(new Date().getTime());


        initialize();
    }

    public void loadEnvironments() {
        Environments.load(this);
        if (!Environments.PREFERENCE_EXIST) {
            Environments.save(this);
        }
    }

    public void initialize() {
        edit_id = (EditText)findViewById(R.id.edit_id);
        edit_pwd = (EditText)findViewById(R.id.edit_password);
        edit_id.setOnClickListener(self);
        edit_pwd.setOnClickListener(self);
        ((ImageButton)findViewById(R.id.btn_service)).setOnClickListener(self);
        /////////////////////////
        if(BuildConfig.DEBUG) {
//            edit_id.setText("sh73zzang");
//            edit_pwd.setText("gmlthahqkdlf");
        }
        ///////////////////////////////
        chk_auto=(CheckBox)findViewById(R.id.chk_autologin);
        chk_push=(CheckBox)findViewById(R.id.chk_autopush);

        if(CUser.autochk>0)
            chk_auto.setChecked(true);
        else
            chk_auto.setChecked(false);
        if(CUser.pushchk==1)
            chk_push.setChecked(true);
        else
            chk_push.setChecked(false);
        chk_auto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                CUser.autochk=isChecked?1:0;
            }
        });
        chk_push.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!chk_push.isChecked()) {
                    pushchk = 0;
                    _api.RegisterApi("N","N", CUser.device_token,CUser.device_token,context,callback);

                } else {

                    pushchk = 1;
                    _api.RegisterApi("N", "Y", CUser.device_token, CUser.device_token, context, callback);



                }
            }
        });
        btn_login = (ImageButton)findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        btn_join = (ImageButton)findViewById(R.id.btn_join);
        btn_join.setOnClickListener(this);

    }
    public void setPushchk(int pushchk)
    {
        DBmanager db_manager= new DBmanager(this,Constants.DATABASE_NAME);
        SQLiteDatabase db=db_manager.getReadableDatabase();
        ContentValues cv= new ContentValues();

        cv.put("pushchk", pushchk);
        int pushcnt=db.update("device",cv,null,null);
        if(pushcnt==0) {
            String sql = "insert into device (device_token,pushchk) values('',"+pushcnt+")";
            db.execSQL(sql);
        }
        db.close();
        CUser.pushchk=pushchk;
    }
    public void onResponseReceived(int api, Object result) {
        // TODO Auto-generated method stub
        Util.debug("Login result    :   " + result);
        try{
            switch(api) {
                case Api.REGISTERAPI:
                    if(result!=null)
                    {
                        ApiResult regapi = (ApiResult) result;
                        Calendar calendar = Calendar.getInstance();
                        Date date = calendar.getTime();
                        String nowtime = new SimpleDateFormat("yyyy년 MM월 dd일").format(date);
                        String process="";
                        if(pushchk==0)
                            process="알림 수신거부 처리 완료";
                        else if(pushchk==1)
                            process="알림 수신동의 처리 완료";
                        if(regapi.result.trim().equals("OK"))
                        {
                            Util.alert(this, "광고성 푸시 알림 수신 동의 안내", "1. 전송자: KG에듀원 \n\n"
                                    + "2. 처리 일자: "+nowtime+"\n\n"
                                    + "3. 처리 내용: 해당 기기 "+process+"\n\n"
                                    +"설정 메뉴에서 알림 설정 변경 가능합니다.", "확인", null, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    setPushchk(pushchk);
                                }

                            }, null);
                        }
                        else
                        {
                            Util.ToastMessage(this,"서버와의 통신에 일시적으로 문제가 발생되었습니다. 차후 다시 시도해주세요.");
                            chk_push.setChecked(!chk_push.isChecked());
                        }
                    }
                    else
                    {
                        Util.ToastMessage(this,"서버와의 통신에 일시적으로 문제가 발생되었습니다. 차후 다시 시도해주세요.");

                        chk_push.setChecked(!chk_push.isChecked());

                    }
                    break;
                case Api.SIGN :

                    if (result != null) {

                        ApiResult apiResult = (ApiResult)result;
                        Log.d("qwer123456","api리절트값:" + apiResult.result);
                        if (apiResult.result.equals("OK")) {

                            cookieManager = CookieManager.getInstance();
                            List<Cookie> cookies=hh.getCookies();
                            Cookie sessionInfo = null;

                            for (Cookie cookie : cookies ) {
                                sessionInfo = cookie;
                                String cookieString = sessionInfo.getName() + "="
                                        + sessionInfo.getValue() + "; path="
                                        + sessionInfo.getPath()+"; domain="+sessionInfo.getDomain();
                                Util.debug("sign:"+cookieString);
                                if(sessionInfo.getName().equals("PASSONE_IDX"))
                                {
                                    CUser.mno= URLDecoder.decode(sessionInfo.getValue());
                                }
                                if(sessionInfo.getName().equals("PASSONE_UID"))
                                {
                                    CUser.userid= URLDecoder.decode(sessionInfo.getValue());
                                }

                                cookieManager.setCookie(sessionInfo.getDomain(), cookieString);
                                CookieSyncManager.getInstance().sync();
                            }

                            CookieSyncManager.getInstance().startSync();

                            addEvent();
                        }
                        else  {
                            String errmsg="";
                            Log.d("qwer123456","apiresult의 메세지값:" + apiResult.message);
                            Util.ToastMessage(self, apiResult.message);
                        }

                    } else {
                        Util.PopupMessage(self, getResources().getString(R.string.api_http_alert));
                    }
                    break;


            }
        }catch(NullPointerException e)
        {
        }
    }
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        userid=edit_id.getText().toString();
        userpw=edit_pwd.getText().toString();
        Util.debug("pw:"+encode_pwd);

        switch(v.getId())
        {
            case R.id.btn_login:
                if(userid.trim().length()==0)
                    Util.ToastMessage(self, getString(R.string.id_null));
                else if(userpw.trim().length()==0)
                    Util.ToastMessage(self, getString(R.string.pwd_null));
                else {
                    encode_pwd= AES256Cipher.encrypt(userpw, Constants.aesKey);

                    _api.Login(userid, encode_pwd, CUser.device_token, context, callback);
                }
                break;
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.btn_service:
                encode_pwd= AES256Cipher.encrypt(Constants.debug_pw, Constants.aesKey);
                userid="mobileguest";
                _api.Login(userid, encode_pwd, CUser.device_token, context, callback);

                break;
            case R.id.btn_join:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.joinUrl));
                startActivity(intent);
                break;
        }
        super.onClick(v);
    }


    public void initializeDebuggable() {
        Constants.debuggable =
                (0 != (getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE));
    }
    private void addEvent() {
        db_manager = new DBmanager(this,"UserInfo.db");
        db = db_manager.getWritableDatabase();

        String sql = "delete from userinfo";
        db.execSQL(sql);
        // 로그??초기 ?�보 ??��


        sql = "insert into userinfo ( userid, mno, userpw, autochk, nick, pushchk) values ('"+userid+"', '"+CUser.mno+"', '"+encode_pwd+"', '"+CUser.autochk+"','"+CUser.nicname+"', '"+CUser.pushchk+"')";
        db.execSQL(sql);
        ContentValues cv= new ContentValues();
        cv.put("version", Util.getVersion(self));
        Util.debug(CUser.autochk+"");
//		sql = "insert into appinfo (version) values ('"+Util.getVersion(self)+"')";
//		db.execSQL(sql);
        db.close();
        CUser.userid=userid;

        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        CUser.autochk=0;
    }
}
