package net.passone;


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import net.passone.adapter.APIAgent;
import net.passone.common.IntentModelActivity;
import net.passone.common.Util;

public class PushNoticeActivity extends IntentModelActivity {
	private ProgressDialog progressDialog;
	String _url="",idx="";
	boolean isRunning=true;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		((TextView)findViewById(R.id.tv_apptitle)).setText("공지사항");

		Bundle bun = this.getIntent().getExtras();
		_url = bun.getString("sendUrl");
		isRunning = bun.getBoolean("isAppRunning");

		if(getIntent().getExtras().getString("idx")!=null)
			idx=bun.getString("idx");
		Util.debug("url:" + _url + ",idx:" + idx + ",send:" + bun.getString("sendUrl"));

		if(_url.length()==0)
			_url= String.format(APIAgent.BASE_URL_FORMAT, getString(R.string.api_prefix),"V12") + "smart.html";

	



		final WebView webView = (WebView) findViewById(R.id.webView);
		webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
		webView.getSettings().setJavaScriptEnabled(true);  // ���信�� �ڹٽ�ũ��Ʈ���డ��
		// Bridge 인스턴스 등록
		webView.addJavascriptInterface(new AndroidBridge(), "android");
		webView.setHorizontalScrollBarEnabled(true); // 세로 scroll 제거
		webView.setVerticalScrollBarEnabled(false); // 가로 scroll 제거
		webView.getSettings().setBuiltInZoomControls(true);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (url.startsWith("tel:")) {
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					startActivity(intent);
				} 	else if(!url.startsWith("http://api.passone.net"))
				{
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					startActivity( intent );
				}
				else {
					view.loadUrl(url);

				}
				return true;
			}


			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				try {
					if (progressDialog == null) {
						progressDialog = new ProgressDialog(self);
						progressDialog.setMessage("로딩중입니다...");
						progressDialog.show();
					}
				} catch (Exception e) {
				}
				super.onPageStarted(view, url, favicon);
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				try {
					if (progressDialog != null && progressDialog.isShowing()) {
						progressDialog.dismiss();
						progressDialog = null;
					}
					if (url.endsWith(".mp4")) {
						Intent i = new Intent(Intent.ACTION_VIEW);
						Uri uri = Uri.parse(url);
						i.setDataAndType(uri, "video/mp4");
						startActivity(i);
					}
				} catch (Exception e) {
				}
				CookieSyncManager.getInstance().sync();

			}


			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

			}
		});
		findViewById(R.id.btn_back).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (webView.getUrl().equals(_url)) {
					if (!isRunning)
						startActivity(new Intent(PushNoticeActivity.this, IntroActivity.class));

					finish();
				} else
					webView.goBack();
			}
		});
		webView.loadUrl(_url);

	}
	private final Handler handler = new Handler();

	private class AndroidBridge {
		public void callAndroid(final String msg) { // must be final
			handler.post(new Runnable() {
				public void run() {
					finish();


				}
			});
		}

	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
	

		super.onNewIntent(intent);
	}
	
}
