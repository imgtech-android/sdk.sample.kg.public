package net.passone;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMBaseIntentService;

import net.passone.adapter.APIAgent;
import net.passone.adapter.Adapter;
import net.passone.adapter.DBmanager;
import net.passone.adapter.OnResponseListener;
import net.passone.common.CUser;
import net.passone.common.Constants;
import net.passone.common.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;


public class GCMIntentService extends GCMBaseIntentService implements OnResponseListener {
	 String registration_id = null;
	 String c2dm_msg = "",send_idx="",send_kind="",msg_img="",fromid="",send_url="";
	DBmanager db_manager;
	SQLiteDatabase db;
	OnResponseListener callback;
	public GCMIntentService() {
		super(Constants.senderID);
	}

	private static final String TAG = "passone";

	public void onReceive(Context context, Intent intent) {

		Log.i(TAG, "Device registered: regId = ");


	}
	
	 
	

	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.d("passone", "reg");
		Log.i(TAG, "Device registered: regId = " + registrationId);
		registration_id=registrationId;
		handleRegistration(context);
		/*
		 * 위에 http:// 어쩌구는 GCMRegistration_Id.php의 url 써주면됨.
		 * 맨 앞에부터 url, id, pw, reg_id, msg순이다.
		 */


	}

	@Override
	protected void onUnregistered(Context context, String arg1) {
		Log.i(TAG, "unregistered = " + arg1);
		CUser.device_token="";
		Log.v("C2DM_REGISTRATION", ">>>>>" + "unregistration done, new messages from the authorized sender will be rejected" + "<<<<<");

	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.i(TAG, "CONTEXT : = " + context);
		Log.i(TAG, "new message= ");
		if (intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
			Bundle bun = intent.getExtras();
			for (String key : bun.keySet()) {
				Object value = bun.get(key);
				Log.d(TAG, String.format("%s %s (%s)", key,
						value.toString(), value.getClass().getName()));
			}
			if(bun != null) {
				if(bun.getString("msg")!=null)
				{
					c2dm_msg=bun.getString("msg");
					Util.debug("msg="+c2dm_msg);

				}
				fromid=bun.getString("from");
				if(bun.getString("send_idx")!=null)
				{
					send_idx=bun.getString("send_idx").replace("\\n","");
					Util.debug("send_idx="+send_idx);

				}
				if(bun.getString("send_kind")!=null)
				{
					send_kind=bun.getString("send_kind");
					Util.debug("send_kind="+send_kind);

				}
				if(intent.getExtras().getString("send_url")!=null)
					send_url=intent.getExtras().getString("send_url");
				System.out.println("send_url======>"+send_url);

			}


			GET_GCM();
			if(c2dm_msg!=null)
				Log.i(TAG, c2dm_msg);

		}

	}
//푸시받음
	public void GET_GCM() {

		Thread thread = new Thread(new Runnable() {
			public void run() {

				handler.sendEmptyMessage(0);
			}
		});
		thread.start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			final Context context = getApplicationContext();
			Intent intent = null;

			int duration = Toast.LENGTH_LONG;
			db_manager = new DBmanager(context,"UserInfo.db");
			db = db_manager.getWritableDatabase();
			long thistime= System.currentTimeMillis();
			long beforetime=thistime-(60*60*1000);
			Cursor tw_cursor = 	db.query("event",null,"fromid=? and senddate>=?",new String[]{c2dm_msg, String.valueOf(beforetime)},null,null,null);
			Cursor d_cursor = 	db.query("device",new String[]{"pushchk"},null,null,null,null,null);
			Util.debug(d_cursor.getColumnCount()+"푸시");
			if(d_cursor.getColumnCount()>0)
			{
				d_cursor.moveToFirst();
				for(int i=0; i<d_cursor.getCount();i++)
				{
					CUser.pushchk=d_cursor.getInt(0);

					d_cursor.moveToNext();
				}
			}

			if(tw_cursor.getCount()==0&& CUser.pushchk>0)
			{

				ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
				List<ActivityManager.RunningTaskInfo> runList = am.getRunningTasks(10);
				ComponentName name = runList.get(0).topActivity;
				String className = name.getClassName();
				boolean isAppRunning = false;

				if(className.contains("net.passone")) {
					isAppRunning = true;
				}
				if(isAppRunning == true) {

					//				  Util.ToastMessage(context, "새 퀴즈가 등록되었습니다.");
					//				  startActivity(new Intent(context, PopLoginActivity.class).putExtra("midx", midx).putExtra("cdidx", cdidx).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));

				}
				if(send_kind.equals("3")||send_kind.equals("2")) //공지사항
					intent=new Intent(context, PushNoticeActivity.class).putExtra("sendUrl", send_url).putExtra("send_idx",send_idx).putExtra("isAppRunning", isAppRunning);
				else
					intent=new Intent(context, IntroActivity.class);
				//Activity정의
				//PendingIntent pendingIntent = PendingIntent.getActivity(
				//		context, 0,new Intent(context, PushDialogActivity.class).putExtra("msg_type", msg_type).putExtra("msg_imgrul", msg_imgurl).putExtra("msg", c2dm_msg).putExtra("msg_img",msg_img), PendingIntent.FLAG_CANCEL_CURRENT);

				//Log.d("pushTeset","PushTest");
				NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
				int requestID = (int) System.currentTimeMillis();
				PendingIntent pendingIntent = PendingIntent.getActivity(
						context, requestID,intent, PendingIntent.FLAG_UPDATE_CURRENT);
				String ticker = c2dm_msg;
				String title = "스마트패스원 - ";
				String text = c2dm_msg;
				String apiPrefix=context.getString(R.string.api_prefix);
				if(apiPrefix.equals("passone"))
				{
					title+="공무원";
				}
				else if(apiPrefix.equals("passmd"))
				{
					title+="PEET/편입";
				}
				else if(apiPrefix.equals("miraecpa"))
				{
					title+="CPA/CTA";
				}
				else if(apiPrefix.equals("goodssam"))
				{
					title+="교원임용";
				}
				KeyguardManager km = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);
				// Create Notification Object
				//		    if(GetClassName(context) && !km.inKeyguardRestrictedInputMode()){}
				//		    else if(km.inKeyguardRestrictedInputMode()){

				PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
				PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK , "push");
				wl.acquire();
				Notification notification = new Notification(R.drawable.icon,ticker, System.currentTimeMillis());
				notification.flags = notification.flags | Notification.FLAG_AUTO_CANCEL  ;

				//notification.setLatestEventInfo(context,title, text, pendingIntent);
				nm.notify(11234, notification);
				//		     try {
				//				nm.wait(5000);
				//			} catch (InterruptedException e) {
				//				// TODO Auto-generated catch block
				//				e.printStackTrace();
				//			}
				wl.release();
				//		    }

				ContentValues cv=new ContentValues();
				cv.put("fromid",c2dm_msg);
				cv.put("senddate",thistime);

				db.insert("event","",cv);
			}
			db.close();



		}
	};
    private void showNotification(Context context) {
		// TODO Auto-generated method stub
    	NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
		    PendingIntent pendingIntent = PendingIntent.getActivity(
                    context, 0, new Intent(context, IntroActivity.class), 0);
		    String ticker = c2dm_msg;
		    String title = context.getString(R.string.app_name);
		    String text = c2dm_msg;

		    //?�면 켜짐 ?�무 체크
		    KeyguardManager km = (KeyguardManager)context.getSystemService(Context.KEYGUARD_SERVICE);
		    // Create Notification Object
//		    if(GetClassName(context) && !km.inKeyguardRestrictedInputMode()){}
//		    else if(km.inKeyguardRestrictedInputMode()){

		     PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
		     PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "push");
		     wl.acquire();
//		     Notification notification = new Notification(R.mipmap.icon,ticker, System.currentTimeMillis());
		Notification.Builder builder = new Notification.Builder(context);
		int color = getResources().getColor(R.color.push_bg);

		builder.setContentIntent(pendingIntent)
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
				.setTicker(ticker)
				.setWhen(System.currentTimeMillis())
				.setAutoCancel(true)
				.setContentTitle(title)
				.setContentText(text);
//		Notification notification=new Notification.Builder(context)
//					.setAutoCancel(true).setContentTitle(title).setContentText(text).setSmallIcon(R.drawable.icon).build();
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
			builder.setSmallIcon(R.mipmap.ic_launcher);

		} else {
			builder.setColor(color);
			builder.setSmallIcon(R.mipmap.ic_launcher);

		}
		Notification n = builder.build();


//		     notification.setLatestEventInfo(context,title, text, pendingIntent);
		     nm.notify(1234, n);
//		     try {
//				nm.wait(5000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		     wl.release();
//		    }
	}

	public void showMsg(String msg, int option)
	{
		Toast.makeText(this, msg, option).show();
	}
	@Override
	protected void onError(Context arg0, String errorId) {

	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		Log.v("C2DM_REGISTRATION", ">>>>>" + "Registration failed, should try again later." + "<<<<<");

		return super.onRecoverableError(context, errorId);
	}
	private void handleRegistration(Context context) {
		try{
			CUser.device_token=registration_id;
			Log.d("passone", "registration_id length=" + registration_id.length());
			CUser.device_token=registration_id;
			System.out.println("registration_id complete!!");
			db_manager = new DBmanager(context,"UserInfo.db");
			db = db_manager.getWritableDatabase();
			String sql = "delete from device";
			db.execSQL(sql);
			sql = "insert into device (device_token,pushchk) values('"+registration_id+"', 1)";
			db.execSQL(sql);
			db.close();
			Adapter _api=new Adapter();
			_api.DeviceInfo(CUser.device_token, CUser.device_token, context, this);
			Util.analyticsSend(context, "푸시 리시버", "디바이스 토큰 생성 완료", "ID: " + CUser.userid);

		}catch(Exception e){}
	}

	public void loadMem(Context context)
	{
		PackageManager pm = context.getPackageManager();
		PackageInfo packageInfo = null;

		try {
			packageInfo = pm.getPackageInfo(context.getPackageName(), 0);

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		APIAgent agent=new APIAgent(context);
		try {
			String response = "";
				response = agent.requestPostWithURL(Constants.baseUrl+"memApi.php","reqType","gen","deviceId",Util.getDeviceID(context),"deviceToken",CUser.device_token,
						"platform","android","model", Build.MODEL,"osversion", Build.VERSION.RELEASE,"appversion",packageInfo.versionName,"company", Build.BRAND,"nickName","예비경찰님"
						,"point","0");
			if(!response.equals(getApplicationContext().getText(R.string.api_http_alert)))
			{
				//				Util.ToastMessage(getActivity(), "네트워크 연결을 확인해주세요.");
			    Util.debug("test:"+response);

			try{

				JSONObject obj = new JSONObject(response);

				if(obj.get("result").equals("OK"))
				{

					JSONArray mArry = obj.getJSONArray("memInfo");
					if(mArry.length()>0)
					{
						JSONObject memList = mArry.getJSONObject(0);
						String mid = memList.getString("mid");
						String nickName = memList.getString("nickName");
						DBmanager db_manager = new DBmanager(getBaseContext(),"UserInfo.db");
						SQLiteDatabase db = db_manager.getWritableDatabase();
						ContentValues cv=new ContentValues();
						cv.put("mid", mid);
						
						int update=db.update("userinfo", cv, null, null);
						if(update==0)
						{
							String sql = "insert into userinfo ( mid, nick, udid) values ('"+mid+"', '"+nickName+"', '"+Util.getDeviceID(this)+"')";
							db.execSQL(sql);
						}
					
				
						if(db!=null) db.close();
					}
				
					Message msg = mem_handler.obtainMessage();
					mem_handler.sendMessage(msg);			

				}
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	final Handler mem_handler = new Handler() {
		public void handleMessage(Message msg) {
			
			//list.invalidateViews();
		}
	}; 


	@Override
	public void onResponseReceived(int api, Object result) {
		// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			Util.debug("Login result    :   "+result);	
			switch(api) {


			}
		}

}
