package net.passone.adapter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.passone.common.Constants;
import net.passone.container.ApiResult;
import net.passone.container.ContinueInfo;
import net.passone.container.ContinueLeclInfo;
import net.passone.container.CouponInfo;
import net.passone.container.CouponUsedInfo;
import net.passone.container.CourseInfo;
import net.passone.container.FreeDetailInfo;
import net.passone.container.FreeInfo;
import net.passone.container.LecDataInfo;
import net.passone.container.LecDetailInfo;
import net.passone.container.Mp3Info;
import net.passone.container.PackageInfo;
import net.passone.container.PlayResultInfo;
import net.passone.container.PlayStatusInfo;
import net.passone.container.TimeCourseInfo;
import net.passone.container.Version;
import net.passone.container.VocaInfo;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class AdapterManager extends Api {

	HttpHelper _httpHelper = new HttpHelper();
	ObjectMapper _objectMapper = new ObjectMapper();
	ExecutorService _threadExecutor = Executors.newCachedThreadPool();

	public AdapterManager() {
		super.setBaseUrl(Constants.baseUrl);
	}
	public AdapterManager(int ApiUrl){

			super.setBaseUrl(Constants.baseUrl);

	}
	/**
	 * HTTP를 이용하여 API 요청
	 * 쓰레드를 이용하여 비동기로 처리.
	 * @param lmParams 
	 */
	public void request(Params lmParams) {

		try {
			/* Set parameters for Asynchronous sender. */
			lmParams.setUri(getUri(lmParams.getApi()));
			lmParams.setValueTypeRef(getTypeReference(lmParams.getApi()));

			/*
			 * Execute asynchronous sender thread.
			 */
			_threadExecutor.execute(
					new AsyncSender(_httpHelper, _objectMapper, lmParams));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * API 명령별 컨테이너 클래스형을 리턴한다.<br><br>
	 * @param api
	 * @return TypeReference<?>
	 */	
	private TypeReference<?> getTypeReference(int api) {
		switch(api) {
		case Api.VERSION 					: return new TypeReference<Version>() {};
			case Api.DEVICEINFO 					: return new TypeReference<ApiResult>() {};
			case Api.SIGN 					: return new TypeReference<ApiResult>() {};
			case Api.PACKAGE 					: return new TypeReference<List<PackageInfo>>() {};
			case Api.PACKAGECOURSE 					: return new TypeReference<List<CourseInfo>>() {};
			case Api.COURSE 					: return new TypeReference<List<CourseInfo>>() {};
			case Api.TIMECOURSE 					: return new TypeReference<List<TimeCourseInfo>>() {};
			case Api.PASSVOCA 					: return new TypeReference<List<VocaInfo>>() {};
			case Api.PASSVOCAMP3 					: return new TypeReference<Mp3Info>() {};
			case Api.COUPON 					: return new TypeReference<List<CouponInfo>>() {};
			case Api.COUPONUSED 					: return new TypeReference<List<CouponUsedInfo>>() {};
			case Api.LECTURE 					: return new TypeReference<List<LecDetailInfo>>() {};
			case Api.LECDOWN 					: return new TypeReference<PlayResultInfo>() {};
			case Api.COURSESINGLE					: return new TypeReference<List<CourseInfo>>() {};
			case Api.LECDATA					: return new TypeReference<List<LecDataInfo>>() {};
			case Api.TIMELECTURE					: return new TypeReference<List<LecDetailInfo>>() {};
			case Api.FREELIST			: return new TypeReference<List<FreeInfo>>() {};
			case Api.FREEDETAIL				: return new TypeReference<List<FreeDetailInfo>>(){};
			case Api.PLAYSTREAM				: return new TypeReference<PlayResultInfo>(){};
			case Api.PLAYSTATUS				: return new TypeReference<PlayStatusInfo>(){};
			case Api.PLAYCONTINUE				: return new TypeReference<ContinueInfo>(){};
			case Api.CONTINUELEC				: return new TypeReference<List<ContinueLeclInfo>>(){};
			case Api.PLAYSAMPLE				: return new TypeReference<Mp3Info>(){};
			case Api.REGISTERAPI				: return new TypeReference<ApiResult>(){};

		}
		return null;
	}

	/**
	 * API의 URI를 구한다.
	 * @param api
	 * @return URI
	 */
	private String getUri(int api) {
		if(api<2 || api== Api.REGISTERAPI)
			return Constants.verUrl+getPath(api);
		else
			return (getBaseUrl() + getPath(api));
	}	

	/**
	 * Encode UTF-8
	 * @param params
	 * @return UTF8 Map<String, String>
	 */
	private Map<String, String> toUTF8(Map<String, String> params) {
		Map<String, String> utf8Map = new HashMap<String, String>();
		for (Map.Entry<String, String> entry : params.entrySet())
			utf8Map.put(entry.getKey(), toUTF8(entry.getValue()));
		return utf8Map;				
	}	

	/**
	 * Encode UTF-8
	 * @param String
	 * @return UTF8 String ( or Empty string )
	 */
	private String toUTF8(String s) {
		try {
			return URLEncoder.encode(s, "UTF-8");
		} catch(Exception e) {
			return "";
		}
	}


	/**
	 * Close thread.
	 */
	public void close() {
		if (_threadExecutor != null)
			_threadExecutor.shutdownNow();
	}

	/**
	 * Shutdown connection manager.
	 */
	public void shutdownConnectionManager() {
		if (_httpHelper != null)
			_httpHelper.shutdownConnectionManager();
	}
}