package net.passone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import net.passone.R;
import net.passone.container.VocaItem;

import java.util.ArrayList;

public class VocaStudyAdapter extends BaseAdapter {
	Context context;
	ArrayList<VocaItem> list;
	long firstdate=0;
    int mode=0; //0: 스크롤업 1: 스크롤다운
         public VocaStudyAdapter(Context context, ArrayList<VocaItem> list) {
		this.context=context;
		this.list=list;
	}
	public void setList(ArrayList<VocaItem> list)
	{
		this.list=list;
	}

	public int getCount() {
		// TODO Auto-generated method stub

		return list.size();
	}


	public VocaItem getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}
    public void setMode(int mode)
    {
        this.mode=mode;
    }
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final VocaItem item;
			item=list.get(position);
		
		if (convertView == null)
		{

			convertView =  LayoutInflater.from(context).inflate(R.layout.passvoca_study, parent, false);
				


		}


		return convertView;
	}
	
}


