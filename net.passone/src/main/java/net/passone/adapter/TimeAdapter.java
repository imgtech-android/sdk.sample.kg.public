package net.passone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import net.passone.R;
import net.passone.container.TimeCourseItem;

import java.util.ArrayList;

public class TimeAdapter extends BaseAdapter {
	Context context;
	ArrayList<TimeCourseItem> list;
	long firstdate=0;
    int mode=0; //0: 스크롤업 1: 스크롤다운
         public TimeAdapter(Context context, ArrayList<TimeCourseItem> list) {
		this.context=context;
		this.list=list;
	}
	public void setList(ArrayList<TimeCourseItem> list)
	{
		this.list=list;
	}

	public int getCount() {
		// TODO Auto-generated method stub

		return list.size();
	}


	public TimeCourseItem getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}
    public void setMode(int mode)
    {
        this.mode=mode;
    }
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final TimeCourseItem item;
			item=list.get(position);
		
		if (convertView == null)
		{

			convertView =  LayoutInflater.from(context).inflate(R.layout.time_cell, parent, false);
				


		}
		((TextView)convertView.findViewById(R.id.tv_time_profname)).setText("["+item.getSubject()+"] "+item.getTeacher()+" - "+item.getCategory());
		((TextView)convertView.findViewById(R.id.tv_time_title)).setText(item.getTitle());

		return convertView;
	}
	
public void goDetail(int position)
{
    final TimeCourseItem item;
    item=list.get(position);

}

}
