package net.passone.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.passone.R;
import net.passone.container.VocaDayItem;

import java.util.ArrayList;

public class VocaDayAdapter extends BaseAdapter {
	Context context;
	ArrayList<VocaDayItem> list;
	long firstdate=0;
	int mode=0; //0: day 1: week
	int bad_num=20,good_num=40,best_num=60;
	Typeface typeface;
	public VocaDayAdapter(Context context, ArrayList<VocaDayItem> list, int mode) {
		this.context=context;
		this.list=list;
		this.mode=mode;
		typeface = Typeface.createFromAsset(context.getAssets(), "playregular.ttf");

	}
	public void setList(ArrayList<VocaDayItem> list)
	{
		this.list=list;
	}

	public int getCount() {
		// TODO Auto-generated method stub

		return list.size();
	}


	public VocaDayItem getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}
	public void setMode(int mode)
	{
		this.mode=mode;
	}
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final VocaDayItem item;
		item=list.get(position);

		if (convertView == null)
		{

			convertView =  LayoutInflater.from(context).inflate(R.layout.vocaday_cell, parent, false);



		}
		int sumfinished=item.getSumFinished();
		int sumseen=item.getSumSeen();
		if(mode==0)
		{
			((TextView)convertView.findViewById(R.id.tv_day)).setText(item.getDay()+"");

			((TextView)convertView.findViewById(R.id.tv_study_result)).setText(sumfinished+"/60");
			((TextView)convertView.findViewById(R.id.tv_study_no)).setText(60 - sumfinished + "");
			 bad_num=20;
			good_num=40;
			best_num=60;

		}
		else
		{
			((TextView)convertView.findViewById(R.id.tv_day)).setText(item.getWeek()+"");
			((TextView)convertView.findViewById(R.id.tv_study_result)).setText(sumfinished+"/300");
			((TextView)convertView.findViewById(R.id.tv_study_no)).setText(300 - sumfinished + "");
			bad_num=100;
			good_num=200;
			best_num=300;

		}
		((TextView)convertView.findViewById(R.id.tv_day)).setTypeface(typeface);
		((TextView)convertView.findViewById(R.id.tv_study_result)).setTypeface(typeface);
		((TextView)convertView.findViewById(R.id.tv_study_no)).setTypeface(typeface);

		if (sumseen == 0) {
			((ImageView) convertView.findViewById(R.id.iv_ribbon)).setVisibility(View.INVISIBLE);
		} else if (sumfinished < bad_num ){
			((ImageView) convertView.findViewById(R.id.iv_ribbon)).setVisibility(View.VISIBLE);

			((ImageView) convertView.findViewById(R.id.iv_ribbon)).setImageResource(R.drawable.img_ribbon_bad);
		}
		else if (sumfinished < good_num && sumfinished>=bad_num ){
			((ImageView) convertView.findViewById(R.id.iv_ribbon)).setVisibility(View.VISIBLE);

			((ImageView) convertView.findViewById(R.id.iv_ribbon)).setImageResource(R.drawable.img_ribbon_good);
		}
		else if (sumfinished < best_num && sumfinished>=good_num){
			((ImageView) convertView.findViewById(R.id.iv_ribbon)).setVisibility(View.VISIBLE);

			((ImageView) convertView.findViewById(R.id.iv_ribbon)).setImageResource(R.drawable.img_ribbon_best);
		}else {
			((ImageView) convertView.findViewById(R.id.iv_ribbon)).setVisibility(View.VISIBLE);

			((ImageView) convertView.findViewById(R.id.iv_ribbon)).setImageResource(R.drawable.img_ribbon_perfect);
		}
		if(position==(getCount()-1))
		{
			((LinearLayout)convertView.findViewById(R.id.line_day)).setVisibility(View.GONE);
		}
		else
		{
			((LinearLayout)convertView.findViewById(R.id.line_day)).setVisibility(View.VISIBLE);

		}
		return convertView;
	}

}


