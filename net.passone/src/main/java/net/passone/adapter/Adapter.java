package net.passone.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;

import net.passone.IntroActivity;
import net.passone.R;
import net.passone.common.Util;

import java.util.HashMap;
import java.util.Map;


/**
 * Adapter 클래스<br>
 * 함수를 호출하면 HTTP 서버로 부터 API 결과를 가져온 후<br>
 * OnResponseListener 인터페이스의 onResponseReceived 콜백함수로 결과를 리턴한다. <br>
 */
public class Adapter implements OnApiAlertListener {

	// Adapter Manager.
	private AdapterManager _am;
	private Handler _handler = new Handler();
	/**
	 * Constructor.
	 */
	public Adapter() {
		// Create an adapter manager.
		_am = new AdapterManager();		
	}
	/**
	 * Constructor.
	 */
	public Adapter(int ApiUrl) {
		// Create an adapter manager.
		_am = new AdapterManager(ApiUrl);		
	}
	
	public void close() {
		// Close an adapter manager.
		_am.close();		
	}

	/**
	 * shutdownConnection<br><br>
	 * DefaultHttpClient 의 ConnectionManager 를 종료한다.<br>
	 * 모든 연결이 끊어진다.<br><br>
	 */
	public void shutdownConnection() { 
		_am.shutdownConnectionManager();	
	}	

	/**
	 *  �⺻ URL ����<br><br>
	 *  @param url
	 */
	public void setBaseUrl(String url) {
		_am.setBaseUrl(url);
	}

	
	/**
	 * 로그인<br><br> 
	 * 로그인(수시로)<br>
	 * 콜백함수인 onResponseReceived 의 두번째 인자를
	 * <b>Sign</b> 클래스로 형변환하여 사용해야한다.<br><br>

	 */

    public void Login(String userid , String password, String devicetoken, Context context, OnResponseListener callback)
    {

        Map<String, String> params = new HashMap<String, String>();
		PackageInfo packageInfo=null;
		String ver="";
		try {
			PackageManager pm = context.getPackageManager();
			packageInfo = pm.getPackageInfo(context.getPackageName(), 0);
			ver=packageInfo.versionName;

		} catch (PackageManager.NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		params.put("userid", userid);
		params.put("password", password);
		params.put("devicetoken", devicetoken);
		params.put("osVersion", Build.VERSION.RELEASE);
		params.put("deviceModel", Build.MODEL);
		params.put("appVersion", ver);
		params.put("udid", devicetoken);
		params.put("platform", "ANDROID");
        _am.request(new Params(Api.SIGN, params, context, callback, this, _handler));
    }

    public void Version(Context context, OnResponseListener callback)
	{		 
		Map<String, String> params = new HashMap<String, String>();
		params.put("platform", "android");
		params.put("api", "passone");
		params.put("store", "google");
		//params.put("store", "skt");
		//params.put("store", "naver");


	_am.request(new Params(Api.VERSION, params, context, callback, this, _handler));
	}

	public void MyCourse(String userid, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("userid", userid);
		_am.request(new Params(Api.COURSE, params, context, callback, this, _handler));
	}
	public void CourseInfo(String userid, String course, String ispackage, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("userid", userid);
		params.put("course", course);
		params.put("package", ispackage);


		_am.request(new Params(Api.COURSESINGLE, params, context, callback, this, _handler));
	}
	public void MyPackage(String userid, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("userid", userid);


		_am.request(new Params(Api.PACKAGE, params, context, callback, this, _handler));
	}
	public void MyPackageCourse(String userid, String package_id, String orderid, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("userid", userid);
		params.put("package", package_id);
		params.put("orderid", orderid);

		_am.request(new Params(Api.PACKAGECOURSE, params, context, callback, this, _handler));
	}
	public void MyTimeCourse(Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		_am.request(new Params(Api.TIMECOURSE, params, context, callback, this, _handler));
	}
	public void PassVoca(Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		_am.request(new Params(Api.PASSVOCA, params, context, callback, this, _handler));
	}
	public void MyCoupon(String userid, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("userid", userid);
		_am.request(new Params(Api.COUPON, params, context, callback, this, _handler));
	}
	public void MyCouponUsed(String userid, String coupon_id, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("userid", userid);
		params.put("coupon_id", coupon_id);

		_am.request(new Params(Api.COUPONUSED, params, context, callback, this, _handler));
	}

	public void DeviceInfo(String device_token, String udid, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("device_token", device_token);
		params.put("udid", udid);
		params.put("appcheck", "ANDROID");


		_am.request(new Params(Api.DEVICEINFO, params, context, callback, this, _handler));
	}
	public void VocaMp3(Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		_am.request(new Params(Api.PASSVOCAMP3, params, context, callback, this, _handler));
	}
	public void Lecture(String course, String orderid, String userid, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("course", course);
		params.put("orderid", orderid);
		params.put("userid", userid);


		_am.request(new Params(Api.LECTURE, params, context, callback, this, _handler));
	}
	public void TimeLecture(String course, String etc, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("course", course);
		params.put("etc", etc);

		_am.request(new Params(Api.LECTURE, params, context, callback, this, _handler));
	}
	public void LectureData(String lecture, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("lecture", lecture);

		_am.request(new Params(Api.LECDATA, params, context, callback, this, _handler));
	}
	public void PlayDownload(String lecture, String orderid, String userid, String courseid, String etc, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("courseid", courseid);
		params.put("orderid", orderid);
		params.put("userid", userid);
		params.put("lecture", lecture);
		params.put("etc", etc);


		_am.request(new Params(Api.LECDOWN, params, context, callback, this, _handler));
	}
	public void freeList(Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();

		_am.request(new Params(Api.FREELIST, params, context, callback, this, _handler));
	}
	public void freeDetail(String lecCode, String courseCode, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("lecCode", lecCode);
		params.put("courseCode", courseCode);

		_am.request(new Params(Api.FREEDETAIL, params, context, callback, this, _handler));
	}
	public void playStream(String lecture, String orderid, String userid, String courseid, String etc, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("courseid", courseid);
		params.put("orderid", orderid);
		params.put("userid", userid);
		params.put("lecture", lecture);
		params.put("etc", etc);


		_am.request(new Params(Api.PLAYSTREAM, params, context, callback, this, _handler));
	}
	public void playStream(String lecture, String courseid, String etc, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("courseid", courseid);

		params.put("lecture", lecture);
		params.put("etc", etc);


		_am.request(new Params(Api.PLAYSTREAM, params, context, callback, this, _handler));
	}
	public void playStatus(String playtype, String elapsed, String etc, String currentTime, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("playtype", playtype);
		params.put("elapsed", elapsed);
		params.put("currentTime", currentTime);
		params.put("etc", etc);
		params.put("platform", "android");


		_am.request(new Params(Api.PLAYSTATUS, params, context, callback, this, _handler));
	}
	public void playContinue(String userid, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("userid",userid);


		_am.request(new Params(Api.PLAYCONTINUE, params, context, callback, this, _handler));
	}
	public void ContinueLecture(String course, String uid, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("course",course);
		params.put("uid",uid);
		_am.request(new Params(Api.CONTINUELEC, params, context, callback, this, _handler));
	}
	public void PlaySample(String course, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("course",course);
		_am.request(new Params(Api.PLAYSAMPLE, params, context, callback, this, _handler));
	}
	public void RegisterApi(String firstlogin, String val, String udid, String devicetoken, Context context, OnResponseListener callback)
	{
		Map<String, String> params = new HashMap<String, String>();
		params.put("division","passone");
		params.put("firstlogin",firstlogin);
		params.put("val",val);
		params.put("udid",udid);
		params.put("devicetoken",devicetoken);

		_am.request(new Params(Api.REGISTERAPI, params, context, callback, this, _handler));
	}
	/**
	 * 통신장애 Alert 콜백
	 * API 요청 후 통신 장애가 발생하면 이 함수가 호출이 된다.<br><br>
	 * @param api - 요청한 API 명령
	 * @param params - 요청한 API 파라메터
	 */
	public void onApiAlert(int api, Params params, ApiException ex) {
		if (params != null) {
			try {

				alertYesNo(api, params, ex);

				Util.debug(" >> HTTP response exception code = " + ex.getErrorCode());
				Util.debug(" >> HTTP response exception message = " + ex.getMessage());
				
			} catch(Exception e) {;}
		}
	}

	/**
	 * 통신장애 Alert 메시지를 출력한다.<br><br>
	 * @param api - 요청한 API 명령
	 * @param params - 요청한 API 파라메터
	 * @throws Exception
	 */
	public void alertYesNo(final int api, final Params params, ApiException ex) throws Exception {

		final Context context = params.getContext();
		String errorstr="";
		if(ex.getErrorCode()==1002)
		{
//			errorstr="앱을 종료합니다. ";
			Util.ToastMessage(context, "인터넷에 연결할 수 없습니다.");
			if(params.getContext().getClass().equals(IntroActivity.class))
			{
//				((IntroActivity)params.getContext()).goNext();
			}
		}
		else
		{
			errorstr= Integer.toString(ex.getErrorCode());
			AlertDialog.Builder dialog = new AlertDialog.Builder(context);
			dialog.setTitle(context.getResources().getString(R.string.app_name))
			.setMessage(ex.getMessage() + "\n" + errorstr)	// error message
			.setCancelable(false)
			.setPositiveButton("확인", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.dismiss();
//					_handler.postDelayed(new Runnable() {
//						@Override
//						public void run() {
//							_am.request(params);
//						}
//					}, 5000);	// 통신상태가 변화할 시간을 준다.
//					IntentModelActivity.closeActivity(context); //통신장애 시 종료
				}
			}).setCancelable(false)	
//			.setNegativeButton("취소", new DialogInterface.OnClickListener() {		
//				@Override
//				public void onClick(DialogInterface dialog, int id) {					
//					dialog.cancel();
//					/*
//					 * 
//					 *  
//					 *  현재 임시로 강제종료.... 절차 밟아 종료하도록 기능 추가..요망
//					 *  
//					 *  
//					 */					
//					IntentModelActivity.closeActivity(context);
//				}		
//			})
			.show();
		}
		
	}
}