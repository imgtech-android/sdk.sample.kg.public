package net.passone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.passone.R;
import net.passone.common.Util;
import net.passone.container.DownloadItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

public class DownLectureAdapter extends BaseAdapter {
	Context context;
	ArrayList<DownloadItem> leclist;
	long firstdate=0;
	boolean mode_down=false;
	public Hashtable<Integer, View> hashConvertView = new Hashtable<Integer, View>();
	private String courseId="";


	public DownLectureAdapter(Context context, ArrayList<DownloadItem> list) {
		this.context=context;
		this.leclist=list;
	}
	public void setList(ArrayList<DownloadItem> list)
	{
		this.leclist=list;
	}

	public int getCount() {
		// TODO Auto-generated method stub

		return leclist.size();
	}


	public DownloadItem getItem(int position) {
		// TODO Auto-generated method stub
		return leclist.get(position);
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}
	public void setMode(boolean mode)
	{
		mode_down=mode;
	}
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final DownloadItem item;
			item=leclist.get(position);
		ViewHolder holder=new ViewHolder();

		if (hashConvertView.containsKey(position) == false) {
			convertView = (LinearLayout) LayoutInflater.from(context).inflate(
					R.layout.down_lecture_cell, parent, false);
			//			convertView = mInflater.inflate(R.layout.wronglist_cell, null);

			holder = new ViewHolder();
			holder.chk_lec = (CheckBox) convertView.findViewById(R.id.chk_lec);
			holder.tv_down_course=(TextView)convertView.findViewById(R.id.tv_down_course);
			holder.tv_down_lectitle=(TextView)convertView.findViewById(R.id.tv_down_lectitle);
			convertView.setTag(holder);
			hashConvertView.put(position, convertView);
			convertView.setTag(R.id.chk_lec, holder.chk_lec);

		} else {
			convertView = (View) hashConvertView.get(position);

			holder = (ViewHolder) convertView.getTag();
		}
		if(mode_down)
		{
			holder.chk_lec.setVisibility(View.VISIBLE);

		}
		else
			holder.chk_lec.setVisibility(View.GONE);


		String filepath=item.getCourseId()+"_"+item.getUid()+".mp4";
		if(item.leccode>0)
		{
			filepath=item.getCourseId()+"_"+item.getLeccode()+"_"+item.getUid()+".mp4";

		}
		String filePath= Util.getFilePath(context, filepath);
		Util.debug("filepath:" + filePath);

		File dfile=new File(filePath);

		holder.tv_down_course.setText(item.getCourse_title());
		if(item.getCourse_title()==null)
			holder.tv_down_course.setVisibility(View.GONE);
		else
			holder.tv_down_course.setVisibility(View.VISIBLE);
		holder.tv_down_lectitle.setText(item.getTitle());
		holder.chk_lec.setId(position);
		holder.chk_lec.setChecked(item.isSelected());
		holder.chk_lec.setFocusable(false);
		holder.chk_lec.setEnabled(true);
		holder.chk_lec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				item.setSelected(isChecked);
				Util.debug("isChecked:" + isChecked);
				notifyDataSetChanged();

			}
		});
		return convertView;
	}
	
public void goDetail(int position)
{
    final DownloadItem item;
    item=leclist.get(position);

}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	static class ViewHolder {
		CheckBox chk_lec;
		TextView tv_down_course;
		TextView tv_down_lectitle;

	}
}
