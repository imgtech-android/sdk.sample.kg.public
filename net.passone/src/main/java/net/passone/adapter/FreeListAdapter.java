package net.passone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import net.passone.R;
import net.passone.container.CourseDataInfo;
import net.passone.container.FreeItem;

import java.util.ArrayList;

/**
 * Created by passone on 2015-08-07.
 */
public class FreeListAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private ExpandableListView list;
    private ArrayList<FreeItem> clist;
    private int[] groupStatus;

    public FreeListAdapter(Context pContext,
                           ExpandableListView pExpandableListView,
                           ArrayList<FreeItem> pGroupCollection) {
        mContext = pContext;
        clist = pGroupCollection;
        list = pExpandableListView;
        groupStatus = new int[clist.size()];

        setListEvent();
    }

    private void setListEvent() {

        list.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {


            public void onGroupExpand(int arg0) {
                // TODO Auto-generated method stub
                groupStatus[arg0] = 1;
            }
        });

        list.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {


            public void onGroupCollapse(int arg0) {
                // TODO Auto-generated method stub
                groupStatus[arg0] = 0;
            }
        });
    }




    public long getChildId(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return 0;
    }


    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
                             ViewGroup parent) {
        // TODO Auto-generated method stub

        ChildHolder childHolder;
        CourseDataInfo cuItem=clist.get(groupPosition).getCourseData().get(childPosition);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.free_cell, null);

            childHolder = new ChildHolder();
            childHolder.tv_free_course = (TextView) convertView.findViewById(R.id.tv_free_course);


            convertView.setTag(childHolder);
        }else {
            childHolder = (ChildHolder) convertView.getTag();
        }
        childHolder.tv_free_course.setText(cuItem.lecName);


        return convertView;
    }


    public int getChildrenCount(int gPosition) {
        // TODO Auto-generated method stub
        if(clist.get(gPosition).getCourseData()!=null) {
            return clist.get(gPosition).getCourseData().size();
        }
        else
            return 0;
    }


    public Object getGroup(int gPosition) {
        // TODO Auto-generated method stub
        return clist.get(gPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return clist.get(groupPosition).getCourseData().get(childPosition);
    }


    public int getGroupCount() {
        // TODO Auto-generated method stub
        return clist.size();
    }


    public long getGroupId(int gPosition) {
        // TODO Auto-generated method stub
        return gPosition;
    }


    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        // TODO Auto-generated method stub
        GroupHolder groupHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.free_course,
                        null);
                groupHolder = new GroupHolder();
                groupHolder.iv_expand = (ImageView) convertView.findViewById(R.id.iv_expand);
                groupHolder.tv_free_package=(TextView)convertView.findViewById(R.id.tv_free_package);

                convertView.setTag(groupHolder);
            } else {
                groupHolder = (GroupHolder) convertView.getTag();
            }
        FreeItem cItem=clist.get(groupPosition);
        groupHolder.tv_free_package.setText(cItem.getCourseName());


                if (groupStatus[groupPosition] == 0) {
                    groupHolder.iv_expand.setImageResource(R.drawable.ico_2depth_off);
                } else {
                    groupHolder.iv_expand.setImageResource(R.drawable.ico_2depth_on);
                }
                groupHolder.iv_expand.setVisibility(View.VISIBLE);

            return convertView;


    }

    class GroupHolder {
        TextView tv_free_package;
        ImageView iv_expand;


    }

    class ChildHolder {
        TextView tv_free_course;


    }


    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return true;
    }


    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return true;
    }
}
