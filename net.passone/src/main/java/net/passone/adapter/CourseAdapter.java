package net.passone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.passone.R;
import net.passone.container.PackageItem;

import java.util.ArrayList;

public class CourseAdapter extends BaseAdapter {
	Context context;
	ArrayList<PackageItem> list;
	long firstdate=0;
    int mode=0; //0: 스크롤업 1: 스크롤다운
         public CourseAdapter(Context context, ArrayList<PackageItem> list) {
		this.context=context;
		this.list=list;
	}
	public void setList(ArrayList<PackageItem> list)
	{
		this.list=list;
	}

	public int getCount() {
		// TODO Auto-generated method stub

		return list.size();
	}


	public PackageItem getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}
    public void setMode(int mode)
    {
        this.mode=mode;
    }
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final PackageItem item;
			item=list.get(position);
		
		if (convertView == null)
		{

			convertView =  LayoutInflater.from(context).inflate(R.layout.course_cell, parent, false);
				


		}
		((TextView)convertView.findViewById(R.id.tv_ctitle)).setText(item.getTitle());
		((TextView)convertView.findViewById(R.id.tv_course_profname)).setText(item.getTeacher());
		((TextView)convertView.findViewById(R.id.tv_course_progress)).setText(item.getProgress()+"%");
		((ProgressBar)convertView.findViewById(R.id.progress_course)).setProgress(item.getProgress());


		return convertView;
	}
	
public void goDetail(int position)
{
    final PackageItem item;
    item=list.get(position);

}

}
