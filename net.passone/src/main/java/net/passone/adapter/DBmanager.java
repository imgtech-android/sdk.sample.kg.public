package net.passone.adapter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.provider.BaseColumns._ID;

public class DBmanager extends SQLiteOpenHelper {
	//private static final String DATABASE_NAME = "History.db";
	private static final int DTATBASE_VERSION =2;
	private String databasename ;

	public DBmanager(Context ctx, String dbname){

		super(ctx, dbname, null, DTATBASE_VERSION);
		databasename = dbname;
		// Log.d("EVENTSDATA", "EventsData ");
	}

	@Override
	public void onCreate(SQLiteDatabase db){
		// Log.d("EVENTSDATA", "SQLiteDatabase start");
		if(databasename.equals(databasename))
		{
			db.execSQL("CREATE TABLE " + "userinfo" +
							" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, mno INTEGER, userid VARCHAR, userpw VARCHAR, "+"nick"+" TEXT, pushchk INTEGER DEFAULT 1, autochk INTEGER, pushYN INTEGER);"
					);
			db.execSQL("CREATE TABLE " + "vocabulary" +
							" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, finish INTEGER, seen INTEGER, vindex INTEGER, day INTEGER, desc VARCHAR, vuid , meaning VARCHAR, word VARCHAR, userid VARCHAR );"
			);
			db.execSQL("CREATE TABLE " + "device" + 
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+"device_token"+" TEXT, pushchk INTEGER DEFAULT 1);"
					);
			db.execSQL("CREATE TABLE " + "event" + 
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, fromid VARCHAR, "+"url"+" TEXT, eventimg TEXT, view INTEGER DEFAULT 0, ispop INTEGER DEFAULT 0, senddate INTEGER DEFAULT 0 );"
					);
			db.execSQL("CREATE TABLE " + "appinfo" + 
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, version TEXT, jumprate INTEGER DEFAULT 10000, useSD INTEGER DEFAULT 0, notice INTEGER DEFAULT 0, isfirst INTEGER DEFAULT 1 );");

			db.execSQL("CREATE TABLE " + "lecture" +
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, leccode INTEGER, progress INTEGER, size INTEGER, downloadcomp INTEGER, sid INTEGER, sequence INTEGER, currenttime INTEGER, periodleft INTEGER, course_id VARCHAR, updatedate VARCHAR, etc VARCHAR, solostudyvideo VARCHAR, filename VARCHAR, orderid VARCHAR, solostudyvideotype VARCHAR, ts VARCHAR, coursreid VARCHAR, downloadurl VARCHAR, poststudytext VARCHAR, streamurl VARCHAR, uid VARCHAR, licenseurl VARCHAR, title VARCHAR, duration VARCHAR, prestudytext VARCHAR, lecindex VARCHAR, refund INTEGER, refundcolor INTEGER, complete INTEGER, smiurl VARCHAR);");

			db.execSQL("CREATE TABLE " + "package" +
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, days INTEGER, progress INTEGER, user_id VARCHAR, begindate VARCHAR, enddate VARCHAR, uid VARCHAR , coupontype VARCHAR, category VARCHAR, orderid VARCHAR, special VARCHAR, title VARCHAR, isdelete INTEGER);");
			db.execSQL("CREATE TABLE " + "packagecourse"  +
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, uid VARCHAR , package_id VARCHAR, course_id VARCHAR, isdelete INTEGER );");
			db.execSQL("CREATE TABLE " + "course"  +
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, progress INTEGER, totaltime INTEGER, lecturecount INTEGER, viewforall INTEGER, days INTEGER, package_id VARCHAR, user_id VARCHAR, enddate VARCHAR, begindate VARCHAR, etc VARCHAR, orderid VARCHAR, category VARCHAR, filmschedule VARCHAR, subject VARCHAR, lastlectureid VARCHAR, special VARCHAR, price VARCHAR, book VARCHAR, coupontype VARCHAR, uid VARCHAR , title VARCHAR, status VARCHAR, teacher VARCHAR, isdelete INTEGER );");

			db.execSQL("CREATE TABLE " + "download" +
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, mno INTEGER, etc VARCHAR, courseid VARCHAR, leccode INTEGER, lecture VARCHAR, filename VARCHAR, course_title VARCHAR, lec_title VARCHAR, orderid VARCHAR );");
			db.execSQL("CREATE TABLE voca ( isfinish INTEGER DEFAULT 0, seen INTEGER DEFAULT 0, vindex INTEGER, day INTEGER, desc VARCHAR, uid VARCHAR PRIMARY KEY, meaning VARCHAR, word VARCHAR, userid VARCHAR );");
			db.execSQL("CREATE TABLE vocaday ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, studydate VARCHAR);");
			db.execSQL("CREATE TABLE " + "time_download" +
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, mno INTEGER, etc VARCHAR, courseid VARCHAR, leccode INTEGER, lecture VARCHAR, filename VARCHAR, course_title VARCHAR, lec_title VARCHAR, orderid VARCHAR );");
			db.execSQL("CREATE TABLE " + "free_download" +
					" ("+_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, mno INTEGER, etc VARCHAR, lecdtlcode VARCHAR, leccode INTEGER, courseid VARCHAR, filename VARCHAR, lec_title VARCHAR, course_title VARCHAR);");

		}
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		// Log.d("EVENTSDATA", "SQLiteDatabase onUpgrade");
		if(oldVersion<=1)
		{
			boolean bla = existsColumnInTable(db,"appinfo","notice");
			if(!bla)
			{
				db.execSQL("ALTER TABLE appinfo " +
						"ADD notice INTEGER DEFAULT 0");
				
			}
			bla = existsColumnInTable(db,"appinfo","isfirst");
			if(!bla)
			{
				db.execSQL("ALTER TABLE appinfo " +
						"ADD isfirst INTEGER DEFAULT 1");

			}
		
		}


	}
	private boolean existsColumnInTable(SQLiteDatabase inDatabase, String inTable, String columnToCheck) {
		try{
			//query 1 row
			Cursor mCursor  = inDatabase.rawQuery( "SELECT * FROM " + inTable + " LIMIT 0", null );

			//getColumnIndex gives us the index (0 to ...) of the column - otherwise we get a -1
			if(mCursor.getColumnIndex(columnToCheck) != -1)
				return true;
			else
				return false;

		}catch (Exception Exp){
			//something went wrong. Missing the database? The table?
			Log.d("... - existsColumnInTable","When checking whether a column exists in the table, an error occurred: " + Exp.getMessage());
			return false;
		}
	}

}