package net.passone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.passone.R;
import net.passone.common.Util;
import net.passone.container.CourseItem;
import net.passone.container.PackageItem;

import java.util.ArrayList;

/**
 * Created by passone on 2015-08-07.
 */
public class PackageAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private ExpandableListView list;
    private ArrayList<PackageItem> fitem;
    private int[] groupStatus;

    public PackageAdapter(Context pContext,
                           ExpandableListView pExpandableListView,
                           ArrayList<PackageItem> pGroupCollection) {
        mContext = pContext;
        fitem = pGroupCollection;
        list = pExpandableListView;
        groupStatus = new int[fitem.size()];

        setListEvent();
    }

    private void setListEvent() {

        list.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {


            public void onGroupExpand(int arg0) {
                // TODO Auto-generated method stub
                if(fitem.get(arg0).getCourseList().size()>0)
                groupStatus[arg0] = 1;
            }
        });

        list.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {


            public void onGroupCollapse(int arg0) {
                // TODO Auto-generated method stub
               if(fitem.get(arg0).getCourseList().size()>0)
                    groupStatus[arg0] = 0;
            }
        });
    }




    public long getChildId(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return 0;
    }


    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
                             ViewGroup parent) {
        // TODO Auto-generated method stub

        ChildHolder childHolder;
        CourseItem pcItem=fitem.get(groupPosition).getCourseList().get(childPosition);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.pkg_course_cell, null);

            childHolder = new ChildHolder();
            childHolder.tv_subject = (TextView) convertView.findViewById(R.id.tv_pctitle);
            childHolder.tv_profname = (TextView) convertView.findViewById(R.id.tv_profname);
            childHolder.tv_pcprogress = (TextView) convertView.findViewById(R.id.tv_pkgcourse_progress);
            childHolder.progress_pc = (ProgressBar) convertView.findViewById(R.id.progress_pkgcourse);

            convertView.setTag(childHolder);
        }else {
            childHolder = (ChildHolder) convertView.getTag();
        }
        childHolder.tv_subject.setText(pcItem.getTitle());
        childHolder.tv_profname.setText(pcItem.getTeacher());
        childHolder.tv_pcprogress.setText(pcItem.getProgress()+"%");
        childHolder.progress_pc.setProgress(pcItem.getProgress());

        return convertView;
    }


    public int getChildrenCount(int gPosition) {
        // TODO Auto-generated method stub
        if(fitem.get(gPosition).getCourseList()!=null) {
            Util.debug("child size:"+fitem.get(gPosition).getCourseList().size());
            return fitem.get(gPosition).getCourseList().size();
        }
        else
            return 0;
    }


    public Object getGroup(int gPosition) {
        // TODO Auto-generated method stub
        return fitem.get(gPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return fitem.get(groupPosition).getCourseList().get(childPosition);
    }


    public int getGroupCount() {
        // TODO Auto-generated method stub
        return fitem.size();
    }


    public long getGroupId(int gPosition) {
        // TODO Auto-generated method stub
        return gPosition;
    }


    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        // TODO Auto-generated method stub
        GroupHolder groupHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.package_cell,
                        null);
                groupHolder = new GroupHolder();
                groupHolder.tv_subject = (TextView) convertView.findViewById(R.id.tv_ptitle);
                groupHolder.iv_expand = (ImageView) convertView.findViewById(R.id.iv_expand);
                groupHolder.tv_pkgprogress=(TextView)convertView.findViewById(R.id.tv_pkgprogress);
                groupHolder.progress_pkg=(ProgressBar)convertView.findViewById(R.id.progress_pkg);
                groupHolder.tv_teacher=(TextView)convertView.findViewById(R.id.tv_teacher);
                convertView.setTag(groupHolder);
            } else {
                groupHolder = (GroupHolder) convertView.getTag();
            }
            PackageItem pItem=fitem.get(groupPosition);
            groupHolder.tv_subject.setText(pItem.getTitle());
            if(!pItem.isPackage)
            {
                groupHolder.iv_expand.setVisibility(View.GONE);
                groupHolder.tv_teacher.setText(pItem.getTeacher());
            }
        else
            {
                if (groupStatus[groupPosition] == 0) {
                    groupHolder.iv_expand.setImageResource(R.drawable.ico_2depth_off);
                } else {
                    groupHolder.iv_expand.setImageResource(R.drawable.ico_2depth_on);
                }
                groupHolder.iv_expand.setVisibility(View.VISIBLE);
                groupHolder.tv_teacher.setText("패키지");
            }
            groupHolder.tv_pkgprogress.setText(pItem.getProgress()+"%");
            groupHolder.progress_pkg.setProgress(pItem.getProgress());
            return convertView;


    }

    class GroupHolder {
        TextView tv_subject;
        TextView tv_pkgprogress;
        ImageView iv_expand;
        ProgressBar progress_pkg;
        TextView tv_teacher;

    }

    class ChildHolder {
        TextView tv_subject;
        TextView tv_profname;
        TextView tv_pcprogress;
        ProgressBar progress_pc;

    }


    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return true;
    }


    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return true;
    }
}
