package net.passone.adapter;

import android.database.sqlite.SQLiteDatabase;

import net.passone.common.StaticVars;
import net.passone.container.CouponInfo;
import net.passone.container.CouponItem;
import net.passone.container.CouponUsedInfo;
import net.passone.container.CouponUsedItem;
import net.passone.container.CourseDataInfo;
import net.passone.container.CourseInfo;
import net.passone.container.CourseItem;
import net.passone.container.FreeDetailInfo;
import net.passone.container.FreeDetailItem;
import net.passone.container.FreeInfo;
import net.passone.container.FreeItem;
import net.passone.container.LecDataInfo;
import net.passone.container.LecDataItem;
import net.passone.container.LecDetailInfo;
import net.passone.container.LecDetailItem;
import net.passone.container.PackageInfo;
import net.passone.container.PackageItem;
import net.passone.container.TimeCourseInfo;
import net.passone.container.TimeCourseItem;

import java.util.ArrayList;
import java.util.List;


public class AdapterItemManager {
	DBmanager db_manager;
	SQLiteDatabase db;

		private static void package_addlist(String uid, String title, String category, String couponType, String beginDate, int days, int progress, String orderid, String soloStudy) {
    	PackageItem myitem = new PackageItem(uid,title,category,couponType,beginDate,days,progress,orderid,soloStudy,null,true,"");
    	StaticVars.packageItems.add(myitem);
		}

	public static void AddPackageList(List<PackageInfo> plist) {
		StaticVars.packageItems.clear();
		for (PackageInfo pkg : plist) {
			AdapterItemManager.package_addlist(pkg.uid,pkg.title,pkg.category,pkg.couponType,pkg.beginDate,pkg.days,pkg.progress,pkg.orderid,pkg.soloStudy);
		}
	}
	private static void pkgcourse_addlist(String uid, String title, String category, String couponType, String beginDate, int days, int lectureCount, String subject, String teacher, int progress, String orderid, String status, int totalTime, String lastLectureId, String soloStudy, String package_id) {
		CourseItem myitem = new CourseItem(uid, title, category, couponType, beginDate, days, lectureCount, subject, teacher, progress, orderid, status, totalTime,lastLectureId,soloStudy,package_id);
		StaticVars.pkgcourseItems.add(myitem);
	}

	public static void AddPkgCourseList(List<CourseInfo> clist, String package_id) {
		StaticVars.pkgcourseItems.clear();
		for (CourseInfo cItem : clist) {
			AdapterItemManager.pkgcourse_addlist(cItem.uid, cItem.title, cItem.category, cItem.couponType, cItem.beginDate, cItem.days, cItem.lectureCount, cItem.subject, cItem.teacher, cItem.progress, cItem.orderid, cItem.status, cItem.totalTime, cItem.lastLectureId, cItem.soloStudy, package_id);
		}
	}
	private static void course_addlist(String uid, String title, String category, String couponType, String beginDate, int days, int lectureCount, String subject, String teacher, int progress, String orderid, String status, int totalTime, String lastLectureId, String soloStudy) {
		PackageItem myitem = new PackageItem(uid,title,category,couponType,beginDate,days,progress,orderid,soloStudy,new ArrayList<CourseItem>(),false,teacher);
		StaticVars.courseItems.add(myitem);
	}

	public static void AddCourseList(List<CourseInfo> clist) {
		StaticVars.courseItems.clear();
		for (CourseInfo cItem : clist) {
			AdapterItemManager.course_addlist(cItem.uid, cItem.title, cItem.category, cItem.couponType, cItem.beginDate, cItem.days, cItem.lectureCount, cItem.subject, cItem.teacher, cItem.progress, cItem.orderid, cItem.status, cItem.totalTime, cItem.lastLectureId, cItem.soloStudy);
		}
	}
	private static void timecourse_addlist(String uid, String title, String category, String couponType, String beginDate, int days, int lectureCount, String subject, String teacher, String etc, String status, int totalTime, String special, String soloStudy, String filmSchedule, String book, String price) {
		TimeCourseItem myitem = new TimeCourseItem(uid, title, category, couponType, beginDate, days, lectureCount, subject, teacher, etc, status, totalTime, special, soloStudy, filmSchedule, book, price);
		StaticVars.timecourseItems.add(myitem);
	}

	public static void AddTimeCourseList(List<TimeCourseInfo> tlist) {
		StaticVars.timecourseItems.clear();
		for (TimeCourseInfo tItem : tlist) {
			AdapterItemManager.timecourse_addlist(tItem.uid, tItem.title, tItem.category, tItem.couponType, tItem.beginDate, tItem.days, tItem.lectureCount, tItem.subject, tItem.teacher, tItem.etc, tItem.status, tItem.totalTime, tItem.special, tItem.soloStudy, tItem.filmSchedule, tItem.book, tItem.price);
		}
	}
	private static void coupon_addlist(String uid, int remainedTime, int boughtTime, String startDate, String expireDate, String name, String couponType) {
		CouponItem myitem = new CouponItem(uid, remainedTime, boughtTime, startDate, expireDate, name, couponType);
		StaticVars.couponItems.add(myitem);
	}

	public static void AddCouponList(List<CouponInfo> clist) {
		StaticVars.couponItems.clear();
		for (CouponInfo cItem : clist) {
			AdapterItemManager.coupon_addlist(cItem.uid, cItem.remainedTime, cItem.boughtTime, cItem.startDate, cItem.expireDate, cItem.name, cItem.couponType);
		}
	}
	private static void couponused_addlist(String uid, String courseTitle, String lectureTitle, String date, int usedTime, String coupon_id) {
		CouponUsedItem myitem = new CouponUsedItem(uid, courseTitle, lectureTitle, date, usedTime,coupon_id);
		StaticVars.couponUsedItems.add(myitem);
	}

	public static void AddCouponUsedList(List<CouponUsedInfo> clist, String coupon_id) {
		StaticVars.couponUsedItems.clear();
		for (CouponUsedInfo cItem : clist) {
			AdapterItemManager.couponused_addlist(cItem.uid, cItem.courseTitle, cItem.lectureTitle, cItem.date, cItem.usedTime, coupon_id);
		}
	}

	private static void lecture_addlist(int refund, int refundColor, String uid, String lec_index, String title, String duration, String updatedDate, int progress, String orderid, String courseid, int leccode, String etc, int currentTime) {
		LecDetailItem myitem = new LecDetailItem(refund, refundColor, uid, lec_index, title, duration,updatedDate,progress,orderid,courseid,leccode,etc,currentTime);
		StaticVars.lecDetailItems.add(myitem);
	}

	public static void AddLectureList(List<LecDetailInfo> leclist) {
		StaticVars.lecDetailItems.clear();
		for (LecDetailInfo lecItem : leclist) {
			AdapterItemManager.lecture_addlist(lecItem.refund, lecItem.refundColor, lecItem.uid, lecItem.lec_index, lecItem.title, lecItem.duration, lecItem.updatedDate, lecItem.progress, lecItem.orderid, lecItem.courseid, lecItem.leccode, lecItem.etc, lecItem.currentTime);
		}
	}
	private static void lecdata_addlist(int uid, String url, String filename, int filesize) {
		LecDataItem myitem = new LecDataItem(uid,url,filename,filesize);
		StaticVars.lecDataItems.add(myitem);
	}

	public static void AddLecDataList(List<LecDataInfo> leclist) {
		StaticVars.lecDataItems.clear();
		for (LecDataInfo lecItem : leclist) {
			AdapterItemManager.lecdata_addlist(lecItem.uid, lecItem.url, lecItem.filename, lecItem.filesize);
		}
	}
	private static void free_addlist(String courseCode, String courseName, List<CourseDataInfo> courseData) {
		FreeItem item = new FreeItem();
		item.setFree(courseCode, courseName, courseData);
		StaticVars.freeItems.add(item);
	}

	public static void AddLecture(List<FreeInfo> list) {
		StaticVars.freeItems.clear();
		for (FreeInfo f : list) {
			AdapterItemManager.free_addlist(f.courseCode, f.courseName, f.courseData);
		}
	}
	private static void freeDetail_addlist(String lecInfo, String url, String lecDtlCode) {
		FreeDetailItem item = new FreeDetailItem();
		item.setFree(lecInfo, url, lecDtlCode);
		StaticVars.freeDetailItems.add(item);
	}

	public static void AddDetail(List<FreeDetailInfo> list) {
		StaticVars.freeDetailItems.clear();
		for (FreeDetailInfo f : list) {
			AdapterItemManager.freeDetail_addlist(f.lecInfo, f.url, f.lecDtlCode);
		}
	}
//	private static void quiz_addlist(String uid, String quest,String example,long regdate,int scrap,String explanation,int joinTotal, int correct, int incorrect, String answers) {
//    	QuizItem qitem = new QuizItem();
//    	qitem.setQuiz(uid, quest, example, regdate, scrap, explanation, joinTotal, correct, incorrect, answers);
//    	StaticVars.quizItems.add(qitem);
//    }
//
//	public static void AddQuizList(List<QuizInfo> qlist) {
//		StaticVars.quizItems.clear();
//		for (QuizInfo q : qlist) {
//			AdapterItemManager.quiz_addlist(q.qidx, q.quest, q.example, q.regdate,0, q.explanation,q.joinTotal, q.correct, q.incorrect, q.answers);
//		}
//	}

}