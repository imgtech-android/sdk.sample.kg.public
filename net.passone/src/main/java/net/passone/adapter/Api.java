package net.passone.adapter;


public class Api {

	/*
	 * API Command.
	 * 
	 * 상수의 값은 0부터 순차적이어야 한다.
	 * Path 배열의 인덱스로 사용되므로 상호 순서가 올바른지 확인하여야 한다.
	 * 신규로 추가 시는 뒤에 저장한다.
	 */	

	public static final int VERSION							= 0;	// 버전체크
	public static final int DEVICEINFO						= 1;	// 기기저장
	public static final int SIGN							= 2;	// 로그인
	public static final int PACKAGE							= 3;	// 내강의실 종합반
	public static final int PACKAGECOURSE					= 4;	// 내강의실 종합반 내 단과
	public static final int COURSE 							= 5;	// 내강의실 단과
	public static final int TIMECOURSE						= 6;	// 시간제강의
	public static final int PASSVOCA						= 7;	// 패스보카
	public static final int PASSVOCAMP3					= 8;	// 패스보카 mp3url
	public static final int COUPON							= 9;	// 시간제쿠폰
	public static final int COUPONUSED						= 10;	// 시간제 쿠폰 사용내역
	public static final int LECTURE							= 11;	// 강의목록
	public static final int LECDOWN							= 12;	// 강의 다운로드
	public static final int COURSESINGLE					= 13;	// 강의정보만 불러오기
	public static final int LECDATA							= 14;	// 강의 데이터
	public static final int TIMELECTURE					= 15;	// 시간제 강의 리스트
	public static final int FREELIST						= 16;	// 무료강의
	public static final int FREEDETAIL						= 17;	// 무료특강 상세 리스트
	public static final int PLAYSTREAM						= 18;	// 재생스트림
	public static final int PLAYSTATUS						= 19;	// 진도보내기
	public static final int PLAYCONTINUE					= 20;	// 이어보기
	public static final int CONTINUELEC					= 21;	// 이어보기 강의정보
	public static final int PLAYSAMPLE						= 22;	// 이어보기 강의정보
	public static final int REGISTERAPI					= 23;	// 광고수신동의



	// Base URL
	private static String BaseUrl;	

	// Path
	private static final String Path[] = {
		"version.php",
		"push_setting_act.php",
		"login_test2.php",
		"packages.php",
		"courses.php",
		"courses.php",
		"courses.php",
		"passvoca.php",
		"passvoca_url.php",
		"coupon.php",
		"coupon_used.php",
		"lectures.php",
		"play_download.php",
		"course.php",
			"lecture_data.php",
			"lectures.php",
			"free_list.php",
			"free_detail.php",
			"play_stream.php",
			"play_status.php",
			"play_continue.php",
			"lecture.php",
			"play_sample.php",
			"commonApi/contents/appsCommon/registerApi.php"

	};
//"memApi.php",
	/**
	 * 기본 URL 구함.<br><br>
	 * @return URL
	 */
	protected String getBaseUrl() {
		return BaseUrl;
	}	

	/**
	 * 기본 URL 설정.<br><br>
	 * @param url
	 */
	protected void setBaseUrl(String url) {
		BaseUrl = url;
	}	

	/**
	 * API에 대한 Path를 구함.<br><br>
	 * @param api
	 * @return 값이 존재하지 않는 경우 공백 &quot;&quot; 리턴.
	 */
	protected String getPath(int api) {
		try {
				
			return Path[api];
		} catch(Exception e) {
			return "";
		}
	}
}
