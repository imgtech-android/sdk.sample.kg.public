package net.passone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import net.passone.LectureDetailActivity;
import net.passone.R;
import net.passone.common.Util;
import net.passone.container.LecDetailItem;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

public class LectureAdapter extends BaseAdapter {
	Context context;
	ArrayList<LecDetailItem> leclist;
	long firstdate=0;
	boolean mode_down=false;
	public Hashtable<Integer, View> hashConvertView = new Hashtable<Integer, View>();
	private String lastLec="";


	public LectureAdapter(Context context, ArrayList<LecDetailItem> list) {
		this.context=context;
		this.leclist=list;
	}
	public void setList(ArrayList<LecDetailItem> list)
	{
		this.leclist=list;
	}

	public int getCount() {
		// TODO Auto-generated method stub

		return leclist.size();
	}


	public LecDetailItem getItem(int position) {
		// TODO Auto-generated method stub
		return leclist.get(position);
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}
	public void setMode(boolean mode)
	{
		mode_down=mode;
	}
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final LecDetailItem item;
			item=leclist.get(position);
		ViewHolder holder=new ViewHolder();

		if (hashConvertView.containsKey(position) == false) {
			convertView = (RelativeLayout) LayoutInflater.from(context).inflate(
					R.layout.lecture_cell, parent, false);
			//			convertView = mInflater.inflate(R.layout.wronglist_cell, null);
			holder = new ViewHolder();
			holder.chk_lec = (CheckBox) convertView.findViewById(R.id.chk_lec);
			holder.iv_downstate = (ImageView) convertView.findViewById(R.id.iv_downstate);
			holder.iv_lastlec = (ImageView) convertView.findViewById(R.id.iv_lastlec);
			holder.progress_course=(ProgressBar)convertView.findViewById(R.id.progress_course);
			holder.tv_course_progress=(TextView)convertView.findViewById(R.id.tv_course_progress);
			holder.tv_lectitle=(TextView)convertView.findViewById(R.id.tv_lectitle);
			convertView.setTag(holder);
			hashConvertView.put(position, convertView);
			convertView.setTag(R.id.chk_lec, holder.chk_lec);

		} else {
			convertView = (View) hashConvertView.get(position);

			holder = (ViewHolder) convertView.getTag();
		}
		if(mode_down)
		{
			holder.chk_lec.setVisibility(View.VISIBLE);

		}
		else
			holder.chk_lec.setVisibility(View.GONE);
		if(lastLec.length()>0 && lastLec.equals(item.getUid()))
			holder.iv_lastlec.setVisibility(View.VISIBLE);
		else
			holder.iv_lastlec.setVisibility(View.INVISIBLE);
		String filepath=item.getCourseId().replace(":","_")+"_"+item.getLeccode()+"_"+item.getUid()+".mp4";

		String filePath= Util.getFilePath(context, filepath);
		File dfile=new File(filePath);
		if(dfile.isFile() && dfile.exists())
		{
			holder.iv_downstate.setVisibility(View.VISIBLE);
			if(item.getIsDown()>0)
				holder.iv_downstate.setImageResource(R.drawable.ico_downok);
			else
				holder.iv_downstate.setImageResource(R.drawable.ico_downstop);

		}
		else {
			holder.iv_downstate.setVisibility(View.INVISIBLE);
			item.setFilePath("");
			item.setIsDown(0);
		}
		holder.progress_course.setProgress(item.getProgress());
		holder.tv_course_progress.setText(item.getProgress() + "%");
		holder.tv_lectitle.setText(item.getTitle()+" ("+item.getDuration()+")");

		holder.chk_lec.setId(position);
		holder.chk_lec.setChecked(item.isSelected());
		holder.chk_lec.setFocusable(false);
		holder.chk_lec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				item.setSelected(isChecked);
				((LectureDetailActivity)context).countChk(isChecked);
			}
		});
		return convertView;
	}
	
public void goDetail(int position)
{
    final LecDetailItem item;
    item=leclist.get(position);

}

	public void setLastLec(String lastLec) {
		this.lastLec = lastLec;
	}

	static class ViewHolder {
		CheckBox chk_lec;
		TextView tv_lectitle;
		ProgressBar progress_course;
		TextView tv_course_progress;
		ImageView iv_lastlec;
		ImageView iv_downstate;

	}
}
