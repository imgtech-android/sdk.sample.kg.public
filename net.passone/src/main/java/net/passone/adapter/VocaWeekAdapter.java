package net.passone.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.passone.R;
import net.passone.container.VocaItem;

import java.util.ArrayList;
import java.util.Hashtable;

public class VocaWeekAdapter extends BaseAdapter {
	Context context;
	ArrayList<VocaItem> list;
	long firstdate=0;
	DBmanager db_manager;
	SQLiteDatabase db;
	public Hashtable<Integer, View> hashConvertView = new Hashtable<Integer, View>();
	MediaPlayer mMediaPlayer;
	int mode=0; //0: 스크롤업 1: 스크롤다운
	public VocaWeekAdapter(Context context, ArrayList<VocaItem> list) {
		this.context=context;
		this.list=list;
		db_manager = new DBmanager(context, "UserInfo.db");
		db = db_manager.getWritableDatabase();

	}
	public void setList(ArrayList<VocaItem> list)
	{
		this.list=list;
	}

	public int getCount() {
		// TODO Auto-generated method stub

		return list.size();
	}


	public VocaItem getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}
	public void setMode(int mode)
	{
		this.mode=mode;
	}
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final VocaItem item;
		item=list.get(position);
		ViewHolder holder=new ViewHolder();

		if (hashConvertView.containsKey(position) == false) {
			convertView = (LinearLayout) LayoutInflater.from(context).inflate(
					R.layout.voca_week_cell, parent, false);
			//			convertView = mInflater.inflate(R.layout.wronglist_cell, null);
			holder.chk_memory = (CheckBox) convertView.findViewById(R.id.chk_memory);
			holder.tv_w_mean = (TextView) convertView.findViewById(R.id.tv_w_mean);
			holder.tv_w_word = (TextView) convertView.findViewById(R.id.tv_w_word);
			convertView.setTag(holder);
			hashConvertView.put(position, convertView);
			convertView.setTag(R.id.chk_memory, holder.chk_memory);
			holder.chk_memory.setOnCheckedChangeListener(null);

		} else {
			convertView = (View) hashConvertView.get(position);

			holder = (ViewHolder) convertView.getTag();
		}
		if(item.getMeanVisible()==0)
		{
			holder.tv_w_mean.setText("???");
		}
		else
		{
			holder.tv_w_mean.setText(item.getMeaning());

		}
		if(item.getWordVisible()==0)
		{
			holder.tv_w_word.setText("???");
		}
		else
		{
			holder.tv_w_word.setText(item.getWord());

		}

		if(item.getIsfinish()>0)
		{
			holder.chk_memory.setChecked(true);
		}
		else
			holder.chk_memory.setChecked(false);
		holder.chk_memory.setVisibility(View.VISIBLE);
		holder.chk_memory.setFocusable(false);
		holder.tv_w_word.setFocusable(false);

		holder.chk_memory.setId(position);
		final ContentValues cv=new ContentValues();
		holder.tv_w_word.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String word = item.getWord().replace(" ", "");

				try {
					mMediaPlayer = new MediaPlayer();
					mMediaPlayer.setDataSource(context.getExternalFilesDir(null).toString()+"/voca/" + word + ".mp3");  // path 에 mp3 파일 경로
					mMediaPlayer.prepare();
					mMediaPlayer.seekTo(0);
					mMediaPlayer.start();
					mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
						public void onCompletion(MediaPlayer mp) {
							mp.release();

						};
					});
				}catch (Exception e){}
			}
		});
		holder.chk_memory.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					item.setIsfinish(1);
					item.setSeen(1);
				} else {
					item.setIsfinish(0);
					item.setSeen(1);
				}
				cv.put("isfinish", item.getIsfinish());
				cv.put("seen", item.getSeen());
				db.update("voca", cv, "uid=?",new String[]{item.getUid()});
			}
		});

		return convertView;
	}
	static class ViewHolder {
		CheckBox chk_memory;
		TextView tv_w_word;
		TextView tv_w_mean;

	}
}


