package net.passone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import net.passone.R;
import net.passone.common.Util;
import net.passone.container.LecDataItem;

import java.io.File;
import java.util.ArrayList;

public class LecDataAdapter extends BaseAdapter {
	Context context;
	ArrayList<LecDataItem> list;
	long firstdate=0;
	boolean mode_del=false;
	String classidx="";
	String filePath="";
    int mode=0; //0: 스크롤업 1: 스크롤다운
         public LecDataAdapter(Context context, ArrayList<LecDataItem> list) {
		this.context=context;
		this.list=list;
	 filePath= Util.getFilePath(context, "");

		 }
	public void setList(ArrayList<LecDataItem> list)
	{
		this.list=list;
	}
	public void setMode(boolean mode)
	{
		mode_del=mode;
	}
	public int getCount() {
		// TODO Auto-generated method stub

		return list.size();
	}


	public LecDataItem getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}


	public long getItemId(int positon) {
		// TODO Auto-generated method stub
		return 0;
	}
    public void setMode(int mode)
    {
        this.mode=mode;
    }
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final LecDataItem item;
			item=list.get(position);
		
		if (convertView == null)
		{

			convertView =  LayoutInflater.from(context).inflate(R.layout.lecdata_cell, parent, false);
				


		}
		((TextView)convertView.findViewById(R.id.tv_lecdata_title)).setText(item.getFileName());
		((TextView)convertView.findViewById(R.id.tv_filesize)).setText(item.getFileSize()/ Util.KiloBytes+"kb");
		final String downPath=filePath+item.getFileName();
		if(new File(downPath).exists())
		{
			((ImageView)convertView.findViewById(R.id.iv_filestate)).setVisibility(View.VISIBLE);
		}
		else
		{
			((ImageView)convertView.findViewById(R.id.iv_filestate)).setVisibility(View.GONE);

		}
		return convertView;
	}
	
public void goDetail(int position)
{
    final LecDataItem item;
    item=list.get(position);

}

}
