package net.passone.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.StrictMode;

import net.passone.R;
import net.passone.common.Constants;
import net.passone.common.Util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class APIAgent {
	public static String BASE_URL_FORMAT = "http://api.passone.net/%s/%s/";
	private DefaultHttpClient client;
	private final Context context;
	private Handler handler;
	private Runnable completeCallback;
	private Thread worker;

	private boolean stopFlag;

	public APIAgent(Context context) {
		this.context = context;
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(new BasicHttpParams(), 1000);
		HttpConnectionParams.setSoTimeout(new BasicHttpParams(), 1000);

		//		try {
		//			String response = requestWithURL(Environments.STATUS_URL);
		//			if (response.equals("OK")) {
		//				String[] valSplit=(Environments.STATUS_URL).split("currentTime=");
		//				if(valSplit[1]!=null){
		//					String currenttime=valSplit[1];
		//					DataBroker broker = new DataBroker(context);
		//					if(Environments.STATUS_UID.length()>0)
		//						broker.update("lecture",  Util.map("currenttime", currenttime), "ZUID=?", Environments.STATUS_UID);
		//					Environments.STATUS_URL="";
		//					Environments.STATUS_UID="";
		//					Environments.save(context);
		//
		//				}
		//			} 
		//
		//
		//
		//		} catch (ClientProtocolException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}

	}

	public String requestWithPath(String path, Object... parameters) throws IOException {
		return requestWithURL(buildURL(path, parameters));
	}

	public String requestWithURL(String url) throws IOException, ClientProtocolException {
	
		url=url.replaceAll(" ", "");
		try
		{
			HttpGet request = new HttpGet(url);
			HttpResponse response = client.execute(request);
			Util.debug(url);

			ByteArrayOutputStream os = new ByteArrayOutputStream();
			InputStream is = response.getEntity().getContent();

			int max = (int)(response.getEntity().getContentLength());
		

			byte[] b = new byte[4096 + 4096];
			int size = 0;
			int downloaded = 0;
			while ((size = is.read(b)) > 0) {
				os.write(b, 0, size);
				downloaded += size;
				
			}

			is.close();

			os.flush();

			os.close();
			//			Util.ToastMessage(context, url);

			Util.debug(os.toString("UTF-8"));
			return os.toString("UTF-8");
		}
		catch(Exception e)
		{
			Util.debug(e.toString());
		
			if(e.toString().contains("Socket closed"))
			{
				return "Socket error";
			}
			else if(e.toString().contains("Request aborted"))
			{
				return "Request error";
			}
			return String.format("%s", context.getText(R.string.api_http_alert));
		}
	}

	public String buildURL(String path, Object... parameters) {
		try {
			for (int i = 0; i < parameters.length; i++) {
				parameters[i] = URLEncoder.encode(parameters[i].toString(), "UTF-8");
			}
			String url;
		
				url = Constants.baseUrl + String.format(path, parameters);
			return url;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}
	public String requestPostWithURL(String url, String... param) throws IOException, ClientProtocolException {
		try
		{
			InputStream is = null;
			
			HttpPost post = new HttpPost(url); 
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			for(int i=0;i<param.length;i+=2)
			{
				params.add(new BasicNameValuePair(param[i], param[i+1]));
			}

			UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,HTTP.UTF_8);
			post.setEntity(ent);
			HttpResponse responsePOST = client.execute(post);  
			HttpEntity resEntity = responsePOST.getEntity();
			is = resEntity.getContent();
			//				 BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();

			String result=sb.toString();		
//			result=result.replace("[", "");
//			result=result.replace("]", "");
			return result;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return String.format("%s", context.getText(R.string.api_http_alert));

		}
	}


	public List<Cookie> getCookies() {
		return client.getCookieStore().getCookies();
	}
	/**
	 * Clear cookie.
	 */
	public void clearCookie() {
		client.getCookieStore().clear();
	}
	
	

	


}

