package net.passone.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import net.passone.R;
import net.passone.common.Util;
import net.passone.container.CouponItem;
import net.passone.container.CouponUsedItem;

import java.util.ArrayList;

/**
 * Created by passone on 2015-08-07.
 */
public class CouponAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private ExpandableListView list;
    private ArrayList<CouponItem> clist;
    private int[] groupStatus;

    public CouponAdapter(Context pContext,
                         ExpandableListView pExpandableListView,
                         ArrayList<CouponItem> pGroupCollection) {
        mContext = pContext;
        clist = pGroupCollection;
        list = pExpandableListView;
        groupStatus = new int[clist.size()];

        setListEvent();
    }

    private void setListEvent() {

        list.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {


            public void onGroupExpand(int arg0) {
                // TODO Auto-generated method stub
                groupStatus[arg0] = 1;
            }
        });

        list.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {


            public void onGroupCollapse(int arg0) {
                // TODO Auto-generated method stub
                groupStatus[arg0] = 0;
            }
        });
    }




    public long getChildId(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return 0;
    }


    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
                             ViewGroup parent) {
        // TODO Auto-generated method stub

        ChildHolder childHolder;
        CouponUsedItem cuItem=clist.get(groupPosition).getCouponUsedList().get(childPosition);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.couponused_cell, null);

            childHolder = new ChildHolder();
            childHolder.tv_lectitle = (TextView) convertView.findViewById(R.id.tv_lectitle);
            childHolder.tv_coursetitle = (TextView) convertView.findViewById(R.id.tv_coursetitle);
            childHolder.tv_coupondate = (TextView) convertView.findViewById(R.id.tv_coupondate);
            childHolder.tv_usedtime = (TextView) convertView.findViewById(R.id.tv_usedtime);

            convertView.setTag(childHolder);
        }else {
            childHolder = (ChildHolder) convertView.getTag();
        }
        childHolder.tv_lectitle.setText(cuItem.getLectureTitle());
        childHolder.tv_coursetitle.setText(cuItem.getCourseTitle());
        childHolder.tv_coupondate.setText(cuItem.getDate());
        childHolder.tv_usedtime.setText(Util.secondToTime(cuItem.getUsedTime()));

        return convertView;
    }


    public int getChildrenCount(int gPosition) {
        // TODO Auto-generated method stub
        if(clist.get(gPosition).getCouponUsedList()!=null) {
            Util.debug("child size:"+clist.get(gPosition).getCouponUsedList().size());
            return clist.get(gPosition).getCouponUsedList().size();
        }
        else
            return 0;
    }


    public Object getGroup(int gPosition) {
        // TODO Auto-generated method stub
        return clist.get(gPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return clist.get(groupPosition).getCouponUsedList().get(childPosition);
    }


    public int getGroupCount() {
        // TODO Auto-generated method stub
        return clist.size();
    }


    public long getGroupId(int gPosition) {
        // TODO Auto-generated method stub
        return gPosition;
    }


    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        // TODO Auto-generated method stub
        GroupHolder groupHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(R.layout.coupon_cell,
                        null);
                groupHolder = new GroupHolder();
                groupHolder.tv_coupontitle = (TextView) convertView.findViewById(R.id.tv_coupontitle);
                groupHolder.iv_expand = (ImageView) convertView.findViewById(R.id.iv_expand);
                groupHolder.tv_startendtime=(TextView)convertView.findViewById(R.id.tv_startendtime);

                convertView.setTag(groupHolder);
            } else {
                groupHolder = (GroupHolder) convertView.getTag();
            }
            CouponItem cItem=clist.get(groupPosition);
            groupHolder.tv_coupontitle.setText(cItem.getCouponName()+"("+ Util.secondToTime(cItem.getRemainedTime())+" / "+ Util.secondToTime(cItem.getBoughtTimet()));
            groupHolder.tv_startendtime.setText("사용기간: "+cItem.getStartDate()+" ~ "+cItem.getExpireDate());


                if (groupStatus[groupPosition] == 0) {
                    groupHolder.iv_expand.setImageResource(R.drawable.ico_2depth_off);
                } else {
                    groupHolder.iv_expand.setImageResource(R.drawable.ico_2depth_on);
                }
                groupHolder.iv_expand.setVisibility(View.VISIBLE);

            return convertView;


    }

    class GroupHolder {
        TextView tv_coupontitle;
        TextView tv_startendtime;
        ImageView iv_expand;


    }

    class ChildHolder {
        TextView tv_lectitle;
        TextView tv_coursetitle;
        TextView tv_coupondate;
        TextView tv_usedtime;

    }


    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return true;
    }


    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return true;
    }
}
